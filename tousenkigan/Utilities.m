//
//  Utilities.m
//  tousenkigan
//
//  Created by JCLuspo on 7/23/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities

+ (UIImage*)resizeImage: (UIImage *)image toSize: (CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage*)createResizableImageFromNinePatchImage:(UIImage*)ninePatchImage
{
    NSArray* rgbaImage = [self getRGBAsFromImage:ninePatchImage atX:0 andY:0 count:ninePatchImage.size.width * ninePatchImage.size.height];
    NSArray* topBarRgba = [rgbaImage subarrayWithRange:NSMakeRange(1, ninePatchImage.size.width - 2)];
    NSMutableArray* leftBarRgba = [NSMutableArray arrayWithCapacity:0];
    int count = [rgbaImage count];
    for (int i = 0; i < count; i += ninePatchImage.size.width) {
        [leftBarRgba addObject:rgbaImage[i]];
    }
    
    int top = -1, left = -1, bottom = -1, right = -1;
    count = [topBarRgba count];
    for (int i = 0; i <= count - 1; i++) {
        NSArray* aColor = topBarRgba[i];
        if ([aColor[3] floatValue] == 1) {
            left = i;
            break;
        }
    }
    NSAssert(left != -1, @"The 9-patch PNG format is not correct.");
    for (int i = count - 1; i >= 0; i--) {
        NSArray* aColor = topBarRgba[i];
        if ([aColor[3] floatValue] == 1) {
            right = i;
            break;
        }
    }
    NSAssert(right != -1, @"The 9-patch PNG format is not correct.");
    for (int i = left + 1; i <= right - 1; i++) {
        NSArray* aColor = topBarRgba[i];
        if ([aColor[3] floatValue] < 1) {
            NSAssert(NO, @"The 9-patch PNG format is not support.");
        }
    }
    count = [leftBarRgba count];
    for (int i = 0; i <= count - 1; i++) {
        NSArray* aColor = leftBarRgba[i];
        if ([aColor[3] floatValue] == 1) {
            top = i;
            break;
        }
    }
    NSAssert(top != -1, @"The 9-patch PNG format is not correct.");
    for (int i = count - 1; i >= 0; i--) {
        NSArray* aColor = leftBarRgba[i];
        if ([aColor[3] floatValue] == 1) {
            bottom = i;
            break;
        }
    }
    NSAssert(bottom != -1, @"The 9-patch PNG format is not correct.");
    for (int i = top + 1; i <= bottom - 1; i++) {
        NSArray* aColor = leftBarRgba[i];
        if ([aColor[3] floatValue] == 0) {
            NSAssert(NO, @"The 9-patch PNG format is not support.");
        }
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([ninePatchImage CGImage], CGRectMake(1, 1, ninePatchImage.size.width - 2, ninePatchImage.size.height - 2));
    UIImage* cropImage = [UIImage imageWithCGImage:imageRef];
    
    return [cropImage resizableImageWithCapInsets:UIEdgeInsetsMake(top, left, bottom, right) resizingMode:UIImageResizingModeStretch];
}

+ (NSArray*)getRGBAsFromImage:(UIImage*)image atX:(int)xx andY:(int)yy count:(int)count
{
    NSMutableArray* result = [NSMutableArray arrayWithCapacity:count];
    
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char* rawData = (unsigned char*)calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    int byteIndex = (bytesPerRow * yy) + xx * bytesPerPixel;
    for (int ii = 0; ii < count; ++ii) {
        CGFloat red = (rawData[byteIndex] * 1.0) / 255.0;
        CGFloat green = (rawData[byteIndex + 1] * 1.0) / 255.0;
        CGFloat blue = (rawData[byteIndex + 2] * 1.0) / 255.0;
        CGFloat alpha = (rawData[byteIndex + 3] * 1.0) / 255.0;
        byteIndex += 4;
        
        NSArray* aColor = [NSArray arrayWithObjects:[NSNumber numberWithFloat:red], [NSNumber numberWithFloat:green], [NSNumber numberWithFloat:blue], [NSNumber numberWithFloat:alpha], nil];
        [result addObject:aColor];
    }
    
    free(rawData);
    
    return result;
}

+ (NSArray *) dailyLuckyNumber: (int)size
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDate *lastUpdate = [userDefault objectForKey:LUCKY_LOTTO_DATE_DEFAULT[size]];
    NSArray *lastNumbers = [userDefault objectForKey:LUCKY_LOTTO_DEFAULT[size]];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateStyle:NSDateFormatterShortStyle];
    [format setTimeStyle:NSDateFormatterNoStyle];
    
    NSString *dateString = [format stringFromDate:lastUpdate];
    lastUpdate = [format dateFromString:dateString];
    
    NSDate *now = [NSDate date];
    dateString = [format stringFromDate:now];
    now = [format dateFromString:dateString];
    
    if ([now compare:lastUpdate] == NSOrderedSame && lastNumbers != NULL) {
        return lastNumbers;
    }
    
    NSMutableArray *numbers = [[NSMutableArray alloc] initWithCapacity:size];
    
    for (int i=0; i<size; i++) {
        
        int randomNumber = 1;
        
        do {
            randomNumber = [self randomNumberFromLotto:size];
        } while (size >= MINILOTTO && [numbers containsObject:[[NSNumber alloc] initWithInt:randomNumber]]);
        
        [numbers addObject:[[NSNumber alloc] initWithInt:randomNumber]];
    }
    
    if (size >= MINILOTTO) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
        numbers = [[numbers sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    }
    
    [userDefault setObject:numbers forKey:LUCKY_LOTTO_DEFAULT[size]];
    [userDefault setObject:[NSDate date] forKey:LUCKY_LOTTO_DATE_DEFAULT[size]];
    
    return [numbers copy];
}

+ (int) randomNumberFromLotto: (int) lottoType
{
    int maxNumber = 37;

    switch (lottoType) {
    
        case NUMBER3:
            maxNumber = 10;
            break;

        case NUMBER4:
            maxNumber = 10;
            break;
            
        case MINILOTTO:
            maxNumber = 31;
            break;
            
        case LOTTO6:
            maxNumber = 43;
            break;
    }
    
    int rand = arc4random_uniform(maxNumber);
    
    if (lottoType >= MINILOTTO) {
        rand++;
    }
    
    return rand;
}

+(NSString*)formatNumberString:(NSString*)str{
    NSString *strNew = @"";
    strNew = [str stringByReplacingOccurrencesOfString:@"(" withString:@""];
    strNew = [strNew stringByReplacingOccurrencesOfString:@")" withString:@""];
    return strNew;
}

/**
　xx年 yy月 zz日　形式のStringからXYZを抜き出してDictionaryで返す。年月日の順で抽出できない部分は空で返す
 */
+ (NSDictionary*)getDateStringArrayFromString: (NSString*)string {
    NSArray* separated = [string componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"年月日"]];
    if ([separated count] < 3){
        return @{@"year":@"",@"month":@"",@"day":@""};
    }
    return @{
        @"year":separated[0],
        @"month":separated[1],
        @"day":separated[2]
    };
}

@end
