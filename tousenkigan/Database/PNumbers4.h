//
//  PNumbers4.h
//  tousenkigan
//
//  Created by JCLuspo on 8/29/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PNumbers4 : NSManagedObject

@property (nonatomic, retain) NSString * count;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * number_1;
@property (nonatomic, retain) NSString * number_2;
@property (nonatomic, retain) NSString * number_3;
@property (nonatomic, retain) NSString * number_4;

@end
