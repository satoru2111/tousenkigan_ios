//
//  Lotto7.m
//  tousenkigan
//
//  Created by JCLuspo on 8/7/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "Lotto7.h"


@implementation Lotto7

@dynamic count;
@dynamic date;
@dynamic hit_1;
@dynamic hit_2;
@dynamic hit_3;
@dynamic hit_4;
@dynamic hit_5;
@dynamic hit_6;
@dynamic number_1;
@dynamic number_2;
@dynamic number_3;
@dynamic number_4;
@dynamic number_5;
@dynamic number_6;
@dynamic number_7;
@dynamic number_bonus_1;
@dynamic number_bonus_2;
@dynamic prize_1;
@dynamic prize_2;
@dynamic prize_3;
@dynamic prize_4;
@dynamic prize_5;
@dynamic prize_6;
@dynamic prize_carry_over;
@dynamic prize_total;

@end
