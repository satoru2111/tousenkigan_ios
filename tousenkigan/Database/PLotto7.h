//
//  PLotto7.h
//  tousenkigan
//
//  Created by JCLuspo on 8/29/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PLotto7 : NSManagedObject

@property (nonatomic, retain) NSString * count;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * number_1;
@property (nonatomic, retain) NSString * number_2;
@property (nonatomic, retain) NSString * number_3;
@property (nonatomic, retain) NSString * number_4;
@property (nonatomic, retain) NSString * number_5;
@property (nonatomic, retain) NSString * number_6;
@property (nonatomic, retain) NSString * number_7;

@end
