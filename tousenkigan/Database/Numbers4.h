//
//  Numbers4.h
//  tousenkigan
//
//  Created by JCLuspo on 8/7/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Numbers4 : NSManagedObject

@property (nonatomic, retain) NSString * count;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * hit_1;
@property (nonatomic, retain) NSString * hit_2;
@property (nonatomic, retain) NSString * hit_3;
@property (nonatomic, retain) NSString * hit_4;
@property (nonatomic, retain) NSString * number_1;
@property (nonatomic, retain) NSString * number_2;
@property (nonatomic, retain) NSString * number_3;
@property (nonatomic, retain) NSString * number_4;
@property (nonatomic, retain) NSString * prize_1;
@property (nonatomic, retain) NSString * prize_2;
@property (nonatomic, retain) NSString * prize_3;
@property (nonatomic, retain) NSString * prize_4;
@property (nonatomic, retain) NSString * prize_total;

@end
