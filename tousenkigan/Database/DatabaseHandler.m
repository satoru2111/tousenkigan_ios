//
//  DatabaseHandler.m
//  tousenkigan
//
//  Created by JCLuspo on 7/15/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "DatabaseHandler.h"
#import "Utilities.h"
#import "FMDatabase.h"
#import "Result.h"
#import "Number.h"

@interface DatabaseHandler ()
@property (strong, nonatomic) NSManagedObjectContext *context;
@end

@implementation DatabaseHandler

- (id)init
{
    self = [super init];
    
    if(self) {
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        self.context = appDelegate.managedObjectContext;
    }
    
    return self;
}

- (void) dbToCoreData
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *documents_dir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *db_path = [documents_dir stringByAppendingPathComponent:[NSString stringWithFormat:@"application.db"]];
    NSString *template_path = [[NSBundle mainBundle] pathForResource:@"app" ofType:@"db"];
    
    if (![fm fileExistsAtPath:db_path])
        [fm copyItemAtPath:template_path toPath:db_path error:nil];
    FMDatabase *db = [FMDatabase databaseWithPath:db_path];
    
    
    
    
    [db open];
    {
        NSMutableArray *resultsLoto7 = [[NSMutableArray alloc] init];
        // list up
        NSString *selectSqlLoto7 = @"SELECT * from loto7 order by CAST(count AS SIGNED) DESC;";
        
        FMResultSet *result = [db executeQuery:selectSqlLoto7];
        while([result next]) {
            NSString *count = [result stringForColumnIndex:1];
            NSString *date = [result stringForColumnIndex:2];
            NSString *number1 = [result stringForColumnIndex:3];
            NSString *number2 = [result stringForColumnIndex:4];
            NSString *number3 = [result stringForColumnIndex:5];
            NSString *number4 = [result stringForColumnIndex:6];
            NSString *number5 = [result stringForColumnIndex:7];
            NSString *number6 = [result stringForColumnIndex:8];
            NSString *number7 = [result stringForColumnIndex:9];
            NSString *numberB1 = [result stringForColumnIndex:10];
            NSString *numberB2 = [result stringForColumnIndex:11];
            NSString *hit1 = [result stringForColumnIndex:12];
            NSString *hit2 = [result stringForColumnIndex:13];
            NSString *hit3 = [result stringForColumnIndex:14];
            NSString *hit4 = [result stringForColumnIndex:15];
            NSString *hit5 = [result stringForColumnIndex:16];
            NSString *hit6 = [result stringForColumnIndex:17];
            NSString *price1 = [result stringForColumnIndex:18];
            NSString *price2 = [result stringForColumnIndex:19];
            NSString *price3 = [result stringForColumnIndex:20];
            NSString *price4 = [result stringForColumnIndex:21];
            NSString *price5 = [result stringForColumnIndex:22];
            NSString *price6 = [result stringForColumnIndex:23];
            NSString *totalPrice = [result stringForColumnIndex:24];
            NSString *carryOver = [result stringForColumnIndex:25];
            
            Number *number = [[Number alloc] init:[number1 intValue]
                                             num2:[number2 intValue]
                                             num3:[number3 intValue]
                                             num4:[number4 intValue]
                                             num5:[number5 intValue]
                                             num6:[number6 intValue]
                                             num7:[number7 intValue]
                                            numB1:[numberB1  intValue]
                                            numB2:[numberB2  intValue]];
            
            Result *result = [[Result alloc] init:number
                                            count:count
                                             date:date
                                             hit1:hit1
                                             hit2:hit2
                                             hit3:hit3
                                             hit4:hit4
                                             hit5:hit5
                                             hit6:hit6
                                           price1:price1
                                           price2:price2
                                           price3:price3
                                           price4:price4
                                           price5:price5
                                           price6:price6
                                       totalPrice:totalPrice
                                        carryOver:carryOver];
            
            if (![self existCount:7 count:[count intValue]]) {
                [resultsLoto7 addObject:result];
            } else {
                break;
            }
        }
        
        if (resultsLoto7.count > 0) {
            [self saveResults:[resultsLoto7 copy] type:L7];
        }
    }
    {
        NSMutableArray *results = [[NSMutableArray alloc] init];
        // list up
        NSString *selectSql = @"SELECT * from loto6 order by CAST(count AS SIGNED) DESC;";
        
        FMResultSet *result = [db executeQuery:selectSql];
        while([result next]) {
            NSString *count = [result stringForColumnIndex:1];
            NSString *date = [result stringForColumnIndex:2];
            NSString *number1 = [result stringForColumnIndex:3];
            NSString *number2 = [result stringForColumnIndex:4];
            NSString *number3 = [result stringForColumnIndex:5];
            NSString *number4 = [result stringForColumnIndex:6];
            NSString *number5 = [result stringForColumnIndex:7];
            NSString *number6 = [result stringForColumnIndex:8];
            NSString *numberB1 = [result stringForColumnIndex:9];
            NSString *hit1 = [result stringForColumnIndex:10];
            NSString *hit2 = [result stringForColumnIndex:11];
            NSString *hit3 = [result stringForColumnIndex:12];
            NSString *hit4 = [result stringForColumnIndex:13];
            NSString *hit5 = [result stringForColumnIndex:14];
            NSString *price1 = [result stringForColumnIndex:15];
            NSString *price2 = [result stringForColumnIndex:16];
            NSString *price3 = [result stringForColumnIndex:17];
            NSString *price4 = [result stringForColumnIndex:18];
            NSString *price5 = [result stringForColumnIndex:19];
            NSString *totalPrice = [result stringForColumnIndex:20];
            NSString *carryOver = [result stringForColumnIndex:21];
            
            Number *number = [[Number alloc] init:[number1 intValue]
                                             num2:[number2 intValue]
                                             num3:[number3 intValue]
                                             num4:[number4 intValue]
                                             num5:[number5 intValue]
                                             num6:[number6 intValue]
                                             num7: 0
                                            numB1:[numberB1 intValue]
                                            numB2:0];
            
            Result *result = [[Result alloc] init:number
                                            count:count
                                             date:date
                                             hit1:hit1
                                             hit2:hit2
                                             hit3:hit3
                                             hit4:hit4
                                             hit5:hit5
                                             hit6:@""
                                           price1:price1
                                           price2:price2
                                           price3:price3
                                           price4:price4
                                           price5:price5
                                           price6:@""
                                       totalPrice:totalPrice
                                        carryOver:carryOver];
            
            if (![self existCount:6 count:[count intValue]]) {
                [results addObject:result];
            } else {
                break;
            }
        }
        
        if (results.count > 0) {
            [self saveResults:[results copy] type:L6];
        }
    }
    {
        NSMutableArray *results = [[NSMutableArray alloc] init];
        // list up
        NSString *selectSql = @"SELECT * from miniloto order by CAST(count AS SIGNED) DESC;";
        
        FMResultSet *result = [db executeQuery:selectSql];
        while([result next]) {
            NSString *count = [result stringForColumnIndex:1];
            NSString *date = [result stringForColumnIndex:2];
            NSString *number1 = [result stringForColumnIndex:3];
            NSString *number2 = [result stringForColumnIndex:4];
            NSString *number3 = [result stringForColumnIndex:5];
            NSString *number4 = [result stringForColumnIndex:6];
            NSString *number5 = [result stringForColumnIndex:7];
            NSString *numberB1 = [result stringForColumnIndex:8];
            NSString *hit1 = [result stringForColumnIndex:9];
            NSString *hit2 = [result stringForColumnIndex:10];
            NSString *hit3 = [result stringForColumnIndex:11];
            NSString *hit4 = [result stringForColumnIndex:12];
            NSString *price1 = [result stringForColumnIndex:13];
            NSString *price2 = [result stringForColumnIndex:14];
            NSString *price3 = [result stringForColumnIndex:15];
            NSString *price4 = [result stringForColumnIndex:16];
            NSString *totalPrice = [result stringForColumnIndex:17];
            
            Number *number = [[Number alloc] init:[number1 intValue]
                                             num2:[number2 intValue]
                                             num3:[number3 intValue]
                                             num4:[number4 intValue]
                                             num5:[number5 intValue]
                                             num6:0
                                             num7:0
                                            numB1:[numberB1 intValue]
                                            numB2:0];
            
            Result *result = [[Result alloc] init:number
                                            count:count
                                             date:date
                                             hit1:hit1
                                             hit2:hit2
                                             hit3:hit3
                                             hit4:hit4
                                             hit5:@""
                                             hit6:@""
                                           price1:price1
                                           price2:price2
                                           price3:price3
                                           price4:price4
                                           price5:@""
                                           price6:@""
                                       totalPrice:totalPrice
                                        carryOver:@""];
            
            if (![self existCount:5 count:[count intValue]]) {
                [results addObject:result];
            } else {
                break;
            }
        }
        
        if (results.count > 0) {
            [self saveResults:[results copy] type:L5];
        }
    }
    {
        NSMutableArray *results = [[NSMutableArray alloc] init];
        // list up
        NSString *selectSql = @"SELECT * from numbers4 order by CAST(count AS SIGNED) DESC;";
        
        
        FMResultSet *result = [db executeQuery:selectSql];
        while([result next]) {
            NSString *count = [result stringForColumnIndex:1];
            NSString *date = [result stringForColumnIndex:2];
            NSString *number1 = [result stringForColumnIndex:3];
            NSString *number2 = [result stringForColumnIndex:4];
            NSString *number3 = [result stringForColumnIndex:5];
            NSString *number4 = [result stringForColumnIndex:6];
            NSString *hit1 = [result stringForColumnIndex:7];
            NSString *hit2 = [result stringForColumnIndex:8];
            NSString *hit3 = [result stringForColumnIndex:9];
            NSString *hit4 = [result stringForColumnIndex:10];
            NSString *price1 = [result stringForColumnIndex:11];
            NSString *price2 = [result stringForColumnIndex:12];
            NSString *price3 = [result stringForColumnIndex:13];
            NSString *price4 = [result stringForColumnIndex:14];

            
            Number *number = [[Number alloc] init:[number1 intValue]
                                             num2:[number2 intValue]
                                             num3:[number3 intValue]
                                             num4:[number4 intValue]
                                             num5:0
                                             num6:0
                                             num7:0
                                            numB1:0
                                            numB2:0];
            
            Result *result = [[Result alloc] init:number
                                            count:count
                                             date:date
                                             hit1:hit1
                                             hit2:hit2
                                             hit3:hit3
                                             hit4:hit4
                                             hit5:@""
                                             hit6:@""
                                           price1:price1
                                           price2:price2
                                           price3:price3
                                           price4:price4
                                           price5:@""
                                           price6:@""
                                       totalPrice:@""
                                        carryOver:@""];
            
            if (![self existCount:4 count:[count intValue]]) {
                [results addObject:result];
            } else {
                break;
            }
        }
        
        if (results.count > 0) {
            [self saveResults:[results copy] type:N4];
        }
    }
    {
        NSMutableArray *results = [[NSMutableArray alloc] init];
        // list up
        NSString *selectSql = @"SELECT * from numbers3 order by CAST(count AS SIGNED) DESC;";
        
        FMResultSet *result = [db executeQuery:selectSql];
        while([result next]) {
            NSString *count = [result stringForColumnIndex:1];
            NSString *date = [result stringForColumnIndex:2];
            NSString *number1 = [result stringForColumnIndex:3];
            NSString *number2 = [result stringForColumnIndex:4];
            NSString *number3 = [result stringForColumnIndex:5];
            NSString *hit1 = [result stringForColumnIndex:6];
            NSString *hit2 = [result stringForColumnIndex:7];
            NSString *hit3 = [result stringForColumnIndex:8];
            NSString *hit4 = [result stringForColumnIndex:9];
            NSString *hit5 = [result stringForColumnIndex:10];
            NSString *price1 = [result stringForColumnIndex:11];
            NSString *price2 = [result stringForColumnIndex:12];
            NSString *price3 = [result stringForColumnIndex:13];
            NSString *price4 = [result stringForColumnIndex:14];
            NSString *price5 = [result stringForColumnIndex:15];
            
            Number *number = [[Number alloc] init:[number1 intValue]
                                             num2:[number2 intValue]
                                             num3:[number3 intValue]
                                             num4:0
                                             num5:0
                                             num6:0
                                             num7:0
                                            numB1:0
                                            numB2:0];
            
            Result *result = [[Result alloc] init:number
                                            count:count
                                             date:date
                                             hit1:hit1
                                             hit2:hit2
                                             hit3:hit3
                                             hit4:hit4
                                             hit5:hit5
                                             hit6:@""
                                           price1:price1
                                           price2:price2
                                           price3:price3
                                           price4:price4
                                           price5:price5
                                           price6:@""
                                       totalPrice:@""
                                        carryOver:@""];
            
            if (![self existCount:3 count:[count intValue]]) {
                [results addObject:result];
            } else {
                break;
            }
        }
        
        if (results.count > 0) {
            [self saveResults:[results copy] type:N3];
        }
    }
    [db close];
}

- (BOOL) existCount: (int) type count :(int) count
{
    NSString *entity = @"";
    switch (type) {
        case LOTTO7:
            entity = L7;
            break;
        case LOTTO6:
            entity = L6;
            break;
        case MINILOTTO:
            entity = L5;
            break;
        case NUMBER4:
            entity = N4;
            break;
        case NUMBER3:
            entity = N3;
            break;
        default:
            return 0;
    }
    
    // 検索リクエストのオブジェクトを生成します。
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    // 対象エンティティを指定します。
    NSEntityDescription *entitys
    = [NSEntityDescription entityForName:entity inManagedObjectContext:self.context];
    [fetchRequest setEntity:entitys];
    
    //    // 検索条件を指定します。
    NSPredicate *pred
    = [NSPredicate predicateWithFormat:@"%K = %d", @"count", count];
    [fetchRequest setPredicate:pred];
    
    // 上記リクエストを元に件数を取得します。
    NSError *error = nil;
    NSUInteger ret = [self.context countForFetchRequest:fetchRequest error:&error];
    
    // エラーがあった場合には、エラー情報を表示する。
    if (error) {
        NSLog(@"error occurred. error = %@", error);
    }
    
    if (ret > 0) {
        return true;
    } else {
        return false;
    }
}

- (NSMutableArray *) updateDatabase: (NSString *)entity
{
    NSArray *queryResult = [self getAllFromEntity:entity withCountSort:YES];
    NSMutableArray *counts = [[NSMutableArray alloc] init];
    
    if (!queryResult) {
        return nil;
    }
    
    if ([L7 isEqualToString:entity]) {
        for (Lotto7 *result in queryResult) {
            [counts addObject:result.count];
        }
    }
    
    else if ([L6 isEqualToString:entity]) {
        for (Lotto6 *result in queryResult) {
            [counts addObject:result.count];
        }
    }
    
    else if ([L5 isEqualToString:entity]) {
        for (Lotto5 *result in queryResult) {
            [counts addObject:result.count];
        }
    }
    
    else if ([N4 isEqualToString:entity]) {
        for (Numbers4 *result in queryResult) {
            [counts addObject:result.count];
        }
    }
    
    if ([N3 isEqualToString:entity]) {
        for (Numbers3 *result in queryResult) {
            [counts addObject:result.count];
        }
    }
    
    return counts;
}

#pragma mark Query -

- (NSArray *) getAllFromEntityByType: (int) type
{
    NSString *entity = @"";
    switch (type) {
        case LOTTO7:
            entity = L7;
            break;
        case LOTTO6:
            entity = L6;
            break;
        case MINILOTTO:
            entity = L5;
            break;
        case NUMBER4:
            entity = N4;
            break;
        case NUMBER3:
            entity = N3;
            break;
        case PURCHASED_LOTTO7:
            entity = PL7;
            break;
        case PURCHASED_LOTTO6:
            entity = PL6;
            break;
        case PURCHASED_MINILOTTO:
            entity = PL5;
            break;
        case PURCHASED_NUMBER4:
            entity = PN4;
            break;
        case PURCHASED_NUMBER3:
            entity = PN3;
            break;
        default:
            return nil;
    }
    
    if (type >= PURCHASED_NUMBER3) {
        return [self getAllFromEntity:entity withCountSort:NO];
    }
    
    return [self getAllFromEntity:entity withCountSort:YES];
}

- (NSArray *) getAllFromEntity: (NSString *)entity withCountSort:(BOOL)hasCount
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:entity inManagedObjectContext:self.context]];
    
    //count.intValueで取得した場合、ソートの挙動が怪しいので、countで取得して事後ソート
    NSString* sortKey = hasCount ? @"count" : @"date";
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSError *error;
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    //降順ソート
    if (results.count!=0){
        results = [results sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
            return [(NSString*)[obj2 valueForKey:sortKey] compare:(NSString*)[obj1 valueForKey:sortKey] options:NSNumericSearch];
        }];
    }
    return [results copy];
}

- (NSArray *) getLatestOfType: (int) type
{
    
    NSString *entity = L7;
    
    switch (type) {
        case LOTTO6:
            entity = L6;
            break;
            
        case MINILOTTO:
            entity = L5;
            break;
            
        case NUMBER4:
            entity = N4;
            break;
            
        case NUMBER3:
            entity = N3;
            break;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:entity inManagedObjectContext:self.context]];
    
    //count.intValueのソートが効かないため、全件取得後、最新のみに絞る
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"count" ascending:NO];
    
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSError *error;
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
    //ソートして最新1件だけ取得する暫定対応
    if (results.count!=0){
        results = [results sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
            return [(NSString*)[obj2 valueForKey:@"count"] compare:(NSString*)[obj1 valueForKey:@"count"] options:NSNumericSearch];
        }];
        results = [NSArray arrayWithObject:[results firstObject]];
    }
    return [results copy];
    
}

- (NSArray *) getAllFromEntityResultByType: (int)type
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    NSArray *results = [self getAllFromEntityByType:type];
    
    switch (type) {
        case LOTTO7:
        {
            for (Lotto7 *item in results) {
                Number *number = [[Number alloc] init:[item.number_1 intValue]
                                                 num2:[item.number_2 intValue]
                                                 num3:[item.number_3 intValue]
                                                 num4:[item.number_4 intValue]
                                                 num5:[item.number_5 intValue]
                                                 num6:[item.number_6 intValue]
                                                 num7:[item.number_7 intValue]
                                                numB1:[item.number_bonus_1 intValue]
                                                numB2:[item.number_bonus_2 intValue]];
                Result *result = [[Result alloc] init:number
                                                count:item.count
                                                 date:item.date
                                                 hit1:item.hit_1
                                                 hit2:item.hit_2
                                                 hit3:item.hit_3
                                                 hit4:item.hit_4
                                                 hit5:item.hit_5
                                                 hit6:item.hit_6
                                               price1:item.prize_1
                                               price2:item.prize_2
                                               price3:item.prize_3
                                               price4:item.prize_4
                                               price5:item.prize_5
                                               price6:item.prize_6
                                           totalPrice:item.prize_total
                                            carryOver:item.prize_carry_over];
                
                [returnArray addObject:result];
            }
        }
        break;
        
        case LOTTO6:
        {
            for (Lotto6 *item in results) {
                Number *number = [[Number alloc] init:[item.number_1 intValue]
                                                 num2:[item.number_2 intValue]
                                                 num3:[item.number_3 intValue]
                                                 num4:[item.number_4 intValue]
                                                 num5:[item.number_5 intValue]
                                                 num6:[item.number_6 intValue]
                                                 num7:0
                                                numB1:[item.number_bonus_1 intValue]
                                                numB2:0];
                Result *result = [[Result alloc] init:number
                                                count:item.count
                                                 date:item.date
                                                 hit1:item.hit_1
                                                 hit2:item.hit_2
                                                 hit3:item.hit_3
                                                 hit4:item.hit_4
                                                 hit5:item.hit_5
                                                 hit6:@""
                                               price1:item.prize_1
                                               price2:item.prize_2
                                               price3:item.prize_3
                                               price4:item.prize_4
                                               price5:item.prize_5
                                               price6:@""
                                           totalPrice:item.prize_total
                                            carryOver:item.prize_carry_over];
                
                [returnArray addObject:result];
            }
        }
        break;
        
        case MINILOTTO:
        {
            for (Lotto5 *item in results) {
                Number *number = [[Number alloc] init:[item.number_1 intValue]
                                                 num2:[item.number_2 intValue]
                                                 num3:[item.number_3 intValue]
                                                 num4:[item.number_4 intValue]
                                                 num5:[item.number_5 intValue]
                                                 num6:0
                                                 num7:0
                                                numB1:[item.number_bonus_1 intValue]
                                                numB2:0];
                Result *result = [[Result alloc] init:number
                                                count:item.count
                                                 date:item.date
                                                 hit1:item.hit_1
                                                 hit2:item.hit_2
                                                 hit3:item.hit_3
                                                 hit4:item.hit_4
                                                 hit5:@""
                                                 hit6:@""
                                               price1:item.prize_1
                                               price2:item.prize_2
                                               price3:item.prize_3
                                               price4:item.prize_4
                                               price5:@""
                                               price6:@""
                                           totalPrice:item.prize_total
                                            carryOver:@""];
                
                [returnArray addObject:result];
            }
        }
        break;
        
        case NUMBER4:
        {
            for (Numbers4 *item in results) {
                Number *number = [[Number alloc] init:[item.number_1 intValue]
                                                 num2:[item.number_2 intValue]
                                                 num3:[item.number_3 intValue]
                                                 num4:[item.number_4 intValue]
                                                 num5:0
                                                 num6:0
                                                 num7:0
                                                numB1:0
                                                numB2:0];
                Result *result = [[Result alloc] init:number
                                                count:item.count
                                                 date:item.date
                                                 hit1:item.hit_1
                                                 hit2:item.hit_2
                                                 hit3:item.hit_3
                                                 hit4:item.hit_4
                                                 hit5:@""
                                                 hit6:@""
                                               price1:item.prize_1
                                               price2:item.prize_2
                                               price3:item.prize_3
                                               price4:item.prize_4
                                               price5:@""
                                               price6:@""
                                           totalPrice:item.prize_total
                                            carryOver:@""];
                
                [returnArray addObject:result];
            }
        }
        break;
        
        case NUMBER3:
        {
            for (Numbers3 *item in results) {
                Number *number = [[Number alloc] init:[item.number_1 intValue]
                                                 num2:[item.number_2 intValue]
                                                 num3:[item.number_3 intValue]
                                                 num4:0
                                                 num5:0
                                                 num6:0
                                                 num7:0
                                                numB1:0
                                                numB2:0];
                Result *result = [[Result alloc] init:number
                                                count:item.count
                                                 date:item.date
                                                 hit1:item.hit_1
                                                 hit2:item.hit_2
                                                 hit3:item.hit_3
                                                 hit4:item.hit_4
                                                 hit5:item.hit_5
                                                 hit6:@""
                                               price1:item.prize_1
                                               price2:item.prize_2
                                               price3:item.prize_3
                                               price4:item.prize_4
                                               price5:item.prize_5
                                               price6:@""
                                           totalPrice:item.prize_total
                                            carryOver:@""];
                
                [returnArray addObject:result];
            }
        }
        break;
        
    }
    
    return [returnArray copy];
}

#pragma mark Prize Check -
- (NSArray *) queryHit: (NSArray *) number ofType: (int)type
{

	switch (type) {
        
    	case NUMBER3:
    	{
        	NSArray *p1 = [self check1stPrizeNumbers3:number];
        	NSArray *p2 = [self check2ndPrizeNumbers3:number];
        	NSArray *p3 = [self check3rdPrizeNumbers3:number];
            
            NSMutableArray *prize1 = [[NSMutableArray alloc] init];
            NSMutableArray *prize2 = [[NSMutableArray alloc] init];
            NSMutableArray *prize3 = [[NSMutableArray alloc] init];
            
            for (Numbers3 *item in p1)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize1 addObject:result];
            }
            
            for (Numbers3 *item in p2)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize2 addObject:result];
            }
            
            for (Numbers3 *item in p3)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize3 addObject:result];
            }
            
        	return @[prize1, prize2, prize3];
    	}
        
        
    	case NUMBER4:
    	{
        	NSArray *p1 = [self check1stPrizeNumbers4:number];
        	NSArray *p2 = [self check2ndPrizeNumbers4:number];
            
            NSMutableArray *prize1 = [[NSMutableArray alloc] init];
            NSMutableArray *prize2 = [[NSMutableArray alloc] init];
            
            for (Numbers4 *item in p1)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize1 addObject:result];
            }
            
            for (Numbers4 *item in p2)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize2 addObject:result];
            }
            
        	return @[prize1, prize2];
    	}
        
    	case MINILOTTO:
    	{
        	NSArray *p1 = [self check1stPrizeLotto5:number];
        	NSArray *p2 = [self check2ndPrizeLotto5:number];
        	NSArray *p3 = [self check3rdPrizeLotto5:number prize1:p1 prize2:p2];
        	NSArray *p4 = [self check4thPrizeLotto5:number prize1:p1 prize2:p2 prize3:p3];
            
        	NSMutableArray *prize1 = [[NSMutableArray alloc] init];
        	NSMutableArray *prize2 = [[NSMutableArray alloc] init];
        	NSMutableArray *prize3 = [[NSMutableArray alloc] init];
        	NSMutableArray *prize4 = [[NSMutableArray alloc] init];
            
        	for (Lotto5 *item in p1)
        	{
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize1 addObject:result];
        	}
            
        	for (Lotto5 *item in p2)
        	{
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize2 addObject:result];
        	}
            
        	for (Lotto5 *item in p3)
        	{
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize3 addObject:result];
        	}
            
        	for (Lotto5 *item in p4)
        	{
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize4 addObject:result];
        	}
            
            
        	return @[prize1, prize2, prize3, prize4];
    	}
        
    	case LOTTO6:
    	{
        	NSArray *p1 = [self check1stPrizeLotto6:number];
        	NSArray *p2 = [self check2ndPrizeLotto6:number];
            NSArray *p3 = [self check3rdPrizeLotto6:number prize1:p1 prize2:p2];
        	NSArray *p4 = [self check4thPrizeLotto6:number prize1:p1 prize2:p2 prize3:p3];
        	NSArray *p5 = [self check5thPrizeLotto6:number prize1:p1 prize2:p2 prize3:p3 prize4:p4];
            
            NSMutableArray *prize1 = [[NSMutableArray alloc] init];
        	NSMutableArray *prize2 = [[NSMutableArray alloc] init];
        	NSMutableArray *prize3 = [[NSMutableArray alloc] init];
        	NSMutableArray *prize4 = [[NSMutableArray alloc] init];
        	NSMutableArray *prize5 = [[NSMutableArray alloc] init];
            
            for (Lotto6 *item in p1)
        	{
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize1 addObject:result];
            }
            
            for (Lotto6 *item in p2)
        	{
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize2 addObject:result];
            }
            
            for (Lotto6 *item in p3)
        	{
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize3 addObject:result];
            }
            
            for (Lotto6 *item in p4)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize4 addObject:result];
            }
        
            for (Lotto6 *item in p5)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize5 addObject:result];
            }
        
            return @[prize1, prize2, prize3, prize4, prize5];
    }
    
        case LOTTO7:
        {
            NSArray *p1 = [self check1stPrizeLotto7:number];
            NSArray *p2 = [self check2ndPrizeLotto7:number];
            NSArray *p3 = [self check3rdPrizeLotto7:number prize1:p1 prize2:p2];
            NSArray *p4 = [self check4thPrizeLotto7:number prize1:p1 prize2:p2 prize3:p3];
            NSArray *p5 = [self check5thPrizeLotto7:number prize1:p1 prize2:p2 prize3:p3 prize4:p4];
            NSArray *p6 = [self check6thPrizeLotto7:number prize1:p1 prize2:p2 prize3:p3 prize4:p4 prize5:p5];
            
            NSMutableArray *prize1 = [[NSMutableArray alloc] init];
            NSMutableArray *prize2 = [[NSMutableArray alloc] init];
            NSMutableArray *prize3 = [[NSMutableArray alloc] init];
            NSMutableArray *prize4 = [[NSMutableArray alloc] init];
            NSMutableArray *prize5 = [[NSMutableArray alloc] init];
            NSMutableArray *prize6 = [[NSMutableArray alloc] init];
            
            for (Lotto7 *item in p1)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize1 addObject:result];
            }
            
            for (Lotto7 *item in p2)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize2 addObject:result];
            }
            
            for (Lotto7 *item in p3)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize3 addObject:result];
            }
            
            for (Lotto7 *item in p4)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize4 addObject:result];
            }
            
            for (Lotto7 *item in p5)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize5 addObject:result];
            }
            
            for (Lotto7 *item in p6)
            {
                Result *result = [[Result alloc] init];
                result.count = item.count;
                result.date = item.date;
                [prize6 addObject:result];
            }
            
            return @[prize1, prize2, prize3, prize4, prize5, prize6];
        }
    }

    return NULL;
}

#pragma mark Number3
- (NSArray *) check3rdPrizeNumbers3: (NSArray*) numbers
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:N3 inManagedObjectContext:self.context]];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i", [numbers[1] integerValue], [numbers[2] integerValue]]];
    
    NSError *error;
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
    return results;
}

- (NSArray *) check2ndPrizeNumbers3: (NSArray*) numbers
{
    if ([numbers[0] integerValue] == [numbers[1] integerValue] && [numbers[0] integerValue] == [numbers[2] integerValue]) {
        return [[NSArray alloc] init];
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:N3 inManagedObjectContext:self.context]];
    
    NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[0] integerValue], [numbers[2] integerValue], [numbers[1] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[1] integerValue], [numbers[0] integerValue], [numbers[2] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[1] integerValue], [numbers[2] integerValue], [numbers[0] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[2] integerValue], [numbers[0] integerValue], [numbers[1] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[2] integerValue], [numbers[1] integerValue], [numbers[0] integerValue]]];
    
    [fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
    NSError *error;
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
    return results;
}

- (NSArray *) check1stPrizeNumbers3: (NSArray*) numbers
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:N3 inManagedObjectContext:self.context]];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]];
    
    NSError *error;
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
    return results;
}

#pragma mark Number4
- (NSArray *) check1stPrizeNumbers4: (NSArray*) numbers
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:N4 inManagedObjectContext:self.context]];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]];
    
    NSError *error;
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
    return results;
}

- (NSArray *) check2ndPrizeNumbers4: (NSArray*) numbers
{
    if ([numbers[0] integerValue] == [numbers[1] integerValue] && [numbers[0] integerValue] == [numbers[2] integerValue] && [numbers[0] integerValue] == [numbers[3] integerValue]) {
        return [[NSArray alloc] init];
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:N4 inManagedObjectContext:self.context]];
    
    NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[3] integerValue], [numbers[2] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[2] integerValue], [numbers[1] integerValue], [numbers[3] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[1] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[3] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[3] integerValue], [numbers[2] integerValue], [numbers[1] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[1] integerValue], [numbers[0] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[1] integerValue], [numbers[0] integerValue], [numbers[3] integerValue], [numbers[2] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[1] integerValue], [numbers[2] integerValue], [numbers[0] integerValue], [numbers[3] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[0] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[1] integerValue], [numbers[3] integerValue], [numbers[0] integerValue], [numbers[2] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[1] integerValue], [numbers[3] integerValue], [numbers[2] integerValue], [numbers[0] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[2] integerValue], [numbers[0] integerValue], [numbers[1] integerValue], [numbers[3] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[2] integerValue], [numbers[0] integerValue], [numbers[3] integerValue], [numbers[1] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[2] integerValue], [numbers[1] integerValue], [numbers[0] integerValue], [numbers[3] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[2] integerValue], [numbers[1] integerValue], [numbers[3] integerValue], [numbers[0] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[2] integerValue], [numbers[3] integerValue], [numbers[0] integerValue], [numbers[1] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[2] integerValue], [numbers[3] integerValue], [numbers[1] integerValue], [numbers[0] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[3] integerValue], [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[3] integerValue], [numbers[0] integerValue], [numbers[2] integerValue], [numbers[1] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[3] integerValue], [numbers[1] integerValue], [numbers[0] integerValue], [numbers[2] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[3] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[0] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[3] integerValue], [numbers[2] integerValue], [numbers[0] integerValue], [numbers[1] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[3] integerValue], [numbers[2] integerValue], [numbers[1] integerValue], [numbers[0] integerValue]]];
    
    [fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
    NSError *error;
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
    return results;
}

#pragma mark MiniLotto

- (NSArray *) check4thPrizeLotto5: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2 prize3: (NSArray *)prize3
{
	NSMutableArray *results = [[NSMutableArray alloc] init];
    
	[results addObjectsFromArray:[self check4thLotto5:@[numbers[0], numbers[1], numbers[2]] prize1:prize1 prize2:prize2 prize3:prize3]]; //34
    
    [results addObjectsFromArray:[self check4thLotto5:@[numbers[0], numbers[1], numbers[3]] prize1:prize1  prize2:prize2 prize3:prize3]]; //24
    [results addObjectsFromArray:[self check4thLotto5:@[numbers[0], numbers[2], numbers[3]] prize1:prize1  prize2:prize2 prize3:prize3]]; //14
    [results addObjectsFromArray:[self check4thLotto5:@[numbers[1], numbers[2], numbers[3]] prize1:prize1  prize2:prize2 prize3:prize3]]; //04
    
    [results addObjectsFromArray:[self check4thLotto5:@[numbers[0], numbers[1], numbers[4]] prize1:prize1  prize2:prize2 prize3:prize3]]; //23
    [results addObjectsFromArray:[self check4thLotto5:@[numbers[0], numbers[2], numbers[4]] prize1:prize1  prize2:prize2 prize3:prize3]]; //13
    [results addObjectsFromArray:[self check4thLotto5:@[numbers[1], numbers[2], numbers[4]] prize1:prize1  prize2:prize2 prize3:prize3]]; //03
    
    [results addObjectsFromArray:[self check4thLotto5:@[numbers[0], numbers[3], numbers[4]] prize1:prize1  prize2:prize2 prize3:prize3]]; //12
    [results addObjectsFromArray:[self check4thLotto5:@[numbers[1], numbers[3], numbers[4]] prize1:prize1   prize2:prize2 prize3:prize3]]; //02
    [results addObjectsFromArray:[self check4thLotto5:@[numbers[2], numbers[3], numbers[4]] prize1:prize1  prize2:prize2 prize3:prize3]]; //01
    
	return results;
}

- (NSArray *) check4thLotto5: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2 prize3: (NSArray *)prize3
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L5 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //45
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //35
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //25
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //15
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //34
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //24
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //14
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //23
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //13
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //12
    
	[fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	NSMutableArray *returnResults = [[NSMutableArray alloc] init];
    
	for (Lotto5 *item in results) {
        
    	NSPredicate *predicate = [NSPredicate predicateWithFormat: @"count == %@", item.count];
    	NSArray *temp = [prize1 filteredArrayUsingPredicate:predicate];
        
    	if ([temp count] > 0) {
        	continue;
    	}
        
        temp = [prize2 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
        temp = [prize3 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
        	continue;
    	}
        
    	[returnResults addObject:item];
	}
    
	return returnResults;
}

- (NSArray *) check3rdPrizeLotto5: (NSArray*) numbers prize1: (NSArray *)prize1  prize2: (NSArray *)prize2
{
	NSMutableArray *results = [[NSMutableArray alloc] init];
    
	[results addObjectsFromArray:[self check3rdLotto5:@[numbers[0], numbers[1], numbers[2], numbers[3]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto5:@[numbers[0], numbers[1], numbers[2], numbers[4]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto5:@[numbers[0], numbers[1], numbers[3], numbers[4]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto5:@[numbers[0], numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto5:@[numbers[1], numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2]];
    
	return results;
}

- (NSArray *) check3rdLotto5: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L5 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]];
    
    
	[fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	NSMutableArray *returnResults = [[NSMutableArray alloc] init];
    
	for (Lotto5 *item in results) {
        
    	NSPredicate *predicate = [NSPredicate predicateWithFormat: @"count == %@", item.count];
    	NSArray *temp = [prize1 filteredArrayUsingPredicate:predicate];
        
    	if ([temp count] > 0) {
        	continue;
    	}
        
        temp = [prize2 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
    	[returnResults addObject:item];
	}
    
	return returnResults;
}

- (NSArray *) check2ndPrizeLotto5: (NSArray*) numbers
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L5 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    
	[fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	return results;
}

- (NSArray *) check1stPrizeLotto5: (NSArray*) numbers
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L5 inManagedObjectContext:self.context]];
    
	[fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i  AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
     
     NSError *error;
     NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
     
     return results;
     }
     


#pragma mark Lotto6

- (NSArray *) check5thPrizeLotto6: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2 prize3: (NSArray *)prize3 prize4: (NSArray *)prize4
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[0], numbers[1], numbers[2]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //456
    
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[0], numbers[1], numbers[3]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //356
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[0], numbers[2], numbers[3]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //256
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[1], numbers[2], numbers[3]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //156
    
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[0], numbers[1], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //346
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[0], numbers[2], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //246
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[1], numbers[2], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //146
    
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[0], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //236
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[1], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //136
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //126
    
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[0], numbers[1], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //345
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[0], numbers[2], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //245
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[1], numbers[2], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //145
    
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[0], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //235
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[1], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //135
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[2], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //125
    
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[0], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //234
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[1], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //134
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[2], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //124
    
    [results addObjectsFromArray:[self check5thLotto6:@[numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4]]; //123
 
    return results;
}

- (NSArray *) check5thLotto6: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2 prize3: (NSArray *)prize3 prize4: (NSArray *)prize4
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:L6 inManagedObjectContext:self.context]];
    
    NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //456
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //256
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //156
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //246
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //146
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //236
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //136
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //126
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //245
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //145
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //235
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //135
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //125
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //234
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //134
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //124
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue]]]; //123
    
    [fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
    NSError *error;
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *returnResults = [[NSMutableArray alloc] init];
    
    for (Lotto6 *item in results) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"count == %@", item.count];
        NSArray *temp = [prize1 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
        temp = [prize2 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
        temp = [prize3 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
        temp = [prize4 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
        [returnResults addObject:item];
    }
    
    return returnResults;
}

- (NSArray *) check4thPrizeLotto6: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2 prize3: (NSArray *)prize3
{
	NSMutableArray *results = [[NSMutableArray alloc] init];
    
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[0], numbers[1], numbers[2], numbers[3]] prize1:prize1 prize2:prize2 prize3:prize3]]; //45
    
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[0], numbers[1], numbers[2], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3]]; //35
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[0], numbers[1], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3]]; //25
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[0], numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3]]; //15
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[1], numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3]]; //05
    
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[0], numbers[1], numbers[2], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //34
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[0], numbers[1], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //24
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[0], numbers[2], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //14
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[1], numbers[2], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //04
    
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[0], numbers[1], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //23
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[0], numbers[2], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //13
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[1], numbers[2], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //03
    
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[0], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //12
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[1], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //02
	[results addObjectsFromArray:[self check4thLotto6:@[numbers[2], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //01
    
	return results;
}

- (NSArray *) check4thLotto6: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2 prize3: (NSArray *)prize3
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L6 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //56
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //46
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //36
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //26
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //16
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //45
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //35
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //25
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //15
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //34
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //24
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //14
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //23
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //13
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //12
    
    
	[fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	NSMutableArray *returnResults = [[NSMutableArray alloc] init];
    
	for (Lotto6 *item in results) {
        
    	NSPredicate *predicate = [NSPredicate predicateWithFormat: @"count == %@", item.count];
    	NSArray *temp = [prize1 filteredArrayUsingPredicate:predicate];
        
    	if ([temp count] > 0) {
        	continue;
    	}
        
        temp = [prize2 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
    	temp = [prize3 filteredArrayUsingPredicate:predicate];
        
    	if ([temp count] > 0) {
        	continue;
    	}
        
    	[returnResults addObject:item];
	}
    
	return returnResults;
}

- (NSArray *) check3rdPrizeLotto6: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2
{
	NSMutableArray *results = [[NSMutableArray alloc] init];
    
	[results addObjectsFromArray:[self check3rdLotto6:@[numbers[0], numbers[1], numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto6:@[numbers[0], numbers[1], numbers[2], numbers[3], numbers[5]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto6:@[numbers[0], numbers[1], numbers[2], numbers[4], numbers[5]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto6:@[numbers[0], numbers[1], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto6:@[numbers[0], numbers[2], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto6:@[numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2]];
    
	return results;
}

- (NSArray *) check3rdLotto6: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L6 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]];
    
    [fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
    NSError *error;
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *returnResults = [[NSMutableArray alloc] init];
    
    for (Lotto6 *item in results) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"count == %@", item.count];
        NSArray *temp = [prize1 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
        temp = [prize2 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
        [returnResults addObject:item];
    }
    
    return returnResults;
}

- (NSArray *) check1stPrizeLotto6: (NSArray*) numbers
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L6 inManagedObjectContext:self.context]];
    
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    
    NSError *error;
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
    return results;
}

- (NSArray *) check2ndPrizeLotto6: (NSArray*) numbers
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L6 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    
	[fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	return results;
}

#pragma mark Lotto7

- (NSArray *) check1stPrizeLotto7: (NSArray*) numbers
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L7 inManagedObjectContext:self.context]];
    
	[fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	return results;
}

- (NSArray *) check2ndPrizeLotto7: (NSArray*) numbers
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L7 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    // １がボーナス１の代わり
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // ２がボーナス１の代わり
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // ３がボーナス１の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // 4がボーナス１の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // 5がボーナス１の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // 6がボーナス１の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // 7がボーナス１の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];

    
    // 1がボーナス2の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // 2がボーナス2の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // 3がボーナス2の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // 4がボーナス2の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // 5がボーナス2の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // 6がボーナス2の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
    // 7がボーナス2の代わり
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue], [numbers[6] integerValue]]];
    
	[fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	return results;
}

- (NSArray *) check3rdPrizeLotto7: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2
{
	NSMutableArray *results = [[NSMutableArray alloc] init];
    
	[results addObjectsFromArray:[self check3rdLotto7:@[numbers[0], numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto7:@[numbers[0], numbers[1], numbers[2], numbers[3], numbers[4], numbers[6]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto7:@[numbers[0], numbers[1], numbers[2], numbers[3], numbers[5], numbers[6]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto7:@[numbers[0], numbers[1], numbers[2], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto7:@[numbers[0], numbers[1], numbers[3], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2]];
	[results addObjectsFromArray:[self check3rdLotto7:@[numbers[0], numbers[2], numbers[3], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2]];
    [results addObjectsFromArray:[self check3rdLotto7:@[numbers[1], numbers[2], numbers[3], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2]];
    
	return results;
}

- (NSArray *) check3rdLotto7: (NSArray*) numbers prize1: (NSArray *) prize1 prize2: (NSArray *) prize2
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L7 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue], [numbers[5] integerValue]]];
    
	[fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	NSMutableArray *returnResults = [[NSMutableArray alloc] init];
    
	for (Lotto7 *item in results) {
        
    	NSPredicate *predicate = [NSPredicate predicateWithFormat: @"count == %@", item.count];
        
    	NSArray *temp1 = [prize1 filteredArrayUsingPredicate:predicate];
    	if ([temp1 count] > 0) {
        	continue;
    	}
        
        NSArray *temp2 = [prize2 filteredArrayUsingPredicate:predicate];
        if ([temp2 count] > 0) {
            continue;
        }
        
    	[returnResults addObject:item];
	}
    
	return returnResults;
}

- (NSArray *) check4thPrizeLotto7: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2 prize3: (NSArray *)prize3
{
	NSMutableArray *results = [[NSMutableArray alloc] init];
    
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3]]; //56
    
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //46
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //36
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[1], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //26
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[2], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //16
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3]]; //06
    
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[3], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //45
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //35
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[1], numbers[3], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //25
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[2], numbers[3], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //15
	[results addObjectsFromArray:[self check4thLotto7:@[numbers[1], numbers[2], numbers[3], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //05
    
    [results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //34
    [results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[1], numbers[3], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //24
    [results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[2], numbers[3], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //14
    [results addObjectsFromArray:[self check4thLotto7:@[numbers[1], numbers[2], numbers[3], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //04
    
    [results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[1], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //23
    [results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[2], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //13
    [results addObjectsFromArray:[self check4thLotto7:@[numbers[1], numbers[2], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //03
    
    [results addObjectsFromArray:[self check4thLotto7:@[numbers[0], numbers[3], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //12
    [results addObjectsFromArray:[self check4thLotto7:@[numbers[1], numbers[3], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //02
    [results addObjectsFromArray:[self check4thLotto7:@[numbers[2], numbers[3], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3]]; //01
    
	return results;
}

- (NSArray *) check4thLotto7: (NSArray*) numbers prize1: (NSArray *) prize1 prize2: (NSArray *) prize2 prize3: (NSArray *) prize3
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L7 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //67
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //57
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //47
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //37
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //27
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //17
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //56
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //46
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //36
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //26
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //16
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //45
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //35
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //25
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //15
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //34
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //24
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //14
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //23
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //13
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue], [numbers[4] integerValue]]]; //12
    
	[fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	NSMutableArray *returnResults = [[NSMutableArray alloc] init];
    
	for (Lotto7 *item in results) {
        
    	NSPredicate *predicate = [NSPredicate predicateWithFormat: @"count == %@", item.count];
    	NSArray *temp = [prize1 filteredArrayUsingPredicate:predicate];
        
    	if ([temp count] > 0) {
        	continue;
    	}
        
        temp = [prize2 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
        temp = [prize3 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
        	continue;
    	}
        
    	[returnResults addObject:item];
	}
    
	return returnResults;
}

- (NSArray *) check5thPrizeLotto7: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2 prize3: (NSArray *)prize3 prize4: (NSArray *)prize4
{
	NSMutableArray *results = [[NSMutableArray alloc] init];
    
	[results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[3]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //456
    
	[results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //356
	[results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[1], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //256
	[results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //156
	[results addObjectsFromArray:[self check5thLotto7:@[numbers[1], numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //056
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //346
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[1], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //246
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[2], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //146
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[1], numbers[2], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //046
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[1], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //236
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[2], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //136
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[1], numbers[2], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //036
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //126
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[1], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //026
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[2], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //016
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //345
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[1], numbers[3], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //245
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[2], numbers[3], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //145
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[1], numbers[2], numbers[3], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //045
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[1], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //235
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[2], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //135
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[1], numbers[2], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //035
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[3], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //125
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[1], numbers[3], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //025
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[2], numbers[3], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //015
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[1], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //234
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[2], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //134
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[1], numbers[2], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //034
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[3], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //124
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[1], numbers[3], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //024
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[2], numbers[3], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //014
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[0], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //123
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[1], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //023
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[2], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //013
    
    [results addObjectsFromArray:[self check5thLotto7:@[numbers[3], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3  prize4:prize4]]; //012
    
	return results;
}

- (NSArray *) check5thLotto7: (NSArray*) numbers prize1: (NSArray *) prize1 prize2: (NSArray *) prize2 prize3: (NSArray *) prize3 prize4: (NSArray *) prize4
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L7 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //567
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //467
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //367
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //267
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //167
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //257
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //157
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //247
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //147
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //237
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //137
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //127
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //256
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //156
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //246
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //146
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //236
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //136
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //126
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //245
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //145
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //235
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //135
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //125
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //234
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //134
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //124
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //123
    
	[fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	NSMutableArray *returnResults = [[NSMutableArray alloc] init];
    
	for (Lotto7 *item in results) {
        
    	NSPredicate *predicate = [NSPredicate predicateWithFormat: @"count == %@", item.count];
    	NSArray *temp = [prize1 filteredArrayUsingPredicate:predicate];
        
    	if ([temp count] > 0) {
        	continue;
    	}
        
        temp = [prize2 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
            continue;
        }
        
        temp = [prize3 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
        	continue;
    	}
        
        temp = [prize4 filteredArrayUsingPredicate:predicate];
        
        if ([temp count] > 0) {
        	continue;
    	}
        
    	[returnResults addObject:item];
	}
    
	return returnResults;
}

- (NSArray *) check6thPrizeLotto7: (NSArray*) numbers prize1: (NSArray *)prize1 prize2: (NSArray *)prize2 prize3: (NSArray *)prize3  prize4: (NSArray *)prize4 prize5: (NSArray *)prize5
{
	NSMutableArray *results = [[NSMutableArray alloc] init];
    
	[results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[3]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //456
    
	[results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //356
	[results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[1], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //256
	[results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //156
	[results addObjectsFromArray:[self check6thLotto7:@[numbers[1], numbers[2], numbers[3], numbers[4]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //056
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //346
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[1], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //246
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[2], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //146
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[1], numbers[2], numbers[3], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //046
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[1], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //236
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[2], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //136
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[1], numbers[2], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //036
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //126
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[1], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //026
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[2], numbers[3], numbers[4], numbers[5]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //016
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[1], numbers[2], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //345
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[1], numbers[3], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //245
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[2], numbers[3], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //145
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[1], numbers[2], numbers[3], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //045
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[1], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //235
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[2], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //135
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[1], numbers[2], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //035
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[3], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //125
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[1], numbers[3], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //025
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[2], numbers[3], numbers[4], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //015
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[1], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //234
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[2], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //134
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[1], numbers[2], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //034
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[3], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //124
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[1], numbers[3], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //024
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[2], numbers[3], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //014
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[0], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //123
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[1], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //023
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[2], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //013
    
    [results addObjectsFromArray:[self check6thLotto7:@[numbers[3], numbers[4], numbers[5], numbers[6]] prize1:prize1 prize2:prize2 prize3:prize3 prize4:prize4 prize5:prize5]]; //012
    
	return results;
}

- (NSArray *) check6thLotto7: (NSArray*) numbers prize1: (NSArray *) prize1 prize2: (NSArray *) prize2 prize3: (NSArray *) prize3 prize4: (NSArray *) prize4 prize5: (NSArray *) prize5
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:L7 inManagedObjectContext:self.context]];
    
	NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //4567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //4567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_3 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //4567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //4567
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3567
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2567
    
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1567
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3467
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2467
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1467
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2367
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1367
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1267
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1267
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1267
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1267
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3457
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2457
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1457
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2357
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1357
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1257
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1257
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1257
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1257
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2347
    
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1347
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1247
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1247
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1247
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1247
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1237
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1237
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1237
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1237
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3456
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_3 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2456
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_3 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_3 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1456
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2356
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1356
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1256
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1256
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_1 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1256
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_3 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1256
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_5 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2346
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_5 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1346
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_5 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1246
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1246
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1246
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_3 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1246
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_5 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1236
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_5 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1236
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_bonus_1 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1236
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1236
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_6 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2345
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_6 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1345
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1245
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_6 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1245
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1245
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_3 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1245
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_5 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_1 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1234
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_5 == %i AND number_6 == %i AND number_bonus_1 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1234
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_5 == %i AND number_bonus_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1234
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_1 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1234
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_3 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //4567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_2 == %i AND number_3 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //4567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_2 == %i AND number_3 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //4567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_2 == %i AND number_3 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //4567
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_4 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_2 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_2 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_2 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3567
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2567
    
	[predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_4 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1567
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_3 == %i AND number_4 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1567
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_5 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3467
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_5 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2467
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_5 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1467
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_3 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1467
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2367
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1367
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1367
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_5 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1267
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1267
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1267
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_3 == %i AND number_4 == %i AND number_5 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1267
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3457
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2457
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1457
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_3 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1457
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2357
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1357
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1357
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1257
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1257
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1257
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_3 == %i AND number_4 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1257
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2347
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1347
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1347
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1247
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1247
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1247
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_3 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1247
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_5 == %i AND number_6 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1237
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1237
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1237
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_4 == %i AND number_5 == %i AND number_6 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1237
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_2 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //3456
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_3 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2456
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_3 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_3 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1456
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_3 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1456
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2356
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1356
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1356
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1256
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_4 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1256
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_2 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1256
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_3 == %i AND number_4 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1256
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_5 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2346
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_5 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1346
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1346
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_5 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1246
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1246
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1246
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_3 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1246
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_5 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1236
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_5 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1236
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_4 == %i AND number_bonus_2 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1236
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_4 == %i AND number_5 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1236
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_6 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_1 == %i AND number_bonus_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_1 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //2345
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_6 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_2 == %i AND number_bonus_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1345
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1345
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1245
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_6 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1245
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_3 == %i AND number_bonus_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1245
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_3 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1245
    
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_5 == %i AND number_6 == %i AND number_7 == %i AND number_bonus_2 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1234
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_5 == %i AND number_6 == %i AND number_bonus_2 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1234
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_5 == %i AND number_bonus_2 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1234
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"number_bonus_2 == %i AND number_5 == %i AND number_6 == %i AND number_7 == %i", [numbers[0] integerValue], [numbers[1] integerValue], [numbers[2] integerValue], [numbers[3] integerValue]]]; //1234
    
	[fetchRequest setPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]];
    
	NSError *error;
	NSArray *results = [self.context executeFetchRequest:fetchRequest error:&error];
    
	NSMutableArray *returnResults = [[NSMutableArray alloc] init];
    
	for (Lotto7 *item in results) {
        
    	NSPredicate *predicate = [NSPredicate predicateWithFormat: @"count == %@", item.count];
    	NSArray *temp = [prize1 filteredArrayUsingPredicate:predicate];
    	if ([temp count] > 0) {
        	continue;
    	}
        
        temp = [prize2 filteredArrayUsingPredicate:predicate];
        if ([temp count] > 0) {
            continue;
        }
        
        temp = [prize3 filteredArrayUsingPredicate:predicate];
        if ([temp count] > 0) {
            continue;
        }
        
        temp = [prize4 filteredArrayUsingPredicate:predicate];
        if ([temp count] > 0) {
            continue;
        }
        
        temp = [prize5 filteredArrayUsingPredicate:predicate];
        if ([temp count] > 0) {
            continue;
        }
        
    	[returnResults addObject:item];
	}
    
	return returnResults;
}

#pragma mark Save Results -

- (Boolean) saveResults: (NSArray *) results type:(NSString *) type
{
 
    if ([results count] == 0) {
        return true;
    }
 
    for (Result *result in results) {
 
        if ([L7 isEqualToString:type]) {
            if (![self saveLotto7: result]) {
                return false;
            }
        }
 
        else if ([L6 isEqualToString:type]) {
            if (![self saveLotto6: result]) {
                return false;
            }
        }
 
        else if ([L5 isEqualToString:type]) {
            if (![self saveLotto5: result]) {
                return false;
            }
        }
 
        else if ([N4 isEqualToString:type]) {
            if (![self saveNumber4: result]) {
                return false;
            }
        }
 
        else if ([N3 isEqualToString:type]) {
            if (![self saveNumber3: result]) {
                return false;
            }
        }
    }
 
    [self.context performBlockAndWait:^{
        NSError *error;
        if(![self.context save:&error]) {
            NSLog(@"insert error %@: %@", type, [error localizedDescription]);
            [self displayValidationError:error];
        }
    }];
 
    return true;
}

- (Boolean) savePurchase: (Number *)number ofType:(int)type
{
    switch (type) {
        case PURCHASED_LOTTO7:
            if (![self savePLotto7: number]) {
                return false;
            }
        break;
 
        case PURCHASED_LOTTO6:
            if (![self savePLotto6: number]) {
                return false;
            }
        break;
 
        case PURCHASED_MINILOTTO:
            if (![self savePLotto5: number]) {
                return false;
            }
        break;
 
        case PURCHASED_NUMBER4:
            if (![self savePNumber4: number]) {
                return false;
            }
        break;
 
        case PURCHASED_NUMBER3:
            if (![self savePNumber3: number]) {
                return false;
            }
        break;
    }
 
    NSError *error;
    if(![self.context save:&error]) {
        NSLog(@"insert error %d: %@", type, [error localizedDescription]);
        [self displayValidationError:error];
        return false;
    }
 
    /*[self.context performBlockAndWait:^{
        NSError *error;
        if(![self.context save:&error]) {
            NSLog(@"insert error %d: %@", type, [error localizedDescription]);
            [self displayValidationError:error];
        }
    }];*/

    return true;
}

- (Boolean) saveLotto7: (Result *) result {
    
    Lotto7 *managedObject = [NSEntityDescription insertNewObjectForEntityForName:L7 inManagedObjectContext:self.context];
    
    managedObject.count = result.count;
    managedObject.number_1 = [NSString stringWithFormat:@"%i", (int)result.number.num1];
    managedObject.number_2 = [NSString stringWithFormat:@"%i", (int)result.number.num2];
    managedObject.number_3 = [NSString stringWithFormat:@"%i", (int)result.number.num3];
    managedObject.number_4 = [NSString stringWithFormat:@"%i", (int)result.number.num4];
    managedObject.number_5 = [NSString stringWithFormat:@"%i", (int)result.number.num5];
    managedObject.number_6 = [NSString stringWithFormat:@"%i", (int)result.number.num6];
    managedObject.number_7 = [NSString stringWithFormat:@"%i", (int)result.number.num7];
    managedObject.number_bonus_1 = [NSString stringWithFormat:@"%i", (int)result.number.numB1];
    managedObject.number_bonus_2 = [NSString stringWithFormat:@"%i", (int)result.number.numB2];
    
    managedObject.hit_1 = result.hit1;
    managedObject.hit_2 = result.hit2;
    managedObject.hit_3 = result.hit3;
    managedObject.hit_4 = result.hit4;
    managedObject.hit_5 = result.hit5;
    managedObject.hit_6 = result.hit6;
    
    managedObject.prize_1 = result.price1;
    managedObject.prize_2 = result.price2;
    managedObject.prize_3 = result.price3;
    managedObject.prize_4 = result.price4;
    managedObject.prize_5 = result.price5;
    managedObject.prize_6 = result.price6;
    
    managedObject.prize_carry_over = result.carryOver;
    managedObject.prize_total = result.totalPrice;
    
    managedObject.date = result.date;
    
    return true;
}

- (Boolean) saveLotto6: (Result *) result {
    
    Lotto6 *managedObject = [NSEntityDescription insertNewObjectForEntityForName:L6 inManagedObjectContext:self.context];
    
    managedObject.count = result.count;
    managedObject.number_1 = [NSString stringWithFormat:@"%i", (int)result.number.num1];
    managedObject.number_2 = [NSString stringWithFormat:@"%i", (int)result.number.num2];
    managedObject.number_3 = [NSString stringWithFormat:@"%i", (int)result.number.num3];
    managedObject.number_4 = [NSString stringWithFormat:@"%i", (int)result.number.num4];
    managedObject.number_5 = [NSString stringWithFormat:@"%i", (int)result.number.num5];
    managedObject.number_6 = [NSString stringWithFormat:@"%i", (int)result.number.num6];
    managedObject.number_bonus_1 = [NSString stringWithFormat:@"%i", (int)result.number.numB1];
    
    managedObject.hit_1 = result.hit1;
    managedObject.hit_2 = result.hit2;
    managedObject.hit_3 = result.hit3;
    managedObject.hit_4 = result.hit4;
    managedObject.hit_5 = result.hit5;
    
    managedObject.prize_1 = result.price1;
    managedObject.prize_2 = result.price2;
    managedObject.prize_3 = result.price3;
    managedObject.prize_4 = result.price4;
    managedObject.prize_5 = result.price5;
    
    managedObject.prize_carry_over = result.carryOver;
    managedObject.prize_total = result.totalPrice;
    
    managedObject.date = result.date;
    
    return true;
}

- (Boolean) saveLotto5: (Result *) result {
    
    Lotto5 *managedObject = [NSEntityDescription insertNewObjectForEntityForName:L5 inManagedObjectContext:self.context];
    
    managedObject.count = result.count;
    managedObject.number_1 = [NSString stringWithFormat:@"%i", (int)result.number.num1];
    managedObject.number_2 = [NSString stringWithFormat:@"%i", (int)result.number.num2];
    managedObject.number_3 = [NSString stringWithFormat:@"%i", (int)result.number.num3];
    managedObject.number_4 = [NSString stringWithFormat:@"%i", (int)result.number.num4];
    managedObject.number_5 = [NSString stringWithFormat:@"%i", (int)result.number.num5];
    managedObject.number_bonus_1 = [NSString stringWithFormat:@"%i", (int)result.number.numB1];
    
    managedObject.hit_1 = result.hit1;
    managedObject.hit_2 = result.hit2;
    managedObject.hit_3 = result.hit3;
    managedObject.hit_4 = result.hit4;
    
    managedObject.prize_1 = result.price1;
    managedObject.prize_2 = result.price2;
    managedObject.prize_3 = result.price3;
    managedObject.prize_4 = result.price4;
    
    managedObject.prize_total = result.totalPrice;
    
    managedObject.date = result.date;
    
    return true;
}

- (Boolean) saveNumber4: (Result *) result {
    
    Numbers4 *managedObject = [NSEntityDescription insertNewObjectForEntityForName:N4 inManagedObjectContext:self.context];
    
    managedObject.count = result.count;
    managedObject.number_1 = [NSString stringWithFormat:@"%i", (int)result.number.num1];
    managedObject.number_2 = [NSString stringWithFormat:@"%i", (int)result.number.num2];
    managedObject.number_3 = [NSString stringWithFormat:@"%i", (int)result.number.num3];
    managedObject.number_4 = [NSString stringWithFormat:@"%i", (int)result.number.num4];
    
    managedObject.hit_1 = result.hit1;
    managedObject.hit_2 = result.hit2;
    managedObject.hit_3 = result.hit3;
    managedObject.hit_4 = result.hit4;
    
    managedObject.prize_1 = result.price1;
    managedObject.prize_2 = result.price2;
    managedObject.prize_3 = result.price3;
    managedObject.prize_4 = result.price4;
    
    managedObject.prize_total = result.totalPrice;
    
    managedObject.date = result.date;
    
    return true;
}

- (Boolean) saveNumber3: (Result *) result {
    
    Numbers3 *managedObject = [NSEntityDescription insertNewObjectForEntityForName:N3 inManagedObjectContext:self.context];
    
    managedObject.count = result.count;
    managedObject.number_1 = [NSString stringWithFormat:@"%i", (int)result.number.num1];
    managedObject.number_2 = [NSString stringWithFormat:@"%i", (int)result.number.num2];
    managedObject.number_3 = [NSString stringWithFormat:@"%i", (int)result.number.num3];
    
    managedObject.hit_1 = result.hit1;
    managedObject.hit_2 = result.hit2;
    managedObject.hit_3 = result.hit3;
    managedObject.hit_4 = result.hit4;
    managedObject.hit_5 = result.hit5;
    
    managedObject.prize_1 = result.price1;
    managedObject.prize_2 = result.price2;
    managedObject.prize_3 = result.price3;
    managedObject.prize_4 = result.price4;
    managedObject.prize_5 = result.price5;
    
    managedObject.prize_total = result.totalPrice;
    
    managedObject.date = result.date;
    
    return true;
}

- (Boolean) savePLotto7: (Number *) number {
    
    PLotto7 *managedObject = [NSEntityDescription insertNewObjectForEntityForName:PL7 inManagedObjectContext:self.context];
    
    managedObject.number_1 = [NSString stringWithFormat:@"%i", number.num1];
    managedObject.number_2 = [NSString stringWithFormat:@"%i", number.num2];
    managedObject.number_3 = [NSString stringWithFormat:@"%i", number.num3];
    managedObject.number_4 = [NSString stringWithFormat:@"%i", number.num4];
    managedObject.number_5 = [NSString stringWithFormat:@"%i", number.num5];
    managedObject.number_6 = [NSString stringWithFormat:@"%i", number.num6];
    managedObject.number_7 = [NSString stringWithFormat:@"%i", number.num7];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy/MM/dd"];

    managedObject.date = [format stringFromDate:date];
    
    return true;
}

- (Boolean) savePLotto6: (Number *) number {
    
    PLotto6 *managedObject = [NSEntityDescription insertNewObjectForEntityForName:PL6 inManagedObjectContext:self.context];
    
    managedObject.number_1 = [NSString stringWithFormat:@"%i", number.num1];
    managedObject.number_2 = [NSString stringWithFormat:@"%i", number.num2];
    managedObject.number_3 = [NSString stringWithFormat:@"%i", number.num3];
    managedObject.number_4 = [NSString stringWithFormat:@"%i", number.num4];
    managedObject.number_5 = [NSString stringWithFormat:@"%i", number.num5];
    managedObject.number_6 = [NSString stringWithFormat:@"%i", number.num6];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy/MM/dd"];
    
    managedObject.date = [format stringFromDate:date];
    
    return true;
}

- (Boolean) savePLotto5: (Number *) number {
    
    PLotto5 *managedObject = [NSEntityDescription insertNewObjectForEntityForName:PL5 inManagedObjectContext:self.context];
    
    managedObject.number_1 = [NSString stringWithFormat:@"%i", number.num1];
    managedObject.number_2 = [NSString stringWithFormat:@"%i", number.num2];
    managedObject.number_3 = [NSString stringWithFormat:@"%i", number.num3];
    managedObject.number_4 = [NSString stringWithFormat:@"%i", number.num4];
    managedObject.number_5 = [NSString stringWithFormat:@"%i", number.num5];

    
    NSDate *date = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy/MM/dd"];
    
    managedObject.date = [format stringFromDate:date];
    
    return true;
}

- (Boolean) savePNumber4: (Number *) number {
    
    PNumbers4 *managedObject = [NSEntityDescription insertNewObjectForEntityForName:PN4 inManagedObjectContext:self.context];
    
    managedObject.number_1 = [NSString stringWithFormat:@"%i", number.num1];
    managedObject.number_2 = [NSString stringWithFormat:@"%i", number.num2];
    managedObject.number_3 = [NSString stringWithFormat:@"%i", number.num3];
    managedObject.number_4 = [NSString stringWithFormat:@"%i", number.num4];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy/MM/dd"];
    
    managedObject.date = [format stringFromDate:date];
    
    return true;
}

- (Boolean) savePNumber3: (Number *) number {
    
    PNumbers3 *managedObject = [NSEntityDescription insertNewObjectForEntityForName:PN3 inManagedObjectContext:self.context];
    
    managedObject.number_1 = [NSString stringWithFormat:@"%i", number.num1];
    managedObject.number_2 = [NSString stringWithFormat:@"%i", number.num2];
    managedObject.number_3 = [NSString stringWithFormat:@"%i", number.num3];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy/MM/dd"];
    
    managedObject.date = [format stringFromDate:date];
    
    return true;
}

- (void) deleteManagedObject: (id)object ofType:(int)type
{
    switch (type) {
        case LOTTO7:
        {
            Lotto7 *item = (Lotto7 *)object;
            [self.context deleteObject:item];
            break;
        }
        case LOTTO6:
        {
            Lotto6 *item = (Lotto6 *)object;
            [self.context deleteObject:item];
            break;
        }
        break;
        case MINILOTTO:
        {
            Lotto5 *item = (Lotto5 *)object;
            [self.context deleteObject:item];
            break;
        }
        break;
        case NUMBER4:
        {
            Numbers4 *item = (Numbers4 *)object;
            [self.context deleteObject:item];
            break;
        }
        break;
        case NUMBER3:
        {
            Numbers3 *item = (Numbers3 *)object;
            [self.context deleteObject:item];
            break;
        }
        break;
        case PURCHASED_LOTTO7:
        {
            PLotto7 *item = (PLotto7 *)object;
            [self.context deleteObject:item];
            break;
        }
        break;
        case PURCHASED_LOTTO6:
        {
            PLotto6 *item = (PLotto6 *)object;
            [self.context deleteObject:item];
            break;
        }
        break;
        case PURCHASED_MINILOTTO:
        {
            PLotto5 *item = (PLotto5 *)object;
            [self.context deleteObject:item];
            break;
        }
        break;
        case PURCHASED_NUMBER4:
        {
            PNumbers4 *item = (PNumbers4 *)object;
            [self.context deleteObject:item];
            break;
        }
        break;
        case PURCHASED_NUMBER3:
        {
            PNumbers3 *item = (PNumbers3 *)object;
            [self.context deleteObject:item];
            break;
        }
        break;
    }
    
    NSError *error;
    if(![self.context save:&error]) {
        NSLog(@"insert error %d: %@", type, [error localizedDescription]);
        [self displayValidationError:error];
    }
}

- (void)displayValidationError:(NSError *)anError {
    if (anError && [[anError domain] isEqualToString:@"NSCocoaErrorDomain"]) {
        NSArray *errors = nil;
        
        // multiple errors?
        if ([anError code] == NSValidationMultipleErrorsError) {
            errors = [[anError userInfo] objectForKey:NSDetailedErrorsKey];
        } else {
            errors = [NSArray arrayWithObject:anError];
        }
        
        if (errors && [errors count] > 0) {
            NSString *messages = @"Reason(s):\n";
            
            for (NSError * error in errors) {
                NSString *entityName = [[[[error userInfo] objectForKey:@"NSValidationErrorObject"] entity] name];
                NSString *attributeName = [[error userInfo] objectForKey:@"NSValidationErrorKey"];
                NSString *msg;
                switch ([error code]) {
                    case NSManagedObjectValidationError:
                        msg = @"Generic validation error.";
                        break;
                    case NSValidationMissingMandatoryPropertyError:
                        msg = [NSString stringWithFormat:@"The attribute '%@' mustn't be empty.", attributeName];
                        break;
                    case NSValidationRelationshipLacksMinimumCountError:
                        msg = [NSString stringWithFormat:@"The relationship '%@' doesn't have enough entries.", attributeName];
                        break;
                    case NSValidationRelationshipExceedsMaximumCountError:
                        msg = [NSString stringWithFormat:@"The relationship '%@' has too many entries.", attributeName];
                        break;
                    case NSValidationRelationshipDeniedDeleteError:
                        msg = [NSString stringWithFormat:@"To delete, the relationship '%@' must be empty.", attributeName];
                        break;
                    case NSValidationNumberTooLargeError:
                        msg = [NSString stringWithFormat:@"The number of the attribute '%@' is too large.", attributeName];
                        break;
                    case NSValidationNumberTooSmallError:
                        msg = [NSString stringWithFormat:@"The number of the attribute '%@' is too small.", attributeName];
                        break;
                    case NSValidationDateTooLateError:
                        msg = [NSString stringWithFormat:@"The date of the attribute '%@' is too late.", attributeName];
                        break;
                    case NSValidationDateTooSoonError:
                        msg = [NSString stringWithFormat:@"The date of the attribute '%@' is too soon.", attributeName];
                        break;
                    case NSValidationInvalidDateError:
                        msg = [NSString stringWithFormat:@"The date of the attribute '%@' is invalid.", attributeName];
                        break;
                    case NSValidationStringTooLongError:
                        msg = [NSString stringWithFormat:@"The text of the attribute '%@' is too long.", attributeName];
                        break;
                    case NSValidationStringTooShortError:
                        msg = [NSString stringWithFormat:@"The text of the attribute '%@' is too short.", attributeName];
                        break;
                    case NSValidationStringPatternMatchingError:
                        msg = [NSString stringWithFormat:@"The text of the attribute '%@' doesn't match the required pattern.", attributeName];
                        break;
                    default:
                        msg = [NSString stringWithFormat:@"Unknown error (code %i).", (int)[error code]];
                        break;
                }
                
                messages = [messages stringByAppendingFormat:@"%@%@%@\n", (entityName?:@""),(entityName?@": ":@""),msg];
            }
            NSLog(@"error message: %@", messages);
        }
    }
}

@end
