//
//  DatabaseHandler.h
//  tousenkigan
//
//  Created by JCLuspo on 7/15/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Result.h"
#import "Lotto7.h"
#import "Lotto6.h"
#import "Lotto5.h"
#import "Numbers4.h"
#import "Numbers3.h"
#import "PLotto7.h"
#import "PLotto6.h"
#import "PLotto5.h"
#import "PNumbers4.h"
#import "PNumbers3.h"

@interface DatabaseHandler : NSObject

#define L7 @"Lotto7"
#define L6 @"Lotto6"
#define L5 @"Lotto5"
#define N4 @"Numbers4"
#define N3 @"Numbers3"

#define PL7 @"PLotto7"
#define PL6 @"PLotto6"
#define PL5 @"PLotto5"
#define PN4 @"PNumbers4"
#define PN3 @"PNumbers3"

- (void) dbToCoreData;
- (BOOL) existCount: (int) type count:(int)count;

- (NSArray *) getAllFromEntityByType: (int) type;
- (NSArray *) getAllFromEntity: (NSString *)entity withCountSort:(BOOL)hasCount;
- (NSArray *) getAllFromEntityResultByType: (int)type;
- (NSArray *) getLatestOfType: (int) type;
- (NSArray *) queryHit: (NSArray *) number ofType: (int)type;
- (NSMutableArray *) updateDatabase: (NSString *)entity;
- (Boolean) saveResults: (NSArray *) results type:(NSString *) type;
- (Boolean) savePurchase: (Number *)number ofType:(int)type;
- (void) deleteManagedObject: (id)object ofType:(int)type;

@end
