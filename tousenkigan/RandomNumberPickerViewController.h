//
//  RandomNumberPickerViewController.h
//  tousenkigan
//
//  Created by JCLuspo on 7/24/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Number.h"

@interface RandomNumberPickerViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property int type;
@property (strong, nonatomic) NSMutableArray *selectedNumbers;

@property (strong, nonatomic) IBOutlet UIButton *pickButton;
@property (strong, nonatomic) IBOutlet UIButton *generateButton;
@property (strong, nonatomic) IBOutlet UIButton *recordButton;

@property (strong, nonatomic) IBOutlet UIView *topAdView;
@property (strong, nonatomic) IBOutlet UIView *bottomAdView;

@property (strong, nonatomic) IBOutlet UIView *numbersView;
@property (strong, nonatomic) IBOutlet UIImageView *mikoView;

@property (strong, nonatomic) IBOutlet UICollectionView *numberCollectionView;

- (IBAction)pickButtonTapped:(id)sender;
- (IBAction)generateButtonTapped:(id)sender;
- (IBAction)recordButtonTapped:(id)sender;

@end
