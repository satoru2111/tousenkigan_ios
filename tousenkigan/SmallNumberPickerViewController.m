//
//  SmallNumberPickerViewController.m
//  tousenkigan
//
//  Created by JCLuspo on 7/24/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "SmallNumberPickerViewController.h"
#import "RandomNumberPickerViewController.h"
#import "CheckNumberViewController.h"
#import "DatabaseHandler.h"
#import "Utilities.h"
#import "Number.h"
#import "AdManager.h"

@interface SmallNumberPickerViewController ()
@property DatabaseHandler *databaseHandler;
@property NSMutableArray *selectedNumbers;
@property UIImage *backgroundImage;
@end

@implementation SmallNumberPickerViewController

@synthesize type;
@synthesize fromRandom;
@synthesize fromCheck;

@synthesize choiceCollectionView;
@synthesize numberCollectionView;
@synthesize checkButton;
@synthesize clearButton;

@synthesize adView;

@synthesize databaseHandler;
@synthesize selectedNumbers;
@synthesize backgroundImage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [AdManager prepareGADBannerView:adView
                           position:AD_POSITION_TOP
                 rootViewController:self];
    
    databaseHandler = [[DatabaseHandler alloc] init];
    
    choiceCollectionView.backgroundColor = [UIColor clearColor];
    choiceCollectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    numberCollectionView.backgroundColor = [UIColor clearColor];
    numberCollectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    selectedNumbers = [[NSMutableArray alloc] init];
    
    backgroundImage = [UIImage imageNamed:@"button_light_brue.9.png"];
    backgroundImage = [Utilities createResizableImageFromNinePatchImage:backgroundImage];
    
    UIImage *checkImage = [UIImage imageNamed:@"button_pink.9.png"];
    checkImage = [Utilities createResizableImageFromNinePatchImage:checkImage];
    [checkButton setBackgroundImage:checkImage forState:UIControlStateNormal];
    
    UIImage *ckearImage = [UIImage imageNamed:@"button_gray.9.png"];
    ckearImage = [Utilities createResizableImageFromNinePatchImage:ckearImage];
    [clearButton setBackgroundImage:ckearImage forState:UIControlStateNormal];

    if (fromRandom) {
        switch (type) {
            case LOTTO7:
            {
                self.title = @"ロト7 祈願番号指定";
                break;
            }
                
            case LOTTO6:
            {
                self.title = @"ロト6 祈願番号指定";
                break;
            }
                
            case MINILOTTO:
            {
                self.title = @"ミニロト 祈願番号指定";
                break;
            }
                
            case NUMBER4:
            {
                self.title = @"ナンバーズ4 祈願番号指定";
                break;
            }
                
            case NUMBER3:
            {
                self.title = @"ナンバーズ3 祈願番号指定";
                break;
            }
        }
        
        [checkButton setTitle:@" 当選祈願 " forState:UIControlStateNormal];
    } else if (fromCheck){
        switch (type) {
            case LOTTO7:
            {
                self.title = @"ロト7 自動当選確認";
                break;
            }
                
            case LOTTO6:
            {
                self.title = @"ロト6 自動当選確認";
                break;
            }
                
            case MINILOTTO:
            {
                self.title = @"ミニロト 自動当選確認";
                break;
            }
                
            case NUMBER4:
            {
                self.title = @"ナンバーズ4 自動当選確認";
                break;
            }
                
            case NUMBER3:
            {
                self.title = @"ナンバーズ3 自動当選確認";
                break;
            }
        }
        [checkButton setTitle:@"　追加　" forState:UIControlStateNormal];
        [checkButton setEnabled:FALSE];
    } else {
        switch (type) {
            case LOTTO7:
            {
                self.title = @"ロト7 当たってるかな？";
                break;
            }
                
            case LOTTO6:
            {
                self.title = @"ロト6 当たってるかな？";
                break;
            }
                
            case MINILOTTO:
            {
                self.title = @"ミニロト 当たってるかな？";
                break;
            }
                
            case NUMBER4:
            {
                self.title = @"ナンバーズ4 当たってるかな？";
                break;
            }
                
            case NUMBER3:
            {
                self.title = @"ナンバーズ3 当たってるかな？";
                break;
            }
        }
        [checkButton setTitle:@" 当選確認 " forState:UIControlStateNormal];
        [checkButton setEnabled:FALSE];
    }
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([collectionView isEqual:choiceCollectionView]) {
        return type;
    }
    
    return 10;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if ([collectionView isEqual:choiceCollectionView]) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell1" forIndexPath:indexPath];
    } else {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell2" forIndexPath:indexPath];
    }
    
    UIImageView *background = (UIImageView *)[cell viewWithTag:1];
    UILabel *numberLabel = (UILabel *)[cell viewWithTag:2];
    
    int index = (int)[indexPath item];
    
    [background setImage:backgroundImage];
    
    CGRect collectionFrame = collectionView.frame;
    CGRect cellFrame = cell.frame;
    
    
    if ([collectionView isEqual:choiceCollectionView]) {
        numberLabel.text = @"*";
        
        CGFloat cellWidth = collectionFrame.size.width / type;
        
        cellFrame.origin.x = (floor(index % type) * cellWidth);
        cellFrame.size.width = cellWidth;
        [cell setFrame:cellFrame];
        
    } else {
        numberLabel.text = [NSString stringWithFormat:@"%d", index];
    }
    
    cell.contentView.frame = cell.bounds;
    
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"clicked: %d", (int)[indexPath item]);
    
    if ([collectionView isEqual:choiceCollectionView]) {
        return;
    }
    
    if (fromRandom || [selectedNumbers count] == type) {
        [selectedNumbers removeLastObject];
    }
    
    [selectedNumbers addObject:[NSNumber numberWithInt:(int)[indexPath item]]];
    
    NSArray *indexPaths = choiceCollectionView.indexPathsForVisibleItems;
    
    for (NSIndexPath *indexPath in indexPaths) {
        
        UICollectionViewCell *cell = [choiceCollectionView cellForItemAtIndexPath:indexPath];
        UILabel *numberLabel = (UILabel *)[cell viewWithTag:2];
        
        if ([selectedNumbers count] <= [indexPath item]) {
            numberLabel.text = @"*";
        } else {
            numberLabel.text = [NSString stringWithFormat:@"%d", [[selectedNumbers objectAtIndex:[indexPath item]] intValue]];
        }
    }
    
    if (fromRandom || [selectedNumbers count] == type) {
        [checkButton setEnabled:TRUE];
    } else {
        [checkButton setEnabled:FALSE];
    }
}

- (IBAction)checkButtonTapped:(id)sender {
    
    //call or transition to results.
    if (fromRandom) {
        
        int previousViewIndex = [self.navigationController.viewControllers count] - 2;
        
        RandomNumberPickerViewController *previous = [self.navigationController.viewControllers objectAtIndex:previousViewIndex];
        previous.selectedNumbers = selectedNumbers;
        
        [self.navigationController popViewControllerAnimated:YES];
        
    } else if (fromCheck) {
        
        Number *number = [[Number alloc] init];
        int length = [selectedNumbers count];
        
        if (1 <= length) {
            number.num1 = [[selectedNumbers objectAtIndex:0] intValue];
        }
        
        if (2 <= length) {
            number.num2 = [[selectedNumbers objectAtIndex:1] intValue];
        }
        
        if (3 <= length) {
            number.num3 = [[selectedNumbers objectAtIndex:2] intValue];
        }
        
        if (4 <= length) {
            number.num4 = [[selectedNumbers objectAtIndex:3] intValue];
        }
        
        number.num5 = -1;
        number.num6 = -1;
        number.num7 = -1;

        
        [databaseHandler savePurchase:number ofType:(type + 10)];
        [self.navigationController popViewControllerAnimated:YES];
    
    } else {
        
        NSArray *results = [databaseHandler queryHit:selectedNumbers ofType:type];
        NSMutableString *message = [[NSMutableString alloc] init];
        int counter = 1;
        
        [message appendString:@"\n"];
        for (NSArray *winResult in results)
        {

            if (counter == 1) {
                [message appendFormat:@"ストレート 合計 %d回\n", [winResult count]];
            } else if (counter == 2) {
                [message appendFormat:@"ボックス 合計 %d回\n", [winResult count]];
            } else {
                [message appendFormat:@"ミニ 合計 %d回\n", [winResult count]];
            }
            
            NSArray *sortResult = [winResult sortedArrayUsingSelector:@selector(comparWithCount:)];
            int resultCount = 1;
            for (Result *lotto in sortResult)
            {
                if (resultCount % 4 == 0) {
                    [message appendString:@"\n"];
                }
                [message appendFormat:@"第%@回 ", lotto.count];
                resultCount++;
            }
            
            [message appendString:@"\n\n"];
            
            counter++;
        }
        
        NSMutableString *selectNum = [[NSMutableString alloc] init];
        [selectNum appendFormat:@"結果 "];
        for (NSString *num in selectedNumbers) {
            [selectNum appendFormat:@"%@  ", num];
        }
        
        if ([UIAlertController class]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:selectNum message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:Nil];
            
        } else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:selectNum
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    
}

- (IBAction)clearButtonTapped:(id)sender {
    
    NSArray *indexPaths = choiceCollectionView.indexPathsForVisibleItems;
    
    for (NSIndexPath *indexPath in indexPaths) {
        
        UICollectionViewCell *cell = [choiceCollectionView cellForItemAtIndexPath:indexPath];
        UILabel *numberLabel = (UILabel *)[cell viewWithTag:2];
        numberLabel.text = @"*";
    }
    
    selectedNumbers = [[NSMutableArray alloc] init];
    [checkButton setEnabled:false];
    
}

@end
