//
//  RandomNumberPickerViewController.m
//  tousenkigan
//
//  Created by JCLuspo on 7/24/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "RandomNumberPickerViewController.h"
#import "Utilities.h"
#import "AdManager.h"
#import "NumberPickerViewController.h"
#import "SmallNumberPickerViewController.h"
#import "DatabaseHandler.h"

@interface RandomNumberPickerViewController ()
@property NSMutableArray *generatedNumbers;
@property NSArray *imageAnimNames;
@property NSMutableArray *imageAnims;
@property DatabaseHandler *databaseHandler;
@end

@implementation RandomNumberPickerViewController

@synthesize type;
@synthesize selectedNumbers;

@synthesize pickButton;
@synthesize generateButton;
@synthesize recordButton;

@synthesize topAdView;
@synthesize bottomAdView;

@synthesize numbersView;
@synthesize mikoView;

@synthesize numberCollectionView;

@synthesize generatedNumbers;
@synthesize imageAnimNames;
@synthesize imageAnims;

@synthesize databaseHandler;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [AdManager prepareGADBannerView:topAdView
                           position:AD_POSITION_TOP
                 rootViewController:self];
    [AdManager prepareGADBannerView:bottomAdView
                           position:AD_POSITION_BOTTOM
                 rootViewController:self];
    
    switch (type) {
        case LOTTO7:
        {
            self.title = @"ロト7 当選番号祈願";
            break;
        }
            
        case LOTTO6:
        {
            self.title = @"ロト6 当選番号祈願";
            break;
        }
            
        case MINILOTTO:
        {
            self.title = @"ミニロト 当選番号祈願";
            break;
        }
            
        case NUMBER4:
        {
            self.title = @"ナンバーズ4 当選番号祈願";
            break;
        }
            
        case NUMBER3:
        {
            self.title = @"ナンバーズ3 当選番号祈願";
            break;
        }
    }
    
    generatedNumbers = [[NSMutableArray alloc] init];
    imageAnimNames = @[@"miko2_1.png", @"miko2_2.png"];
    imageAnims = [[NSMutableArray alloc] init];
    
    for (NSString *imageName in imageAnimNames) {
        [imageAnims addObject:[UIImage imageNamed:imageName]];
    }
    
    numberCollectionView.backgroundColor = [UIColor clearColor];
    numberCollectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIImage *checkImage = [UIImage imageNamed:@"button_gray.9.png"];
    checkImage = [Utilities createResizableImageFromNinePatchImage:checkImage];
    [pickButton setBackgroundImage:checkImage forState:UIControlStateNormal];
    [recordButton setBackgroundImage:checkImage forState:UIControlStateNormal];
    
    UIImage *image = [UIImage imageNamed:@"button_pink.9.png"];
    image = [Utilities createResizableImageFromNinePatchImage:image];
    [generateButton setBackgroundImage:image forState:UIControlStateNormal];
    
    UIBarButtonItem *backButton = [UIBarButtonItem new];
    backButton.title = @"戻る";
    self.navigationItem.backBarButtonItem = backButton;
    [[recordButton titleLabel] setNumberOfLines:2];
    [recordButton setHidden:YES];
    
    databaseHandler = [[DatabaseHandler alloc] init];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([selectedNumbers count] > 0) {
        [generateButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    } else {
        selectedNumbers = [[NSMutableArray alloc] init];
    }
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [generatedNumbers count];
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    UILabel *numberLabel = (UILabel *)[cell viewWithTag:1];
    
    int index = (int)[indexPath item];
    
    CGRect cellFrame = cell.frame;

    //294 -> width of view minus inset
    cellFrame.size.width = 294 / type;
    cellFrame.origin.x = index * cellFrame.size.width;
    cell.frame = cellFrame;
    
    int value = [(NSNumber *) [generatedNumbers objectAtIndex:index] intValue];
    numberLabel.text = [NSString stringWithFormat:@"%d", value];
    
    return cell;
}

- (IBAction)pickButtonTapped:(id)sender
{
    if (type >= MINILOTTO) {
        [self performSegueWithIdentifier:@"fmRandomToNumberPicker" sender:self];
    } else {
        [self performSegueWithIdentifier:@"fmRandomToSmallPicker" sender:self];
    }
}

- (IBAction)generateButtonTapped:(id)sender
{
    
    [self generateRandomNumbers];
    
    mikoView.animationImages = imageAnims;
    mikoView.animationDuration = 1;
    mikoView.animationRepeatCount = 4;
    
    [mikoView setImage:[UIImage imageNamed:@"miko3.png"]];
    [mikoView startAnimating];
    [self performSelector:@selector(finishAnimation) withObject:nil afterDelay:4];
}

- (IBAction)recordButtonTapped:(id)sender
{
    if( number != nil )
    {
        [databaseHandler savePurchase:number ofType:(type + 10)];
        number = nil;
        [recordButton setHidden:YES];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"fmRandomToNumberPicker"]) {
    
        NumberPickerViewController *destination = (NumberPickerViewController *)segue.destinationViewController;
        destination.type = type;
        destination.fromRandom = YES;
        
    } else if ([[segue identifier] isEqualToString:@"fmRandomToSmallPicker"]) {
        SmallNumberPickerViewController *destination = (SmallNumberPickerViewController *)segue.destinationViewController;
        destination.type = type;
        destination.fromRandom = YES;
    }
}

Number *number;
- (void) generateRandomNumbers
{
    generatedNumbers = [[NSMutableArray alloc] init];
    
    [numberCollectionView reloadData];
    [numberCollectionView layoutIfNeeded];
    
    generatedNumbers = [[NSMutableArray alloc] initWithArray:selectedNumbers];
    
    for (int index = (int)[generatedNumbers count]; index < type; index++) {
        
        int randomNumber = 1;
        
        do {
            randomNumber = [Utilities randomNumberFromLotto:type];
        } while (type >= MINILOTTO && [generatedNumbers containsObject:[[NSNumber alloc] initWithInt:randomNumber]]);
        
        [generatedNumbers addObject: [[NSNumber alloc] initWithInt:randomNumber]];
    }
    
    if (type >= MINILOTTO) {
        [generatedNumbers sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES]]];
    }
    
    number = nil;
    number = [[Number alloc] init];
    int length = [generatedNumbers count];
    
    number.num1 = [[generatedNumbers objectAtIndex:0] intValue];
    number.num2 = [[generatedNumbers objectAtIndex:1] intValue];
    number.num3 = [[generatedNumbers objectAtIndex:2] intValue];
    
    if( 4 <= length)
    {
        number.num4 = [[generatedNumbers objectAtIndex:3] intValue];
    }
    
    if( 5 <= length )
    {
        number.num5 = [[generatedNumbers objectAtIndex:4] intValue];
    }
    
    if (6 <= length) {
        number.num6 = [[generatedNumbers objectAtIndex:5] intValue];
    }
    
    if (7 <= length) {
        number.num7 = [[generatedNumbers objectAtIndex:6] intValue];
    }
    
    
    
    return;
}

- (void) finishAnimation {
    
    [generateButton setTitle:@"もう一回" forState:UIControlStateNormal];
//    [recordButton setEnabled:YES];
    [recordButton setHidden:NO];
    
    [numberCollectionView reloadData];
    [numberCollectionView layoutIfNeeded];
}

@end
