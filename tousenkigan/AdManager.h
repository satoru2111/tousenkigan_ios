//
//  AdManager.h
//  tousenkigan
//
//  Created by S.Suzuki on 2022/01/30.
//  Copyright © 2022 S.Suzuki. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GoogleMobileAds;

@interface AdManager : NSObject

#define AD_UNIT_ID_TOP @"ca-app-pub-2556516387311475/3069288540";
#define AD_UNIT_ID_BOTTOM @"ca-app-pub-2556516387311475/4765362088";

typedef NS_ENUM(NSInteger,AdPosition) {
    AD_POSITION_TOP,
    AD_POSITION_BOTTOM
};

+ (void)prepareGADBannerView:(UIView*)bannerView
                    position: (AdPosition)adPosition
          rootViewController:(id)rootViewController;

@end
