
#import "HTMLParser.h"
#import "HTML.h"
#import "Result.h"
#import "Number.h"
#import "DatabaseHandler.h"
#import "CHCSVParser.h"

@interface HTML ()
@property (strong, nonatomic) DatabaseHandler *databaseHandler;
@end

@implementation HTML

- (id) init {
    self = [super init];
    
    if(self) {
        self.databaseHandler = [[DatabaseHandler alloc] init];
    }
    
    return self;
}

-(void)createCoredata {
    [self.databaseHandler dbToCoreData];
}

-(int)getLatestNo:(NSString*)db
{
    NSMutableArray *dbCounts = [self.databaseHandler updateDatabase:db];
    return [dbCounts count];
}

-(NSString*)getLatestCount:(NSString*) str {
    return [str substringWithRange:NSMakeRange(1, 5)];
}

-(BOOL)checkLotto:(int)type error:(NSError **)error {

    *error = nil;
    NSURL *tutorialsUrl;
    NSMutableArray *dbCounts;
    
    switch (type) {
        case LOTTO7:
        {
            tutorialsUrl = [NSURL URLWithString:@"https://www.mizuhobank.co.jp/retail/takarakuji/loto/l_oto7/csv/loto7.csv"];
            dbCounts = [self.databaseHandler updateDatabase:L7];
            break;
        }
            
        case LOTTO6:
        {
            tutorialsUrl = [NSURL URLWithString:@"https://www.mizuhobank.co.jp/retail/takarakuji/loto/loto6/csv/loto6.csv"];
            dbCounts = [self.databaseHandler updateDatabase:L6];
            break;
        }
            
        case MINILOTTO:
        {
            tutorialsUrl = [NSURL URLWithString:@"https://www.mizuhobank.co.jp/retail/takarakuji/loto/miniloto/csv/miniloto.csv"];
            dbCounts = [self.databaseHandler updateDatabase:L5];
            break;
        }
            
        case NUMBER4:
        {
            tutorialsUrl = [NSURL URLWithString:@"https://www.mizuhobank.co.jp/retail/takarakuji/numbers/csv/numbers.csv"];
            dbCounts = [self.databaseHandler updateDatabase:N4];
            break;
        }
        
        case NUMBER3:
        {
            tutorialsUrl = [NSURL URLWithString:@"https://www.mizuhobank.co.jp/retail/takarakuji/numbers/csv/numbers.csv"];
            dbCounts = [self.databaseHandler updateDatabase:N3];
            break;
        }
    }
    return true;
}

-(BOOL)updateLotto7 {
    
    int ret = [self getMonthlyLotto7:@"https://www.mizuhobank.co.jp/retail/takarakuji/loto/loto7/csv/loto7.csv"];
    if (ret == -1) {
        return false;
    } else {
        return true;
    }
}

-(int)getMonthlyLotto7:(NSString *)url {
    NSURL *tutorialsUrl = [NSURL URLWithString:url];
    NSString *reply = [NSString stringWithContentsOfURL:tutorialsUrl encoding:NSShiftJISStringEncoding error:nil];
    NSArray *csvItems = [reply componentsSeparatedByString:@"\r\n"];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    NSMutableArray *dbCounts = [self.databaseHandler updateDatabase:L7];
    
    for (NSString *str in csvItems) {
        if ([str length] == 0 || [str containsString:@"A"]) {
            continue;
        }
        NSString *count = nil;
        NSString *date = nil;
        NSString *number1 = nil;
        NSString *number2 = nil;
        NSString *number3 = nil;
        NSString *number4 = nil;
        NSString *number5 = nil;
        NSString *number6 = nil;
        NSString *number7 = nil;
        NSString *numberB1 = nil;
        NSString *numberB2 = nil;
        NSString *hit1 = nil;
        NSString *hit2 = nil;
        NSString *hit3 = nil;
        NSString *hit4 = nil;
        NSString *hit5 = nil;
        NSString *hit6 = nil;
        NSString *price1 = nil;
        NSString *price2 = nil;
        NSString *price3 = nil;
        NSString *price4 = nil;
        NSString *price5 = nil;
        NSString *price6 = nil;
        NSString *carryOver = nil;
        NSString *totalPrice = nil;
    
        count = [str substringWithRange:NSMakeRange(2, 3)];
        
        if (dbCounts && [dbCounts containsObject:count]) {
            if ([results count] > 0) {
                [self.databaseHandler saveResults:[results copy] type:L7];
            }
            return 0;
        } else if ([results count] == 0) {
            if([self.delegate respondsToSelector:@selector(HTMLStartProcess)]) {
                [self.delegate HTMLStartProcess];
            }
        }
        
        NSURL *detailUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%04d%@",@"https://www.mizuhobank.co.jp/retail/takarakuji/loto/loto7/csv/A103", [count intValue], @".CSV"]];
        NSString *reply2 = [NSString stringWithContentsOfURL:detailUrl encoding:NSShiftJISStringEncoding error:nil];
        NSArray *csvItems2 = [reply2 componentsSeparatedByString:@"\r\n"];
        NSArray *dateItems = [csvItems2[1] componentsSeparatedByString:@","];
        NSArray *numberItems = [csvItems2[3] componentsSeparatedByString:@","];
        NSArray *result1 = [csvItems2[4] componentsSeparatedByString:@","];
        NSArray *result2 = [csvItems2[5] componentsSeparatedByString:@","];
        NSArray *result3 = [csvItems2[6] componentsSeparatedByString:@","];
        NSArray *result4 = [csvItems2[7] componentsSeparatedByString:@","];
        NSArray *result5 = [csvItems2[8] componentsSeparatedByString:@","];
        NSArray *result6 = [csvItems2[9] componentsSeparatedByString:@","];
        NSArray *carry = [csvItems2[10] componentsSeparatedByString:@","];
        NSArray *total = [csvItems2[11] componentsSeparatedByString:@","];
        NSLog(@"%@", numberItems);
        date = dateItems[2];
        number1 = numberItems[1];
        number2 = numberItems[2];
        number3 = numberItems[3];
        number4 = numberItems[4];
        number5 = numberItems[5];
        number6 = numberItems[6];
        number7 = numberItems[7];
        numberB1 = numberItems[9];
        numberB2 = numberItems[10];
        
        hit1 = result1[1];
        price1 = result1[2];
        hit2 = result2[1];
        price2 = result2[2];
        hit3 = result3[1];
        price3 = result3[2];
        hit4 = result4[1];
        price4 = result4[2];
        hit5 = result5[1];
        price5 = result5[2];
        hit6 = result6[1];
        price6 = result6[2];
        carryOver = carry[1];
        totalPrice = total[1];
        
        Number *number = [[Number alloc] init:[number1 intValue]
                                         num2:[number2 intValue]
                                         num3:[number3 intValue]
                                         num4:[number4 intValue]
                                         num5:[number5 intValue]
                                         num6:[number6 intValue]
                                         num7:[number7 intValue]
                                        numB1:[[Utilities formatNumberString:numberB1]intValue]
                                        numB2:[[Utilities formatNumberString:numberB2]intValue]];
        
        Result *result = [[Result alloc] init:number
                                        count:count
                                         date:date
                                         hit1:hit1
                                         hit2:hit2
                                         hit3:hit3
                                         hit4:hit4
                                         hit5:hit5
                                         hit6:hit6
                                       price1:price1
                                       price2:price2
                                       price3:price3
                                       price4:price4
                                       price5:price5
                                       price6:price6
                                   totalPrice:totalPrice
                                    carryOver:carryOver];
        
        [results addObject:result];
    }
    
    return ([self.databaseHandler saveResults:[results copy] type:L7])?1:-1;
}

-(BOOL)updateLotto6 {
    int ret = [self getLotto6: @"https://www.mizuhobank.co.jp/retail/takarakuji/loto/loto6/csv/loto6.csv"];
    if (ret == -1) {
        return false;
    } else {
        return true;
    }
}

-(int)getLotto6:(NSString *)url {
    NSURL *tutorialsUrl = [NSURL URLWithString:url];

    NSString *reply = [NSString stringWithContentsOfURL:tutorialsUrl encoding:NSShiftJISStringEncoding error:nil];
    NSArray *csvItems = [reply componentsSeparatedByString:@"\r\n"];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    NSMutableArray *dbCounts = [self.databaseHandler updateDatabase:L6];
    
    for (NSString* str in csvItems) {
        if ([str length] == 0 || [str containsString:@"A"]) {
            continue;
        }
        NSString *count = nil;
        NSString *date = nil;
        NSString *number1 = nil;
        NSString *number2 = nil;
        NSString *number3 = nil;
        NSString *number4 = nil;
        NSString *number5 = nil;
        NSString *number6 = nil;
        NSString *numberB1 = nil;
        NSString *hit1 = nil;
        NSString *hit2 = nil;
        NSString *hit3 = nil;
        NSString *hit4 = nil;
        NSString *hit5 = nil;
        NSString *price1 = nil;
        NSString *price2 = nil;
        NSString *price3 = nil;
        NSString *price4 = nil;
        NSString *price5 = nil;
        NSString *carryOver = nil;
        NSString *totalPrice = nil;
    
        count = [str substringWithRange:NSMakeRange(1, 4)];
        
        if (dbCounts && [dbCounts containsObject:count]) {
            if ([results count] > 0) {
                [self.databaseHandler saveResults:[results copy] type:L6];
            }
            return 0;
        } else if ([results count] == 0) {
            if([self.delegate respondsToSelector:@selector(HTMLStartProcess)]) {
                [self.delegate HTMLStartProcess];
            }
        }
        
        NSURL *detailUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%04d%@",@"https://www.mizuhobank.co.jp/retail/takarakuji/loto/loto6/csv/A102", [count intValue], @".CSV"]];
        NSString *reply2 = [NSString stringWithContentsOfURL:detailUrl encoding:NSShiftJISStringEncoding error:nil];
        NSArray *csvItems2 = [reply2 componentsSeparatedByString:@"\r\n"];
        NSArray *dateItems = [csvItems2[1] componentsSeparatedByString:@","];
        NSArray *numberItems = [csvItems2[3] componentsSeparatedByString:@","];
        NSArray *result1 = [csvItems2[4] componentsSeparatedByString:@","];
        NSArray *result2 = [csvItems2[5] componentsSeparatedByString:@","];
        NSArray *result3 = [csvItems2[6] componentsSeparatedByString:@","];
        NSArray *result4 = [csvItems2[7] componentsSeparatedByString:@","];
        NSArray *result5 = [csvItems2[8] componentsSeparatedByString:@","];
        NSArray *carry = [csvItems2[9] componentsSeparatedByString:@","];
        NSArray *total = [csvItems2[10] componentsSeparatedByString:@","];
        
        date = dateItems[2];
        number1 = numberItems[1];
        number2 = numberItems[2];
        number3 = numberItems[3];
        number4 = numberItems[4];
        number5 = numberItems[5];
        number6 = numberItems[6];
        numberB1 = numberItems[8];
        hit1 = result1[1];
        price1 = result1[2];
        hit2 = result2[1];
        price2 = result2[2];
        hit3 = result3[1];
        price3 = result3[2];
        hit4 = result4[1];
        price4 = result4[2];
        hit5 = result5[1];
        price5 = result5[2];
        carryOver = carry[1];
        totalPrice = total[1];
        
        Number *number = [[Number alloc] init:[number1 intValue]
                                         num2:[number2 intValue]
                                         num3:[number3 intValue]
                                         num4:[number4 intValue]
                                         num5:[number5 intValue]
                                         num6:[number6 intValue]
                                         num7: 0
                                        numB1:[[Utilities formatNumberString:numberB1]intValue]
                                        numB2:0];
        
        Result *result = [[Result alloc] init:number
                                        count:count
                                         date:date
                                         hit1:hit1
                                         hit2:hit2
                                         hit3:hit3
                                         hit4:hit4
                                         hit5:hit5
                                         hit6:@""
                                       price1:price1
                                       price2:price2
                                       price3:price3
                                       price4:price4
                                       price5:price5
                                       price6:@""
                                   totalPrice:totalPrice
                                    carryOver:carryOver];
        
        [results addObject:result];
    }
    
    return ([self.databaseHandler saveResults:[results copy] type:L6])?1:-1;
}

-(BOOL)updateLotto5 {
    
    int ret = [self getMiniLoto: @"https://www.mizuhobank.co.jp/retail/takarakuji/loto/miniloto/csv/miniloto.csv"];
    if (ret == -1) {
        return false;
    } else {
        return true;
    }
}

-(int)getMiniLoto:(NSString *)url {
    NSError *error = nil;
    NSURL *tutorialsUrl = [NSURL URLWithString:url];

    NSString *reply = [NSString stringWithContentsOfURL:tutorialsUrl encoding:NSShiftJISStringEncoding error:nil];
    NSArray *csvItems = [reply componentsSeparatedByString:@"\r\n"];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    NSMutableArray *dbCounts = [self.databaseHandler updateDatabase:L5];
    
    for (NSString *str in csvItems) {
        if ([str length] == 0 || [str containsString:@"A"]) {
            continue;
        }
        NSString *count = nil;
        NSString *date = nil;
        NSString *number1 = nil;
        NSString *number2 = nil;
        NSString *number3 = nil;
        NSString *number4 = nil;
        NSString *number5 = nil;
        NSString *numberB1 = nil;
        NSString *hit1 = nil;
        NSString *hit2 = nil;
        NSString *hit3 = nil;
        NSString *hit4 = nil;
        NSString *price1 = nil;
        NSString *price2 = nil;
        NSString *price3 = nil;
        NSString *price4 = nil;
        NSString *totalPrice = nil;
    
        count = [str substringWithRange:NSMakeRange(1, 4)];
        
        if (dbCounts && [dbCounts containsObject:count]) {
            if ([results count] > 0) {
                [self.databaseHandler saveResults:[results copy] type:L5];
            }
            return 0;
        } else if ([results count] == 0) {
            if([self.delegate respondsToSelector:@selector(HTMLStartProcess)]) {
                [self.delegate HTMLStartProcess];
            }
        }
        
        NSURL *detailUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%04d%@",@"https://www.mizuhobank.co.jp/retail/takarakuji/loto/miniloto/csv/A101", [count intValue], @".CSV"]];
        NSString *reply2 = [NSString stringWithContentsOfURL:detailUrl encoding:NSShiftJISStringEncoding error:nil];
        NSArray *csvItems2 = [reply2 componentsSeparatedByString:@"\r\n"];
        NSArray *dateItems = [csvItems2[1] componentsSeparatedByString:@","];
        NSArray *numberItems = [csvItems2[3] componentsSeparatedByString:@","];
        NSArray *result1 = [csvItems2[4] componentsSeparatedByString:@","];
        NSArray *result2 = [csvItems2[5] componentsSeparatedByString:@","];
        NSArray *result3 = [csvItems2[6] componentsSeparatedByString:@","];
        NSArray *result4 = [csvItems2[7] componentsSeparatedByString:@","];
        NSArray *total = [csvItems2[8] componentsSeparatedByString:@","];
        
        date = dateItems[2];
        number1 = numberItems[1];
        number2 = numberItems[2];
        number3 = numberItems[3];
        number4 = numberItems[4];
        number5 = numberItems[5];
        numberB1 = numberItems[7];
        hit1 = result1[1];
        price1 = result1[2];
        hit2 = result2[1];
        price2 = result2[2];
        hit3 = result3[1];
        price3 = result3[2];
        hit4 = result4[1];
        price4 = result4[2];
        totalPrice = total[1];
        
        Number *number = [[Number alloc] init:[number1 intValue]
                                         num2:[number2 intValue]
                                         num3:[number3 intValue]
                                         num4:[number4 intValue]
                                         num5:[number5 intValue]
                                         num6:0
                                         num7:0
                                        numB1:[[Utilities formatNumberString:numberB1]intValue]
                                        numB2:0];
        
        Result *result = [[Result alloc] init:number
                                        count:count
                                         date:date
                                         hit1:hit1
                                         hit2:hit2
                                         hit3:hit3
                                         hit4:hit4
                                         hit5:@""
                                         hit6:@""
                                       price1:price1
                                       price2:price2
                                       price3:price3
                                       price4:price4
                                       price5:@""
                                       price6:@""
                                   totalPrice:totalPrice
                                    carryOver:@""];
        
        [results addObject:result];
    }
    
    return ([self.databaseHandler saveResults:[results copy] type:L5])?1:-1;
}

- (BOOL) updateNumbers4AndNumbers3
{
    int ret = [self getNumbers4AndNumbers3];
    if (ret == -1) {
        return false;
    } else {
        return true;
    }
}

/**
 numbersの3と4をまとめて更新する
  @return 1...全てのアイテムを更新した　0...部分的に更新があった  -1...エラー
 */
-(int)getNumbers4AndNumbers3{
    NSError *error = nil;
    NSURL *tutorialsUrl = [NSURL URLWithString:@"https://www.mizuhobank.co.jp/retail/takarakuji/numbers/csv/numbers.csv"];

    NSString *reply = [NSString stringWithContentsOfURL:tutorialsUrl encoding:NSShiftJISStringEncoding error:nil];
    NSArray *csvItems = [reply componentsSeparatedByString:@"\r\n"];
    NSMutableArray *resultsN3 = [[NSMutableArray alloc] init];
    NSMutableArray *resultsN4 = [[NSMutableArray alloc] init];
    NSMutableArray *dbCountsN3 = [self.databaseHandler updateDatabase:N3];
    NSMutableArray *dbCountsN4 = [self.databaseHandler updateDatabase:N4];
    
    for (NSString *str in csvItems) {
        if ([str containsString:@"A"] || [str length] == 0) {
            continue;
        }
        
        NSString* count = [str substringWithRange:NSMakeRange(1, 4)];
        bool hasDataNumbers4 = dbCountsN4 && [dbCountsN4 containsObject:count];
        bool hasDataNumbers3 = dbCountsN3 && [dbCountsN3 containsObject:count];
        
        if (hasDataNumbers4 && hasDataNumbers3){
            if ([resultsN4 count] > 0) {
                [self.databaseHandler saveResults:[resultsN4 copy] type:N4];
            }
            if ([resultsN3 count] > 0) {
                [self.databaseHandler saveResults:[resultsN3 copy] type:N3];
            }
            return 0;
        }
            
        if ([resultsN4 count] == 0 && [resultsN3 count] ==0) {
            if([self.delegate respondsToSelector:@selector(HTMLStartProcess)]) {
                [self.delegate HTMLStartProcess];
            }
        }
        NSURL *detailUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%04d%@",@"https://www.mizuhobank.co.jp/retail/takarakuji/numbers/csv/A100", [count intValue], @".CSV"]];
        NSString *reply = [NSString stringWithContentsOfURL:detailUrl encoding:NSShiftJISStringEncoding error:nil];
        
        if (!hasDataNumbers4){
            [resultsN4 addObject:[self createNumbers4ResultFromCSVString:reply ofLotoNumber:count]];
        }
        if (!hasDataNumbers3){
            [resultsN3 addObject:[self createNumbers3ResultFromCSVString:reply ofLotoNumber:count]];
        }
    }
    
    int finalResult=1;
    if ([resultsN4 count]!=0){
        if (![self.databaseHandler saveResults:[resultsN4 copy] type:N4]){
            finalResult =-1;
        }
    }
    if ([resultsN3 count]!=0){
        if (![self.databaseHandler saveResults:[resultsN3 copy] type:N3]){
            finalResult =-1;
        }
    }
    return finalResult;
}

/**
  csvからnumbers4オブジェクトを作成するメソッド
    @param csvString 変換をかけるCSVの本体
    @param number 第何回の結果か
    @return Resultオブジェクト
 */
-(Result*) createNumbers4ResultFromCSVString:(NSString *)csvString ofLotoNumber:(NSString *)count {
    
    NSArray *csvItems = [csvString componentsSeparatedByString:@"\r\n"];
    NSArray *dateItems = [csvItems[1] componentsSeparatedByString:@","];
    NSArray *numberItems = [csvItems[10] componentsSeparatedByString:@","];
    NSArray *result1 = [csvItems[11] componentsSeparatedByString:@","];
    NSArray *result2 = [csvItems[12] componentsSeparatedByString:@","];
    NSArray *result3 = [csvItems[13] componentsSeparatedByString:@","];
    NSArray *result4 = [csvItems[14] componentsSeparatedByString:@","];
    NSArray *total = [csvItems[15] componentsSeparatedByString:@","];
    
    Number *number = [[Number alloc] init:[[numberItems[1] substringWithRange:NSMakeRange(0, 1)] intValue]
                                     num2:[[numberItems[1] substringWithRange:NSMakeRange(1, 1)] intValue]
                                     num3:[[numberItems[1] substringWithRange:NSMakeRange(2, 1)] intValue]
                                     num4:[[numberItems[1] substringWithRange:NSMakeRange(3, 1)] intValue]
                                     num5:0
                                     num6:0
                                     num7:0
                                    numB1:0
                                    numB2:0];
    
    Result *result = [[Result alloc] init:number
                                    count:count
                                     date:dateItems[2]
                                     hit1:result1[2]
                                     hit2:result2[1]
                                     hit3:result3[1]
                                     hit4:result4[1]
                                     hit5:@""
                                     hit6:@""
                                   price1:result1[3]
                                   price2:result2[2]
                                   price3:result3[2]
                                   price4:result4[2]
                                   price5:@""
                                   price6:@""
                               totalPrice:total[1]
                                carryOver:@""];
    
    return result;
}

/**
  csvからnumbers3オブジェクトを作成するメソッド
    @param csvString 変換をかけるCSVの本体
    @param number 第何回の結果か
    @return Resultオブジェクト
 */
-(Result*) createNumbers3ResultFromCSVString:(NSString *)csvString ofLotoNumber:(NSString *)count {
    
    NSArray *csvItems = [csvString componentsSeparatedByString:@"\r\n"];
    NSArray *dateItems = [csvItems[1] componentsSeparatedByString:@","];
    NSArray *numberItems = [csvItems[3] componentsSeparatedByString:@","];
    NSArray *result1 = [csvItems[4] componentsSeparatedByString:@","];
    NSArray *result2 = [csvItems[5] componentsSeparatedByString:@","];
    NSArray *result3 = [csvItems[6] componentsSeparatedByString:@","];
    NSArray *result4 = [csvItems[7] componentsSeparatedByString:@","];
    NSArray *result5 = [csvItems[8] componentsSeparatedByString:@","];
    NSArray *total = [csvItems[9] componentsSeparatedByString:@","];
    
    Number *number = [[Number alloc] init:[[numberItems[1] substringWithRange:NSMakeRange(0, 1)] intValue]
                                     num2:[[numberItems[1] substringWithRange:NSMakeRange(1, 1)] intValue]
                                     num3:[[numberItems[1] substringWithRange:NSMakeRange(2, 1)] intValue]
                                     num4:0
                                     num5:0
                                     num6:0
                                     num7:0
                                    numB1:0
                                    numB2:0];
    
    Result *result = [[Result alloc] init:number
                                    count:count
                                     date:dateItems[2]
                                     hit1:result1[2]
                                     hit2:result2[1]
                                     hit3:result3[1]
                                     hit4:result4[1]
                                     hit5:result5[2]
                                     hit6:@""
                                   price1:result1[3]
                                   price2:result2[2]
                                   price3:result3[2]
                                   price4:result4[2]
                                   price5:result5[3]
                                   price6:@""
                               totalPrice:total[1]
                                carryOver:@""];
    
    return result;
}

@end
