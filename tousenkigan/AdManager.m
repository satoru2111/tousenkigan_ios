//
//  AdManager.m
//  tousenkigan
//
//  Created by S.Suzuki on 2022/01/30.
//  Copyright © 2022 S.Suzuki. All rights reserved.
//

#import "AdManager.h"

@implementation AdManager
/**
 * 各VC上で行うGoogle広告の準備処理をまとめたもの。
 */
+ (void)prepareGADBannerView:(GADBannerView*)bannerView
                    position: (AdPosition)adPosition
          rootViewController:(id)rootViewController {
    if (![bannerView isKindOfClass:[GADBannerView class]]){
        @throw @"bannerView is not typeof GADBannerView";
    }
    switch(adPosition){
        case AD_POSITION_TOP:
            bannerView.adUnitID = AD_UNIT_ID_TOP;
            break;
        case AD_POSITION_BOTTOM:
            bannerView.adUnitID = AD_UNIT_ID_BOTTOM;
            break;
        default:
            @throw @"Undefined AdPosition";
    }
    bannerView.rootViewController = rootViewController;
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
}

@end
