//
//  RankingViewController.m
//  tousenkigan
//
//  Created by JCLuspo on 7/24/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "RankingViewController.h"
#import "DatabaseHandler.h"
#import "Rank.h"
#import "AdManager.h"

@interface RankingViewController ()

@property DatabaseHandler *databaseHandler;
@property NSArray *originalList;
@property NSMutableArray *filteredList;

@property int numberFilterType;
@property int historyFilterCount;

@property UIImage *cellBgImage;
@property UIImage *buttonBgImage;
@property UIImage *buttonSelectedBgImage;

@property UIButton *selectedNumber;
@property UIButton *selectedHistory;

@end

@implementation RankingViewController

@synthesize type;

@synthesize topAdView;
@synthesize bottomAdView;

@synthesize resultTableView;
@synthesize buttonContainerView;

@synthesize numberfilter1;
@synthesize numberfilter2;
@synthesize numberfilter3;
@synthesize numberfilter4;
@synthesize numberfilter5;
@synthesize numberfilter6;
@synthesize numberfilter7;

@synthesize historyFilter1;
@synthesize historyFilter2;
@synthesize historyFilter3;
@synthesize historyFilter4;
@synthesize historyFilter5;
@synthesize historyFilter6;

@synthesize databaseHandler;
@synthesize originalList;
@synthesize filteredList;

@synthesize numberFilterType;
@synthesize historyFilterCount;

@synthesize cellBgImage;
@synthesize buttonBgImage;
@synthesize buttonSelectedBgImage;

@synthesize selectedNumber;
@synthesize selectedHistory;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [AdManager prepareGADBannerView:topAdView
                           position:AD_POSITION_TOP
                 rootViewController:self];
    [AdManager prepareGADBannerView:bottomAdView
                           position:AD_POSITION_BOTTOM
                 rootViewController:self];
    
    switch (type) {
        case LOTTO7:
        {
            self.title = @"ロト7 よく出る数字";
            break;
        }
            
        case LOTTO6:
        {
            self.title = @"ロト6 よく出る数字";
            break;
        }
            
        case MINILOTTO:
        {
            self.title = @"ミニロト よく出る数字";
            break;
        }
            
        case NUMBER4:
        {
            self.title = @"ナンバーズ4 よく出る数字";
            break;
        }
            
        case NUMBER3:
        {
            self.title = @"ナンバーズ3 よく出る数字";
            break;
        }
    }
    
    databaseHandler = [[DatabaseHandler alloc] init];
    
    cellBgImage = [UIImage imageNamed:@"listbackwood.png"];
    cellBgImage = [Utilities createResizableImageFromNinePatchImage:cellBgImage];
    
    buttonBgImage = [UIImage imageNamed:@"button_gray.9.png"];
    buttonBgImage = [Utilities createResizableImageFromNinePatchImage:buttonBgImage];
    
    buttonSelectedBgImage = [UIImage imageNamed:@"button_light_brue.9.png"];
    buttonSelectedBgImage = [Utilities createResizableImageFromNinePatchImage:buttonSelectedBgImage];
    
    [numberfilter1 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [numberfilter2 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [numberfilter3 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [numberfilter4 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [numberfilter5 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [numberfilter6 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [numberfilter7 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    
    [historyFilter1 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [historyFilter2 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [historyFilter3 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [historyFilter4 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [historyFilter5 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    [historyFilter6 setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    
    if (MINILOTTO <= type) {
        numberFilterType = 1;
        [numberfilter3 setHidden:YES];
        [numberfilter4 setHidden:YES];
        [numberfilter5 setHidden:YES];
        [numberfilter6 setHidden:YES];
        [numberfilter7 setHidden:YES];
        
        [numberfilter1 setBackgroundImage:buttonSelectedBgImage forState:UIControlStateNormal];
        selectedNumber = numberfilter1;
    } else if (NUMBER4 == type) {
        numberFilterType = 3;
        [numberfilter1 setHidden:YES];
        [numberfilter2 setHidden:YES];
        
        [numberfilter3 setBackgroundImage:buttonSelectedBgImage forState:UIControlStateNormal];
        selectedNumber = numberfilter3;
    } else if (NUMBER3 == type) {
        numberFilterType = 3;
        [numberfilter1 setHidden:YES];
        [numberfilter2 setHidden:YES];
        [numberfilter6 setHidden:YES];
        [numberfilter3 setBackgroundImage:buttonSelectedBgImage forState:UIControlStateNormal];
        selectedNumber = numberfilter3;
    }
    
    [historyFilter3 setBackgroundImage:buttonSelectedBgImage forState:UIControlStateNormal];
    selectedHistory = historyFilter3;
    historyFilterCount = 0;
    
    buttonContainerView.backgroundColor = [UIColor clearColor];
    
    resultTableView.backgroundColor = [UIColor clearColor];
    resultTableView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    // 余白があれば消す操作
    resultTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    originalList = [self proccessManagedObjectArray:[databaseHandler getAllFromEntityByType:type]];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        resultTableView.separatorInset = UIEdgeInsetsZero;
        if ([resultTableView respondsToSelector:@selector(layoutMargins)]) {
            resultTableView.layoutMargins = UIEdgeInsetsZero;
        }
    }
    [self filterData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [filteredList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"tableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // デバイダーを端まで表示
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        cell.separatorInset = UIEdgeInsetsZero;
        // ios8以降
        if ([cell respondsToSelector:@selector(layoutMargins)]) {
            cell.layoutMargins = UIEdgeInsetsZero;
        }
    }
    
    Rank *rank = [filteredList objectAtIndex:[indexPath item]];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
    imageView.image = cellBgImage;
    
    UILabel *label1 = (UILabel *)[cell viewWithTag:2];
    UILabel *label2 = (UILabel *)[cell viewWithTag:3];
    UILabel *label3 = (UILabel *)[cell viewWithTag:4];
    
    label1.text = [NSString stringWithFormat:@"%d位", (int)[indexPath item] + 1];
    label3.text = [NSString stringWithFormat:@"%d回", rank.count];
    
    NSMutableString *temp = [[NSMutableString alloc] init];
    
    if ([rank.number1 intValue] > -1) {
        [temp appendString:rank.number1];
        [temp appendString:@" 　"];
    }
    
    if ([rank.number2 intValue] > -1) {
        [temp appendString:rank.number2];
        [temp appendString:@" 　"];
    }
    
    if ([rank.number3 intValue] > -1) {
        [temp appendString:rank.number3];
        [temp appendString:@" 　"];
    }
    
    if ([rank.number4 intValue] > -1) {
        [temp appendString:rank.number4];
    }
    
    label2.text = temp;
    
    cell.contentView.frame = cell.bounds;
    
    return cell;
}

- (IBAction)numberPressed:(id)sender
{
    UIButton *button = (UIButton *) sender;
    numberFilterType = (int)button.tag - 200;
    
    [selectedNumber setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    selectedNumber = button;
    [selectedNumber setBackgroundImage:buttonSelectedBgImage forState:UIControlStateNormal];
    
    [self filterData];
    [resultTableView reloadData];
    [resultTableView layoutIfNeeded];
}

- (IBAction)historyPressed:(id)sender
{
    UIButton *button = (UIButton *) sender;
    historyFilterCount = (int)button.tag - 300;
    
    [selectedHistory setBackgroundImage:buttonBgImage forState:UIControlStateNormal];
    selectedHistory = button;
    [selectedHistory setBackgroundImage:buttonSelectedBgImage forState:UIControlStateNormal];
    
    [self filterData];
    [resultTableView reloadData];
    [resultTableView layoutIfNeeded];
}

- (NSArray *) proccessManagedObjectArray: (NSArray *)objectArray
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    if (LOTTO7 == type) {
        for (Lotto7 *item in objectArray) {
            
            NSMutableArray *numArray = [[NSMutableArray alloc] init];
            [numArray addObject:item.number_1];
            [numArray addObject:item.number_2];
            [numArray addObject:item.number_3];
            [numArray addObject:item.number_4];
            [numArray addObject:item.number_5];
            [numArray addObject:item.number_6];
            [numArray addObject:item.number_7];
            
            [result addObject:numArray];
        }
    }
    
    else if (LOTTO6 == type) {
        for (Lotto6 *item in objectArray) {
            
            NSMutableArray *numArray = [[NSMutableArray alloc] init];
            [numArray addObject:item.number_1];
            [numArray addObject:item.number_2];
            [numArray addObject:item.number_3];
            [numArray addObject:item.number_4];
            [numArray addObject:item.number_5];
            [numArray addObject:item.number_6];
            
            [result addObject:numArray];
        }
    }
    
    else if (MINILOTTO == type) {
        for (Lotto5 *item in objectArray) {
            
            NSMutableArray *numArray = [[NSMutableArray alloc] init];
            [numArray addObject:item.number_1];
            [numArray addObject:item.number_2];
            [numArray addObject:item.number_3];
            [numArray addObject:item.number_4];
            [numArray addObject:item.number_5];
            
            [result addObject:numArray];
        }
    }
    
    else if (NUMBER4 == type) {
        for (Numbers4 *item in objectArray) {
            
            NSMutableArray *numArray = [[NSMutableArray alloc] init];
            [numArray addObject:item.number_1];
            [numArray addObject:item.number_2];
            [numArray addObject:item.number_3];
            [numArray addObject:item.number_4];
            
            [result addObject:numArray];
        }
    }
    
    else if (NUMBER3 == type) {
        for (Numbers3 *item in objectArray) {
            
            NSMutableArray *numArray = [[NSMutableArray alloc] init];
            [numArray addObject:item.number_1];
            [numArray addObject:item.number_2];
            [numArray addObject:item.number_3];
            
            [result addObject:numArray];
        }
    }
    
    return result;
}

- (void) filterData
{
    filteredList = [[NSMutableArray alloc] init];
    NSArray *historyFiltered = [[NSArray alloc] initWithArray: [originalList subarrayWithRange:NSMakeRange(0, ((historyFilterCount && [originalList count] > historyFilterCount)?historyFilterCount:[originalList count]))]];

    for (NSArray *item in historyFiltered) {
        
        if (1 == numberFilterType) {
            for (NSString *num in item) {
                [self updateFilteredList:@[num]];
            }
        }
        
        else if (2 == numberFilterType) {
            for (int i=0; i<[item count] - 1; i++) {
                for (int j=i+1; j < [item count]; j++) {
                    [self updateFilteredList:@[[item objectAtIndex:i], [item objectAtIndex:j]]];
                }
            }
        }
        
        else if (3 == numberFilterType) {
            [self updateFilteredList:[item objectAtIndex:0] atIndex:0];
        }
        
        else if (4 == numberFilterType) {
            [self updateFilteredList:[item objectAtIndex:1] atIndex:1];
        }
        
        else if (5 == numberFilterType) {
            [self updateFilteredList:[item objectAtIndex:2] atIndex:2];
        }
        
        else if (6 == numberFilterType && NUMBER4 == type) {
            [self updateFilteredList:[item objectAtIndex:3] atIndex:3];
        }
        
        else if (7 == numberFilterType && NUMBER3 == type) {
            [self updateFilteredList:@[[item objectAtIndex:0], [item objectAtIndex:1], [item objectAtIndex:2]]];
        }
        
        else if (7 == numberFilterType) {
            [self updateFilteredList:@[[item objectAtIndex:0], [item objectAtIndex:1], [item objectAtIndex:2], [item objectAtIndex:3]]];
        }
    }
    
    
    
    NSSortDescriptor *sort1 = [NSSortDescriptor sortDescriptorWithKey:@"count" ascending:NO];
    NSSortDescriptor *sort2 = [NSSortDescriptor sortDescriptorWithKey:@"compareString" ascending:YES];

    [filteredList sortUsingDescriptors:@[sort1, sort2]];
}

- (void) updateFilteredList: (NSArray *)nums
{
    Rank *rank = [[Rank alloc] initWithNumbers:nums];
    bool isNew = true;
    
    for (Rank *listItem in filteredList) {
        if ([listItem.compareString isEqualToString:rank.compareString]) {
            isNew = false;
            [listItem countUp];
            break;
        }
    }
    
    if (isNew) {
        [filteredList addObject:rank];
    }
}

- (void) updateFilteredList: (NSString *) num atIndex: (int) index
{
    bool isNew = true;
    Rank *rank = [[Rank alloc] initWithNumber:num atIndex:index];
    
    for (Rank *listItem in filteredList) {
        if ([listItem.compareString isEqualToString:rank.compareString]) {
            isNew = false;
            [listItem countUp];
            break;
        }
    }
    
    if (isNew) {
        [filteredList addObject:rank];
    }
}

- (IBAction)number1Pressed:(id)sender {
}
@end
