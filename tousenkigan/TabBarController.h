//
//  UITabBarController_TabBarController.h
//  tousenkigan
//
//  Created by Satoru Ozeki on 2014/05/30.
//  Copyright (c) 2014年 Satoru Ozeki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseHandler.h"
#import "ImobileSdkAds/ImobileSdkAds.h"
#import "HTML.h"
@interface TabBarController : UITabBarController<UITabBarControllerDelegate, HTMLDelegate>
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;

- (void)button1Tap:(int)sender;
- (void)button2Tap:(int)sender;
- (void)button3Tap:(int)sender;
- (void)button4Tap:(int)sender;
- (void)button5Tap:(int)sender;
- (void)button6Tap:(int)sender;
//- (void)updateDatabase;
@end
