//
//  Loto7ViewController.m
//  tousenkigan
//
//  Created by Satoru Ozeki on 2014/05/30.
//  Copyright (c) 2014年 Satoru Ozeki. All rights reserved.
//

#import "Loto7ViewController.h"
#import "SVProgressHUD.h"
#import "TabBarController.h"
#import "DatabaseHandler.h"
#import "AdManager.h"

@interface Loto7ViewController ()
@property DatabaseHandler *databaseHandler;
@end

@implementation Loto7ViewController

@synthesize button1;
@synthesize button2;
@synthesize button3;
@synthesize button4;
@synthesize button5;
@synthesize button6;

@synthesize lucky1;
@synthesize lucky2;
@synthesize lucky3;
@synthesize lucky4;
@synthesize lucky5;
@synthesize lucky6;
@synthesize lucky7;

@synthesize adView;

@synthesize buttonViewHeightConstraint;
@synthesize mikoViewWidthContstraint;
@synthesize mikoViewHeightConstraint;

@synthesize databaseHandler;

- (void)viewDidLoad
{
    [super viewDidLoad];

    databaseHandler = [[DatabaseHandler alloc] init];
    NSArray *result = [databaseHandler getLatestOfType:LOTTO7];

    [AdManager prepareGADBannerView:adView
                           position:AD_POSITION_TOP
                 rootViewController:self];
    
    if ([result count] > 0) {
        Lotto7 *item = result[0];
        [self setButtonTitleFromDateString:item.date];
    }
    
    if (IS_IPHONE_5) {
        buttonViewHeightConstraint.constant = 312;
        mikoViewWidthContstraint.constant = mikoViewHeightConstraint.constant = 145;
    } else {
        buttonViewHeightConstraint.constant = 222;
        mikoViewWidthContstraint.constant = mikoViewHeightConstraint.constant = 130;
    }
    
    UIImage *bgButton1 = [button1 backgroundImageForState:UIControlStateNormal];
    bgButton1 = [Utilities createResizableImageFromNinePatchImage:bgButton1];
    [button1 setBackgroundImage:bgButton1 forState:UIControlStateNormal];
    
    UIImage *bgButton2 = [button2 backgroundImageForState:UIControlStateNormal];
    bgButton2 = [Utilities createResizableImageFromNinePatchImage:bgButton2];
    [button2 setBackgroundImage:bgButton2 forState:UIControlStateNormal];
    
    UIImage *bgButton3 = [button3 backgroundImageForState:UIControlStateNormal];
    bgButton3 = [Utilities createResizableImageFromNinePatchImage:bgButton3];
    [button3 setBackgroundImage:bgButton3 forState:UIControlStateNormal];
    
    UIImage *bgButton4 = [button4 backgroundImageForState:UIControlStateNormal];
    bgButton4 = [Utilities createResizableImageFromNinePatchImage:bgButton4];
    [button4 setBackgroundImage:bgButton4 forState:UIControlStateNormal];
    
    UIImage *bgButton5 = [button5 backgroundImageForState:UIControlStateNormal];
    bgButton5 = [Utilities createResizableImageFromNinePatchImage:bgButton5];
    [button5 setBackgroundImage:bgButton5 forState:UIControlStateNormal];
    
    UIImage *bgButton6 = [button6 backgroundImageForState:UIControlStateNormal];
    bgButton6 = [Utilities createResizableImageFromNinePatchImage:bgButton6];
    [button6 setBackgroundImage:bgButton6 forState:UIControlStateNormal];
    
    NSArray *lucky = [Utilities dailyLuckyNumber:LOTTO7];
    
    lucky1.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:0] intValue]];
    lucky2.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:1] intValue]];
    lucky3.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:2] intValue]];
    lucky4.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:3] intValue]];
    lucky5.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:4] intValue]];
    lucky6.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:5] intValue]];
    lucky7.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:6] intValue]];
}

- (IBAction)button1Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button1Tap:LOTTO7];
}

- (IBAction)button2Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button2Tap:LOTTO7];
}

- (IBAction)button3Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button3Tap:LOTTO7];
}

- (IBAction)button4Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button4Tap:LOTTO7];
}

- (IBAction)button5Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button5Tap:LOTTO7];
}

- (IBAction)button6Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button6Tap:LOTTO7];
}

- (void)viewDidAppear:(BOOL)animated
{
    databaseHandler = [[DatabaseHandler alloc] init];
    NSArray *result = [databaseHandler getLatestOfType:LOTTO7];
    
    if ([result count] > 0) {
        Lotto7 *latestItem = result[0];
        [self setButtonTitleFromDateString:latestItem.date];
    }
}

- (void) updateButtonTitle {
    NSArray *result = [databaseHandler getLatestOfType:LOTTO7];
    
    if ([result count] > 0) {
        Lotto7 *latestItem = result[0];
        [self setButtonTitleFromDateString:latestItem.date];
    }
}

- (void) setButtonTitleFromDateString: (NSString*)string {
    NSDictionary* date = [Utilities getDateStringArrayFromString:string];
    NSString* title = [NSString stringWithFormat:@"   → 最新当せん番号 %@月%@日",[date objectForKey:@"month"],[date objectForKey:@"day"]];
    
    if ([NSThread isMainThread]) {
        [button1 setTitle:title forState:UIControlStateNormal];
    } else {
        dispatch_sync (
            dispatch_get_main_queue(),
            ^{
                [button1 setTitle:title forState:UIControlStateNormal];
            }
        );
    }
}

/*-(void)checkHit:(int)lastNo
{
    NSArray *result = [databaseHandler getLatestOfType:LOTTO7];
    
    if ([result count] > 0) {
        Lotto7 *latestItem = result[0];
        NSString *date = latestItem.date;
        
        [button1 setTitle:[NSString stringWithFormat:@"%@ %@", @"   → 最新当せん番号", [date substringFromIndex:5]] forState:UIControlStateNormal];
        
        lucky1.text = latestItem.number_1;
        lucky2.text = latestItem.number_2;
        lucky3.text = latestItem.number_3;
        lucky4.text = latestItem.number_4;
        lucky5.text = latestItem.number_5;
        lucky6.text = latestItem.number_6;
        lucky7.text = latestItem.number_7;
        
        // 最新当選番号と照合
        NSMutableArray *checkList = [[NSMutableArray alloc] init];
        NSMutableArray *checkDatas = [[databaseHandler getAllFromEntityByType:(LOTTO7 + 10)] mutableCopy];
        NSMutableArray *checkNo = [[NSMutableArray alloc] init];
        
        for (PLotto7 *item in checkDatas) {
            NSArray *numbers = [[NSArray alloc] initWithObjects:item.number_1,item.number_2,item.number_3 ,item.number_4,item.number_5,item.number_6,item.number_7, nil];
            NSArray *results = [databaseHandler queryHit:numbers ofType:LOTTO7];
            [checkList addObject:results];
        }
        
        for( int i = lastNo; i < [latestItem.count intValue]+1; i++)
        {
            [checkNo addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        if( [checkList count] > 0 )
        {
            // 6等分のカウンタ
            NSMutableArray *winResults = [[NSMutableArray alloc] init];
            for(int i = 0; i < 6; i++)
            {
                [winResults addObject:[[NSMutableArray alloc] init]];
            }
            
            // 当選結果格納
            NSMutableArray *winCount = [[NSMutableArray alloc] init];
            int checkNum = 0;
            for(NSArray *results in checkList)
            {
                int winNum=0;
                for (NSArray *winResult in results)
                {
                    NSArray *sortResult = [winResult sortedArrayUsingSelector:@selector(comparWithCount:)];
                    for (Result *lotto in sortResult)
                    {
                        for( NSString *count in checkNo)
                        {
                            if( [lotto.count isEqualToString:count] )
                            {
                                [winCount addObject:lotto.count];
                                PLotto7 *lot = [checkDatas objectAtIndex:checkNum];
                                lot.count = lotto.count;
                                [[winResults objectAtIndex:winNum] addObject:lot];
                                break;
                            }
                        }
                    }
                    winNum++;
                }
                checkNum++;
            }
            
            if( [winCount count] > 0 )
            {
                // メッセージ形成
                NSMutableString *message = [[NSMutableString alloc] init];
                int counter = 1;
                for( NSMutableArray *result in winResults)
                {
                    if( [result count] > 0 )
                    {
                        for( PLotto7 *lotto in result)
                        {
                            [message appendFormat:@"\nおめでとうございます！\n第%@回抽選当選です！\n\n",lotto.count];
                            [message appendFormat:@"%d等 \n", counter];
                            [message appendFormat:@"購入日:%@ \n",lotto.date];
                            [message appendFormat:@"購入番号:%@  ",lotto.number_1];
                            [message appendFormat:@"%@  ",lotto.number_2];
                            [message appendFormat:@"%@  ",lotto.number_3];
                            [message appendFormat:@"%@  ",lotto.number_4];
                            [message appendFormat:@"%@  ",lotto.number_5];
                            [message appendFormat:@"%@  ",lotto.number_6];
                            [message appendFormat:@"%@  ",lotto.number_7];
                            [message appendFormat:@"\n"];
                        }
                    }
                    counter++;
                }
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"当選確認"
                                                                message:message
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }
}*/

@end

