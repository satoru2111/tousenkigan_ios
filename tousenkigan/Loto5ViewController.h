//
//  Loto5ViewController.h
//  tousenkigan
//
//  Created by JCLuspo on 7/23/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utilities.h"
@interface Loto5ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIButton *button3;
@property (strong, nonatomic) IBOutlet UIButton *button4;
@property (strong, nonatomic) IBOutlet UIButton *button5;
@property (strong, nonatomic) IBOutlet UIButton *button6;

@property (strong, nonatomic) IBOutlet UILabel *lucky1;
@property (strong, nonatomic) IBOutlet UILabel *lucky2;
@property (strong, nonatomic) IBOutlet UILabel *lucky3;
@property (strong, nonatomic) IBOutlet UILabel *lucky4;
@property (strong, nonatomic) IBOutlet UILabel *lucky5;

@property (strong, nonatomic) IBOutlet UIView *adView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mikoViewWidthContstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mikoViewHeightConstraint;

- (IBAction)button1Tap:(id)sender;
- (IBAction)button2Tap:(id)sender;
- (IBAction)button3Tap:(id)sender;
- (IBAction)button4Tap:(id)sender;
- (IBAction)button5Tap:(id)sender;
- (IBAction)button6Tap:(id)sender;

//-(void)checkHit:(int)lastNo;
- (void) updateButtonTitle;
@end
