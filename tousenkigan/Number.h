
#import <Foundation/Foundation.h>

@interface Number : NSObject

-(id)init:(int) num1 num2:(int) num2 num3:(int) num3 num4:(int) num4 num5:(int) num5 num6:(int) num6 num7:(int) num7 numB1:(int) numB1 numB2:(int) numB2;

@property int num1;
@property int num2;
@property int num3;
@property int num4;
@property int num5;
@property int num6;
@property int num7;
@property int numB1;
@property int numB2;

@end
