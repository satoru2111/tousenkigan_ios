//
//  CheckNumberViewController.m
//  tousenkigan
//
//  Created by JCLuspo on 7/24/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "CheckNumberViewController.h"
#import "NumberPickerViewController.h"
#import "SmallNumberPickerViewController.h"
#import "DatabaseHandler.h"
#import "Result.h"
#import "Number.h"
#import "AdManager.h"

@interface CheckNumberViewController ()

@property DatabaseHandler *databaseHandler;
@property NSMutableArray *purchaseList;
@property NSMutableArray *managedArray;

@property UIImage *cellBgImage;

@end

@implementation CheckNumberViewController

@synthesize type;

@synthesize bottomAdView;

@synthesize purchaseListTable;
@synthesize bottomLabel;
@synthesize addButton;

@synthesize databaseHandler;
@synthesize purchaseList;
@synthesize managedArray;

@synthesize cellBgImage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [AdManager prepareGADBannerView:bottomAdView
                           position:AD_POSITION_BOTTOM
                 rootViewController:self];
    
    switch (type) {
        case LOTTO7:
        {
            self.title = @"ロト7 自動当選確認";
            break;
        }
            
        case LOTTO6:
        {
            self.title = @"ロト6 自動当選確認";
            break;
        }
            
        case MINILOTTO:
        {
            self.title = @"ミニロト 自動当選確認";
            break;
        }
            
        case NUMBER4:
        {
            self.title = @"ナンバーズ4 自動当選確認";
            break;
        }
            
        case NUMBER3:
        {
            self.title = @"ナンバーズ3 自動当選確認";
            break;
        }
    }
    
    databaseHandler = [[DatabaseHandler alloc] init];
    
    UIImage *addImage = [UIImage imageNamed:@"button_pink.9.png"];
    addImage = [Utilities createResizableImageFromNinePatchImage:addImage];
    [addButton setBackgroundImage:addImage forState:UIControlStateNormal];
    
    cellBgImage = [UIImage imageNamed:@"listbackwood.png"];
    cellBgImage = [Utilities createResizableImageFromNinePatchImage:cellBgImage];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    purchaseList = [[NSMutableArray alloc] init];
    managedArray = [[databaseHandler getAllFromEntityByType:(type + 10)] mutableCopy];
    [self storeResultsToArray:managedArray];
    
    [purchaseListTable reloadData];
    [purchaseListTable layoutIfNeeded];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [purchaseList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Result *item = [purchaseList objectAtIndex:[indexPath item]];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:2];
    UILabel *numbersLabel = (UILabel *)[cell viewWithTag:3];
    UIButton *delete = (UIButton *)[cell viewWithTag:4];
    
    
    imageView.image = cellBgImage;
    dateLabel.text = item.date;
    
    
    numbersLabel.text = [self numberToString:item];
    [delete addTarget:self action:@selector(deleteTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(NSString*)numberToString:(Result*)item
{
    
    NSMutableString *temp = [[NSMutableString alloc] init];
    
    if (item.number.num1 > -1) {
        [temp appendString:[NSString stringWithFormat:@"%d", item.number.num1]];
        [temp appendString:@"  "];
    }
    
    if (item.number.num2 > -1) {
        [temp appendString:[NSString stringWithFormat:@"%d", item.number.num2]];
        [temp appendString:@"  "];
    }
    
    if (item.number.num3 > -1) {
        [temp appendString:[NSString stringWithFormat:@"%d", item.number.num3]];
        [temp appendString:@"  "];
    }
    
    if (item.number.num4 > -1) {
        [temp appendString:[NSString stringWithFormat:@"%d", item.number.num4]];
        [temp appendString:@"  "];
    }
    
    if (item.number.num5 > -1) {
        [temp appendString:[NSString stringWithFormat:@"%d", item.number.num5]];
        [temp appendString:@"  "];
    }
    
    if (item.number.num6 > -1) {
        [temp appendString:[NSString stringWithFormat:@"%d", item.number.num6]];
        [temp appendString:@"  "];
    }
    
    if (item.number.num7 > -1) {
        [temp appendString:[NSString stringWithFormat:@"%d", item.number.num7]];
    }
    
    return temp;
}
- (IBAction)addClick: (id)sender
{
    if (MINILOTTO <= type) {
        [self performSegueWithIdentifier:@"fmCheckNumberToNumberPicker" sender:self];
    } else {
        [self performSegueWithIdentifier:@"fmCheckNumberToSmallNumberPicker" sender:self];
    }
}

- (void) deleteTapped: (id)sender
{
    
    
    UIButton *button = (UIButton *)sender;
    UIView *view = button;
    while (view != nil && ![view isKindOfClass:[UITableViewCell class]]) {
        view = [view superview];
    }
    UITableViewCell *cell = (UITableViewCell *)view;
    NSIndexPath *indexPath = [purchaseListTable indexPathForCell:cell];
    int index = (int) [indexPath item];
    
    Result *item = [purchaseList objectAtIndex:[indexPath item]];
    
    if ([UIAlertController class]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%@\nを削除してもよろしいですか？",[self numberToString:item]] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [databaseHandler deleteManagedObject:[managedArray objectAtIndex:index] ofType:type];
            [managedArray removeObjectAtIndex:index];
            [purchaseList removeObjectAtIndex:index];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^(){
                [purchaseListTable reloadData];
                [purchaseListTable layoutIfNeeded];
            }];
        }];
        
        [alertController addAction:noAction];
        [alertController addAction:yesAction];
        [self presentViewController:alertController animated:YES completion:Nil];
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@\nを削除してもよろしいですか？",[self numberToString:item]] delegate:self cancelButtonTitle:@"いいえ" otherButtonTitles:@"はい", nil];
        [alertView show];
        alertView.tag = index;
    }
}
#pragma mark UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 1:
            [databaseHandler deleteManagedObject:[managedArray objectAtIndex:alertView.tag] ofType:type];
            [managedArray removeObjectAtIndex:alertView.tag];
            [purchaseList removeObjectAtIndex:alertView.tag];
            
            [purchaseListTable reloadData];
            [purchaseListTable layoutIfNeeded];
            break;
            
        default:
            break;
    }
}

- (void) storeResultsToArray: (NSArray *)results
{
    switch (type) {
        case LOTTO7:
            for (PLotto7 *item in results) {
                Number *number = [[Number alloc] init:[item.number_1 intValue]
                                                 num2:[item.number_2 intValue]
                                                 num3:[item.number_3 intValue]
                                                 num4:[item.number_4 intValue]
                                                 num5:[item.number_5 intValue]
                                                 num6:[item.number_6 intValue]
                                                 num7:[item.number_7 intValue]
                                                numB1:0
                                                numB2:0];
                
                Result *result = [[Result alloc] init];
                result.number = number;
                result.date = item.date;
                [purchaseList addObject:result];
            }
        break;

        case LOTTO6:
            for (PLotto6 *item in results) {
                Number *number = [[Number alloc] init:[item.number_1 intValue]
                                                 num2:[item.number_2 intValue]
                                                 num3:[item.number_3 intValue]
                                                 num4:[item.number_4 intValue]
                                                 num5:[item.number_5 intValue]
                                                 num6:[item.number_6 intValue]
                                                 num7:-1
                                                numB1:0
                                                numB2:0];
                
                Result *result = [[Result alloc] init];
                result.number = number;
                result.date = item.date;
                [purchaseList addObject:result];
            }
        break;

        case MINILOTTO:
            for (PLotto5 *item in results) {
                Number *number = [[Number alloc] init:[item.number_1 intValue]
                                                 num2:[item.number_2 intValue]
                                                 num3:[item.number_3 intValue]
                                                 num4:[item.number_4 intValue]
                                                 num5:[item.number_5 intValue]
                                                 num6:-1
                                                 num7:-1
                                                numB1:0
                                                numB2:0];
                
                Result *result = [[Result alloc] init];
                result.number = number;
                result.date = item.date;
                [purchaseList addObject:result];
            }
        break;
        
        case NUMBER4:
            for (PNumbers4 *item in results) {
                Number *number = [[Number alloc] init:[item.number_1 intValue]
                                                 num2:[item.number_2 intValue]
                                                 num3:[item.number_3 intValue]
                                                 num4:[item.number_4 intValue]
                                                 num5:-1
                                                 num6:-1
                                                 num7:-1
                                                numB1:0
                                                numB2:0];
                
                Result *result = [[Result alloc] init];
                result.number = number;
                result.date = item.date;
                [purchaseList addObject:result];
            }
        break;
        
        case NUMBER3:
        for (PNumbers3 *item in results) {
            Number *number = [[Number alloc] init:[item.number_1 intValue]
                                             num2:[item.number_2 intValue]
                                             num3:[item.number_3 intValue]
                                             num4:-1
                                             num5:-1
                                             num6:-1
                                             num7:-1
                                            numB1:0
                                            numB2:0];
            
            Result *result = [[Result alloc] init];
            result.number = number;
            result.date = item.date;
            [purchaseList addObject:result];
        }
        break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"fmCheckNumberToNumberPicker"]) {
        NumberPickerViewController *destination = segue.destinationViewController;
        destination.type = self.type;
        destination.fromCheck = YES;
    }
    
    else if ([[segue identifier] isEqualToString:@"fmCheckNumberToSmallNumberPicker"]) {
        SmallNumberPickerViewController *destination = segue.destinationViewController;
        destination.type = self.type;
        destination.fromCheck = YES;
    }
}

@end
