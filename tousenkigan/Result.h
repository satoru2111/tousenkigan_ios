
#import <Foundation/Foundation.h>
#import "Number.h"

@interface Result : NSObject{
    Number *_number;
    NSString *_count;
    NSString *_date;
    NSString *_hit1;
    NSString *_hit2;
    NSString *_hit3;
    NSString *_hit4;
    NSString *_hit5;
    NSString *_hit6;
    NSString *_price1;
    NSString *_price2;
    NSString *_price3;
    NSString *_price4;
    NSString *_price5;
    NSString *_price6;
    NSString *_totalPrice;
    NSString *_carryOver;
}


-(id)init:(Number *)number
count:(NSString *)count
date:(NSString *)date
hit1:(NSString *)hit1
hit2:(NSString *)hit2
hit3:(NSString *)hit3
hit4:(NSString *)hit4
hit5:(NSString *)hit5
hit6:(NSString *)hit6
price1:(NSString *)price1
price2:(NSString *)price2
price3:(NSString *)price3
price4:(NSString *)price4
price5:(NSString *)price5
price6:(NSString *)price6
totalPrice:(NSString *)totalPrice
carryOver:(NSString *)carryOver;

- (NSComparisonResult)comparWithCount:(Result *)item;
@property Number *number;
@property NSString *count;
@property NSString *date;
@property NSString *hit1;
@property NSString *hit2;
@property NSString *hit3;
@property NSString *hit4;
@property NSString *hit5;
@property NSString *hit6;
@property NSString *price1;
@property NSString *price2;
@property NSString *price3;
@property NSString *price4;
@property NSString *price5;
@property NSString *price6;
@property NSString *totalPrice;
@property NSString *carryOver;

@end
