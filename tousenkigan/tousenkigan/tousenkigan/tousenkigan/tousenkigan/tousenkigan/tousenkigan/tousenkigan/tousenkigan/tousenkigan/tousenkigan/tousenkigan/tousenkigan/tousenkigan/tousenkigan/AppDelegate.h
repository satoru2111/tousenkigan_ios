//
//  AppDelegate.h
//  tousenkigan
//
//  Created by Satoru Ozeki on 2014/05/06.
//  Copyright (c) 2014年 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
