//
//  RankingViewController.h
//  tousenkigan
//
//  Created by JCLuspo on 7/24/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface RankingViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property int type;

@property (strong, nonatomic) IBOutlet UIView *topAdView;
@property (strong, nonatomic) IBOutlet UIView *bottomAdView;

@property (strong, nonatomic) IBOutlet UITableView *resultTableView;
@property (strong, nonatomic) IBOutlet UIView *buttonContainerView;

@property (strong, nonatomic) IBOutlet UIButton *numberfilter1;
@property (strong, nonatomic) IBOutlet UIButton *numberfilter2;
@property (strong, nonatomic) IBOutlet UIButton *numberfilter3;
@property (strong, nonatomic) IBOutlet UIButton *numberfilter4;
@property (strong, nonatomic) IBOutlet UIButton *numberfilter5;
@property (strong, nonatomic) IBOutlet UIButton *numberfilter6;
@property (strong, nonatomic) IBOutlet UIButton *numberfilter7;

@property (strong, nonatomic) IBOutlet UIButton *historyFilter1;
@property (strong, nonatomic) IBOutlet UIButton *historyFilter2;
@property (strong, nonatomic) IBOutlet UIButton *historyFilter3;
@property (strong, nonatomic) IBOutlet UIButton *historyFilter4;
@property (strong, nonatomic) IBOutlet UIButton *historyFilter5;
@property (strong, nonatomic) IBOutlet UIButton *historyFilter6;

- (IBAction)numberPressed:(id)sender;
- (IBAction)historyPressed:(id)sender;

@end
