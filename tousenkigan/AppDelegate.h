//
//  AppDelegate.h
//  tousenkigan
//
//  Created by Satoru Ozeki on 2014/05/06.
//  Copyright (c) 2014年 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Loto7ViewController.h"
#import "Loto6ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    UIViewController *tab1;
    UIViewController *tab2;
    UITabBarController *tabController;
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@end
