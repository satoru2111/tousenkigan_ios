//
//  Loto5ViewController.m
//  tousenkigan
//
//  Created by JCLuspo on 7/23/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "Loto5ViewController.h"
#import "TabBarController.h"
#import "DatabaseHandler.h"
#import "AdManager.h"

@interface Loto5ViewController ()
@property DatabaseHandler *databaseHandler;
@end

@implementation Loto5ViewController

@synthesize button1;
@synthesize button2;
@synthesize button3;
@synthesize button4;
@synthesize button5;
@synthesize button6;

@synthesize lucky1;
@synthesize lucky2;
@synthesize lucky3;
@synthesize lucky4;
@synthesize lucky5;

@synthesize adView;

@synthesize buttonViewHeightConstraint;
@synthesize mikoViewWidthContstraint;
@synthesize mikoViewHeightConstraint;

@synthesize databaseHandler;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    databaseHandler = [[DatabaseHandler alloc] init];
    NSArray *result = [databaseHandler getLatestOfType:MINILOTTO];
    
    [AdManager prepareGADBannerView:adView
                           position:AD_POSITION_TOP
                 rootViewController:self];
    
    if ([result count] > 0) {
        Lotto5 *item = result[0];
        [self setButtonTitleFromDateString:item.date];
    }
    
    if (IS_IPHONE_5) {
        buttonViewHeightConstraint.constant = 312;
        mikoViewWidthContstraint.constant = mikoViewHeightConstraint.constant = 145;
    } else {
        buttonViewHeightConstraint.constant = 222;
        mikoViewWidthContstraint.constant = mikoViewHeightConstraint.constant = 130;
    }
    
    UIImage *bgButton1 = [button1 backgroundImageForState:UIControlStateNormal];
    bgButton1 = [Utilities createResizableImageFromNinePatchImage:bgButton1];
    [button1 setBackgroundImage:bgButton1 forState:UIControlStateNormal];
    
    UIImage *bgButton2 = [button2 backgroundImageForState:UIControlStateNormal];
    bgButton2 = [Utilities createResizableImageFromNinePatchImage:bgButton2];
    [button2 setBackgroundImage:bgButton2 forState:UIControlStateNormal];
    
    UIImage *bgButton3 = [button3 backgroundImageForState:UIControlStateNormal];
    bgButton3 = [Utilities createResizableImageFromNinePatchImage:bgButton3];
    [button3 setBackgroundImage:bgButton3 forState:UIControlStateNormal];
    
    UIImage *bgButton4 = [button4 backgroundImageForState:UIControlStateNormal];
    bgButton4 = [Utilities createResizableImageFromNinePatchImage:bgButton4];
    [button4 setBackgroundImage:bgButton4 forState:UIControlStateNormal];
    
    UIImage *bgButton5 = [button5 backgroundImageForState:UIControlStateNormal];
    bgButton5 = [Utilities createResizableImageFromNinePatchImage:bgButton5];
    [button5 setBackgroundImage:bgButton5 forState:UIControlStateNormal];
    
    UIImage *bgButton6 = [button6 backgroundImageForState:UIControlStateNormal];
    bgButton6 = [Utilities createResizableImageFromNinePatchImage:bgButton6];
    [button6 setBackgroundImage:bgButton6 forState:UIControlStateNormal];
    
    NSArray *lucky = [Utilities dailyLuckyNumber:MINILOTTO];
    
    lucky1.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:0] intValue]];
    lucky2.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:1] intValue]];
    lucky3.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:2] intValue]];
    lucky4.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:3] intValue]];
    lucky5.text = [NSString stringWithFormat:@"%d", [(NSNumber *)[lucky objectAtIndex:4] intValue]];
}

- (void)viewDidAppear:(BOOL)animated
{
    databaseHandler = [[DatabaseHandler alloc] init];
    NSArray *result = [databaseHandler getLatestOfType:MINILOTTO];
    
    if ([result count] > 0) {
        Lotto5 *latestItem = result[0];
        [self setButtonTitleFromDateString:latestItem.date];
        /*
        lucky1.text = latestItem.number_1;
        lucky2.text = latestItem.number_2;
        lucky3.text = latestItem.number_3;
        lucky4.text = latestItem.number_4;
        lucky5.text = latestItem.number_5;*/
    }
}

- (void) updateButtonTitle {
    NSArray *result = [databaseHandler getLatestOfType:MINILOTTO];
    
    if ([result count] > 0) {
        Lotto5 *latestItem = result[0];
        [self setButtonTitleFromDateString:latestItem.date];
    }
}

- (void) setButtonTitleFromDateString: (NSString*)string {
    NSDictionary* date = [Utilities getDateStringArrayFromString:string];
    NSString* title = [NSString stringWithFormat:@"   → 最新当せん番号 %@月%@日",[date objectForKey:@"month"],[date objectForKey:@"day"]];
    
    if ([NSThread isMainThread]) {
        [button1 setTitle:title forState:UIControlStateNormal];
    } else {
        dispatch_sync (
            dispatch_get_main_queue(),
            ^{
                [button1 setTitle:title forState:UIControlStateNormal];
            }
        );
    }
}

/*-(void)checkHit:(int)lastNo
{
    NSArray *result = [databaseHandler getLatestOfType:MINILOTTO];
    
    if ([result count] > 0) {
        Lotto5 *latestItem = result[0];
        NSString *date = latestItem.date;
        
        [button1 setTitle:[NSString stringWithFormat:@"%@ %@", @"   → 最新当せん番号", [date substringFromIndex:5]] forState:UIControlStateNormal];
        
        lucky1.text = latestItem.number_1;
        lucky2.text = latestItem.number_2;
        lucky3.text = latestItem.number_3;
        lucky4.text = latestItem.number_4;
        lucky5.text = latestItem.number_5;
        
        // 最新当選番号と照合
        NSMutableArray *checkList = [[NSMutableArray alloc] init];
        NSMutableArray *checkDatas = [[databaseHandler getAllFromEntityByType:(MINILOTTO + 10)] mutableCopy];
        NSMutableArray *checkNo = [[NSMutableArray alloc] init];
        
        for (PLotto5 *item in checkDatas) {
            NSArray *numbers = [[NSArray alloc] initWithObjects:item.number_1,item.number_2,item.number_3 ,item.number_4,item.number_5, nil];
            NSArray *results = [databaseHandler queryHit:numbers ofType:MINILOTTO];
            [checkList addObject:results];
        }
        
        for( int i = lastNo; i <= [latestItem.count intValue]; i++)
        {
            [checkNo addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        if( [checkList count] > 0 )
        {
            // 4等分のカウンタ
            NSMutableArray *winResults = [[NSMutableArray alloc] init];
            for(int i = 0; i < 4; i++)
            {
                [winResults addObject:[[NSMutableArray alloc] init]];
            }
            
            // 当選結果格納
            NSMutableArray *winCount = [[NSMutableArray alloc] init];
            int checkNum = 0;
            for(NSArray *results in checkList)
            {
                int winNum = 0;
                for (NSArray *winResult in results)
                {
                    NSArray *sortResult = [winResult sortedArrayUsingSelector:@selector(comparWithCount:)];
                    for (Result *lotto in sortResult)
                    {
                        for( NSString *count in checkNo)
                        {
                            if( [lotto.count isEqualToString:count] )
                            {
                                [winCount addObject:lotto.count];
                                PLotto5 *lot = [checkDatas objectAtIndex:checkNum];
                                lot.count = lotto.count;
                                [[winResults objectAtIndex:winNum] addObject:lot];
                                break;
                            }
                        }
                    }
                    winNum++;
                }
                checkNum++;
            }
            if( [winCount count] > 0 )
            {
                // メッセージ形成
                NSMutableString *message = [[NSMutableString alloc] init];
                int counter = 1;
                for( NSMutableArray *result in winResults)
                {
                    if( [result count] > 0 )
                    {
                        for( PLotto5 *lotto in result)
                        {
                            [message appendFormat:@"\nおめでとうございます！\n第%@回抽選当選です！\n\n",lotto.count];
                            [message appendFormat:@"%d等 \n", counter];
                            [message appendFormat:@"購入日:%@ \n",lotto.date];
                            [message appendFormat:@"購入番号:%@  ",lotto.number_1];
                            [message appendFormat:@"%@  ",lotto.number_2];
                            [message appendFormat:@"%@  ",lotto.number_3];
                            [message appendFormat:@"%@  ",lotto.number_4];
                            [message appendFormat:@"%@  ",lotto.number_5];
                            [message appendFormat:@"\n"];
                        }
                    }
                    counter++;
                }
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"当選確認"
                                                                message:message
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }
}*/

- (IBAction)button1Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button1Tap:MINILOTTO];
}

- (IBAction)button2Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button2Tap:MINILOTTO];
}

- (IBAction)button3Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button3Tap:MINILOTTO];
}

- (IBAction)button4Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button4Tap:MINILOTTO];
}

- (IBAction)button5Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button5Tap:MINILOTTO];
}

- (IBAction)button6Tap:(id)sender
{
    TabBarController *tabController = (TabBarController *) self.tabBarController;
    [tabController button6Tap:MINILOTTO];
}

@end
