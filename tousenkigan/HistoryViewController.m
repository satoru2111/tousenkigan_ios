//
//  HistoryViewController.m
//  tousenkigan
//
//  Created by JCLuspo on 9/25/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "HistoryViewController.h"
#import "Utilities.h"
#import "AdManager.h"
#import "DatabaseHandler.h"

@interface HistoryViewController()

@property DatabaseHandler *databaseHandler;
@property NSArray *historyArray;
@property NSArray *labelArray;

@end

@implementation HistoryViewController

@synthesize type;

@synthesize topAdView;
@synthesize bottomAdView;


@synthesize databaseHandler;
@synthesize historyArray;
@synthesize labelArray;


- (void)viewDidLoad
{
    [super viewDidLoad];
    databaseHandler = [[DatabaseHandler alloc] init];
    
    [AdManager prepareGADBannerView:topAdView
                           position:AD_POSITION_TOP
                 rootViewController:self];
    [AdManager prepareGADBannerView:bottomAdView
                           position:AD_POSITION_BOTTOM
                 rootViewController:self];
    
    [self generateDisplayData];
    
    if (type >= MINILOTTO)
    {
        labelArray = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7"];
    } else {
        labelArray = @[@"ストレート", @"ボックス", @"セット\nストレート", @"セット\nボックス", @"ミニ"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)generateDisplayData
{
    switch (type) {
        case LOTTO7:
        {
            self.title = @"ロト7 当選結果一覧";
            break;
        }
            
        case LOTTO6:
        {
            self.title = @"ロト6 当選結果一覧";
            break;
        }
            
        case MINILOTTO:
        {
            self.title = @"ミニロト 当選結果一覧";
            break;
        }
            
        case NUMBER4:
        {
            self.title = @"ナンバーズ4 当選結果一覧";
            break;
        }
            
        case NUMBER3:
        {
            self.title = @"ナンバーズ3 当選結果一覧";
            break;
        }
    }
    
    historyArray = [databaseHandler getAllFromEntityResultByType:type];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (type >= MINILOTTO) {
    if( type == MINILOTTO){
        return 190;
    }
    
    return 220;
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [historyArray count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"tableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    // デバイダーを端まで表示
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1)
    {
        // ios8以降
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    else
    {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    if ([cell respondsToSelector:@selector(layoutMargins)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
    Result *result = historyArray[[indexPath item]];
    NSArray *hitArray = [self generateHitArrayFromResult:result];
    NSArray *prizeArray = [self generatePrizeArrayFromResult:result];
    
    UILabel *lottoHeadLabel = (UILabel *)[cell viewWithTag:1];
    lottoHeadLabel.text = [NSString stringWithFormat:@" 第%@回   %@", result.count, result.date];
    
    UILabel *numberLabel = (UILabel *)[cell viewWithTag:2];
    numberLabel.textAlignment = NSTextAlignmentCenter;
    numberLabel.text = [self numberDisplay:result.number label:numberLabel];
    numberLabel.text = [numberLabel.text stringByReplacingOccurrencesOfString:@"          " withString:@"    "];
    
    CGRect previousFrame = numberLabel.frame;
    int max = [hitArray count];
    if( type == LOTTO6 || type == LOTTO7){ max++; }
    for (int index=0; index < max; index++)
    {
        UIView *view = [[UIView alloc] init];
        view.tag = 30 + index;
        
        UIView *subview = [cell viewWithTag:view.tag];
        
        if (subview)
        {
            [subview removeFromSuperview];
        }
        
        
        UILabel *label = [[UILabel alloc] init];
        UILabel *hitLabel = [[UILabel alloc] init];
        UILabel *prizeLabel = [[UILabel alloc] init];

        if( (type == LOTTO6 || type == LOTTO7) && index >= max-1)
        {
            label.text = @"ｷｬﾘｰｵｰﾊﾞｰ";
            
            CGRect rect = previousFrame;
            rect.origin.x = 10;
            rect.origin.y = 0;
            rect.size.width = previousFrame.size.width * 0.3;
            
            label.frame = rect;
            
            rect.origin.x = rect.origin.x + rect.size.width;
            rect.size.width = previousFrame.size.width * 0.25 -10;
            
            hitLabel.frame = rect;
            hitLabel.textAlignment = NSTextAlignmentRight;
            hitLabel.text = @"";
            
            rect.origin.x = rect.origin.x + rect.size.width;
            rect.size.width = previousFrame.size.width * 0.4;
            prizeLabel.frame = rect;
            prizeLabel.textAlignment = NSTextAlignmentRight;
            prizeLabel.text = result.carryOver;
            
            previousFrame.origin.y += previousFrame.size.height + 3;
            previousFrame.size.height = rect.size.height;
            view.frame = previousFrame;
        }
        else
        {
            if (type >= MINILOTTO) {
                label.text = [NSString stringWithFormat:@"  %@%@",[labelArray objectAtIndex:index],@"等"];
            } else {
                label.text = [labelArray objectAtIndex:index];
            }
            
            CGRect rect = previousFrame;
            rect.origin.x = 0;
            rect.origin.y = 0;
            rect.size.width = previousFrame.size.width * 0.2;
            
            if (type < MINILOTTO)
            {
                rect.size.width = previousFrame.size.width * 0.3;
                label.numberOfLines = 0;
                CGFloat height = [label sizeThatFits:CGSizeMake(rect.size.width, CGFLOAT_MAX)].height;
                rect.size.height = height;
            }
            
            label.frame = rect;
            
            rect.origin.x = rect.origin.x + rect.size.width;
            rect.size.width = previousFrame.size.width * 0.35;
            if (type < MINILOTTO)
            {
                rect.size.width = previousFrame.size.width * 0.25;
            }
            hitLabel.frame = rect;
            hitLabel.textAlignment = NSTextAlignmentRight;
            hitLabel.text = [hitArray objectAtIndex:index];
            
            rect.origin.x = rect.origin.x + rect.size.width;
            rect.size.width = previousFrame.size.width * 0.4;
            prizeLabel.frame = rect;
            prizeLabel.textAlignment = NSTextAlignmentRight;
            prizeLabel.text = [prizeArray objectAtIndex:index];
            
            previousFrame.origin.y += previousFrame.size.height + 3;
            previousFrame.size.height = rect.size.height;
            view.frame = previousFrame;
        }
        
        [view addSubview:label];
        [view addSubview:hitLabel];
        [view addSubview:prizeLabel];
        
        [cell.contentView addSubview:view];
    }
    
    return cell;
}

- (NSArray *) generateHitArrayFromResult: (Result*)result
{
    NSMutableArray *hitArray = [[NSMutableArray alloc] init];
    [hitArray addObject:[NSString stringWithFormat:@"%@", result.hit1]];
    [hitArray addObject:[NSString stringWithFormat:@"%@", result.hit2]];
    [hitArray addObject:[NSString stringWithFormat:@"%@", result.hit3]];
    [hitArray addObject:[NSString stringWithFormat:@"%@", result.hit4]];
    
    if (![result.hit5 isEqual: @""])
    {
        [hitArray addObject:[NSString stringWithFormat:@"%@", result.hit5]];
    }
    
    if (![result.hit6 isEqual: @""])
    {
        [hitArray addObject:[NSString stringWithFormat:@"%@", result.hit6]];
    }
    
    return [hitArray copy];
}

- (NSArray *) generatePrizeArrayFromResult: (Result*)result
{
    NSMutableArray *prizeArray = [[NSMutableArray alloc] init];
    [prizeArray addObject:[NSString stringWithFormat:@"%@", result.price1]];
    [prizeArray addObject:[NSString stringWithFormat:@"%@", result.price2]];
    [prizeArray addObject:[NSString stringWithFormat:@"%@", result.price3]];
    [prizeArray addObject:[NSString stringWithFormat:@"%@", result.price4]];
    
    if (![result.price5 isEqual: @""])
    {
        [prizeArray addObject:[NSString stringWithFormat:@"%@", result.price5]];
    }
    
    if (![result.price6 isEqual: @""])
    {
        [prizeArray addObject:[NSString stringWithFormat:@"%@", result.price6]];
    }
    
    return [prizeArray copy];
}

- (NSString*) numberDisplay: (Number *)number label:(UILabel*)label
{
    CGFloat labelWidth = label.frame.size.width;
    CGFloat numberWidth = 0;

    NSMutableArray *numbersArray = [[NSMutableArray alloc] init];
    [numbersArray addObject:[NSString stringWithFormat:@"%i", number.num1]];
    [numbersArray addObject:[NSString stringWithFormat:@"%i", number.num2]];
    [numbersArray addObject:[NSString stringWithFormat:@"%i", number.num3]];
    
    if (type >= NUMBER4) {    
        [numbersArray addObject:[NSString stringWithFormat:@"%i", number.num4]];
    }
    
    if (number.num5 > 0)
    {
        [numbersArray addObject:[NSString stringWithFormat:@"%i", number.num5]];
    }
    
    if (number.num6 > 0)
    {
        [numbersArray addObject:[NSString stringWithFormat:@"%i", number.num6]];
    }
    
    if (number.num7 > 0)
    {
        [numbersArray addObject:[NSString stringWithFormat:@"%i", number.num7]];
    }
    
    if (number.numB1 > 0)
    {
        [numbersArray addObject:[NSString stringWithFormat:@"(%i)", number.numB1]];
    }
    
    if (number.numB2 > 0)
    {
        [numbersArray addObject:[NSString stringWithFormat:@"(%i)", number.numB2]];
    }
    
    for (NSString *number in numbersArray) {
        label.text = number;
        numberWidth += [label sizeThatFits:CGSizeMake(labelWidth, CGFLOAT_MAX)].width;
    }
    
    label.text = @" ";
    CGFloat spaceWidth = [label sizeThatFits:CGSizeMake(labelWidth, CGFLOAT_MAX)].width;
    int inBetweenCount = floor((labelWidth - numberWidth)/([numbersArray count]*spaceWidth));
    
    NSMutableString *temp = [[NSMutableString alloc] init];
    
    for (int index = 0; index < [numbersArray count]; index++) {
        
        NSString *number = numbersArray[index];
        
        if (index > 0)
        {
            for (int i = 0; i < inBetweenCount; i++) {
                [temp appendString:@" "];
            }
        }
        
        [temp appendString:number];
    }
    NSLog(@"label.text:%@, temp:%@, %d",label.text, temp,[numbersArray count]);
    
    return temp;
}

@end
