//
//  ViewController.m
//  tousenkigan
//
//  Created by Satoru Ozeki on 2014/05/06.
//  Copyright (c) 2014年 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"
#import "SVProgressHUD.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self performSelector:@selector(resetContentOffset) withObject:nil afterDelay:0.0f];
}

-(void)resetContentOffset{
    txtView_agreement.contentOffset = CGPointZero;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
