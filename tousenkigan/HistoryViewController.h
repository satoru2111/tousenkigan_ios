//
//  HistoryViewController.h
//  tousenkigan
//
//  Created by JCLuspo on 9/25/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface HistoryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property int type;

@property (strong, nonatomic) IBOutlet UIView *topAdView;
@property (strong, nonatomic) IBOutlet UIView *bottomAdView;

@end
