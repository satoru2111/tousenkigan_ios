//
//  LatestResultViewController.m
//  tousenkigan
//
//  Created by JCLuspo on 7/24/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "LatestResultViewController.h"
#import "Utilities.h"
#import "DatabaseHandler.h"
#import "HistoryViewController.h"
#import "AdManager.h"

@interface LatestResultViewController()
@property DatabaseHandler *databaseHandler;

@property NSMutableArray *numbersArray;
@property NSMutableArray *bonusArray;
@property NSMutableArray *hitArray;
@property NSMutableArray *prizeArray;
@property NSArray *labelArray;

@property NSString *count;
@property NSString *date;
@property NSString *prizeTotal;
@property NSString *prizeCarryOver;

@property UIImage *lightBlueImage;
@property UIImage *blueImage;
@property UIImage *pinkImage;
@property UIImage *grayImage;
@property int rowCount;
@end

@implementation LatestResultViewController

@synthesize type;

@synthesize historyButton;

@synthesize topAdView;
@synthesize bottomAdView;

@synthesize displayCollectionView;

@synthesize databaseHandler;

@synthesize numbersArray;
@synthesize bonusArray;
@synthesize hitArray;
@synthesize prizeArray;
@synthesize labelArray;

@synthesize count;
@synthesize date;
@synthesize prizeTotal;
@synthesize prizeCarryOver;

@synthesize lightBlueImage;
@synthesize blueImage;
@synthesize pinkImage;
@synthesize grayImage;
@synthesize rowCount;

- (void)viewDidLoad
{
    [super viewDidLoad];

    [AdManager prepareGADBannerView:topAdView
                           position:AD_POSITION_TOP
                 rootViewController:self];
    [AdManager prepareGADBannerView:bottomAdView
                           position:AD_POSITION_BOTTOM
                 rootViewController:self];
    
    databaseHandler = [[DatabaseHandler alloc] init];
    
    numbersArray = [[NSMutableArray alloc] init];
    bonusArray = [[NSMutableArray alloc] init];
    hitArray = [[NSMutableArray alloc] init];
    prizeArray = [[NSMutableArray alloc] init];
    
    [self generateDisplayData];
    rowCount = [self numberOfRows];
    
    displayCollectionView.backgroundColor = [UIColor clearColor];
    displayCollectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    lightBlueImage = [UIImage imageNamed:@"button_light_brue.9.png"];
    lightBlueImage = [Utilities createResizableImageFromNinePatchImage:lightBlueImage];
    
    blueImage = [UIImage imageNamed:@"button_brue.9.png"];
    blueImage = [Utilities createResizableImageFromNinePatchImage:blueImage];
    
    pinkImage = [UIImage imageNamed:@"button_pink.9.png"];
    pinkImage = [Utilities createResizableImageFromNinePatchImage:pinkImage];
    
    grayImage = [UIImage imageNamed:@"button_gray.9.png"];
    grayImage = [Utilities createResizableImageFromNinePatchImage:grayImage];
    
    labelArray = @[@"ストレート", @"ボックス", @"セットストレート", @"セットボックス", @"ミニ"];
    
    [historyButton setBackgroundImage:blueImage forState:UIControlStateNormal];
}

- (void) generateDisplayData {
    
    switch (type) {
        case LOTTO7:
        {
            self.title = @"ロト7 最新当選番号";
            Lotto7 *item = (Lotto7 *)[[databaseHandler getLatestOfType:type] firstObject];
            
            [numbersArray addObject:item.number_1];
            [numbersArray addObject:item.number_2];
            [numbersArray addObject:item.number_3];
            [numbersArray addObject:item.number_4];
            [numbersArray addObject:item.number_5];
            [numbersArray addObject:item.number_6];
            [numbersArray addObject:item.number_7];
            
            [bonusArray addObject:item.number_bonus_1];
            [bonusArray addObject:item.number_bonus_2];
            
            [hitArray addObject:item.hit_1];
            [hitArray addObject:item.hit_2];
            [hitArray addObject:item.hit_3];
            [hitArray addObject:item.hit_4];
            [hitArray addObject:item.hit_5];
            [hitArray addObject:item.hit_6];
            
            [prizeArray addObject:item.prize_1];
            [prizeArray addObject:item.prize_2];
            [prizeArray addObject:item.prize_3];
            [prizeArray addObject:item.prize_4];
            [prizeArray addObject:item.prize_5];
            [prizeArray addObject:item.prize_6];
            
            count = item.count;
            date = item.date;
            prizeTotal = item.prize_total;
            prizeCarryOver = item.prize_carry_over;
            
            break;
        }
            
        case LOTTO6:
        {
            self.title = @"ロト6 最新当選番号";
            Lotto6 *item = (Lotto6 *)[[databaseHandler getLatestOfType:type] firstObject];
            
            [numbersArray addObject:item.number_1];
            [numbersArray addObject:item.number_2];
            [numbersArray addObject:item.number_3];
            [numbersArray addObject:item.number_4];
            [numbersArray addObject:item.number_5];
            [numbersArray addObject:item.number_6];
            
            [bonusArray addObject:item.number_bonus_1];
            
            [hitArray addObject:item.hit_1];
            [hitArray addObject:item.hit_2];
            [hitArray addObject:item.hit_3];
            [hitArray addObject:item.hit_4];
            [hitArray addObject:item.hit_5];
            
            [prizeArray addObject:item.prize_1];
            [prizeArray addObject:item.prize_2];
            [prizeArray addObject:item.prize_3];
            [prizeArray addObject:item.prize_4];
            [prizeArray addObject:item.prize_5];
            
            count = item.count;
            date = item.date;
            prizeTotal = item.prize_total;
            prizeCarryOver = item.prize_carry_over;
            
            break;
        }
            
        case MINILOTTO:
        {
            self.title = @"ミニロト 最新当選番号";
            Lotto5 *item = (Lotto5 *)[[databaseHandler getLatestOfType:type] firstObject];
            
            [numbersArray addObject:item.number_1];
            [numbersArray addObject:item.number_2];
            [numbersArray addObject:item.number_3];
            [numbersArray addObject:item.number_4];
            [numbersArray addObject:item.number_5];
            
            [bonusArray addObject:item.number_bonus_1];
            
            [hitArray addObject:item.hit_1];
            [hitArray addObject:item.hit_2];
            [hitArray addObject:item.hit_3];
            [hitArray addObject:item.hit_4];
            
            [prizeArray addObject:item.prize_1];
            [prizeArray addObject:item.prize_2];
            [prizeArray addObject:item.prize_3];
            [prizeArray addObject:item.prize_4];
            
            count = item.count;
            date = item.date;
            prizeTotal = item.prize_total;
            
            break;
        }
            
        case NUMBER4:
        {
            self.title = @"ナンバーズ4 最新当選番号";
            Numbers4 *item = (Numbers4 *)[[databaseHandler getLatestOfType:type] firstObject];
            
            [numbersArray addObject:item.number_1];
            [numbersArray addObject:item.number_2];
            [numbersArray addObject:item.number_3];
            [numbersArray addObject:item.number_4];
            
            [hitArray addObject:item.hit_1];
            [hitArray addObject:item.hit_2];
            [hitArray addObject:item.hit_3];
            [hitArray addObject:item.hit_4];
            
            [prizeArray addObject:item.prize_1];
            [prizeArray addObject:item.prize_2];
            [prizeArray addObject:item.prize_3];
            [prizeArray addObject:item.prize_4];
            
            count = item.count;
            date = item.date;
            prizeTotal = item.prize_total;
            
            break;
        }
            
        case NUMBER3:
        {
            self.title = @"ナンバーズ3 最新当選番号";
            Numbers3 *item = (Numbers3 *)[[databaseHandler getLatestOfType:type] firstObject];
            
            [numbersArray addObject:item.number_1];
            [numbersArray addObject:item.number_2];
            [numbersArray addObject:item.number_3];
            
            [hitArray addObject:item.hit_1];
            [hitArray addObject:item.hit_2];
            [hitArray addObject:item.hit_3];
            [hitArray addObject:item.hit_4];
            [hitArray addObject:item.hit_5];
            
            [prizeArray addObject:item.prize_1];
            [prizeArray addObject:item.prize_2];
            [prizeArray addObject:item.prize_3];
            [prizeArray addObject:item.prize_4];
            [prizeArray addObject:item.prize_5];
            
            count = item.count;
            date = item.date;
            prizeTotal = item.prize_total;
            
            break;
        }
    }
    
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return rowCount;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    int lastIndex = rowCount - 1;
    
    if (section == 0 || section == 1) {
        return 1;
    } else if (type >= MINILOTTO && section == 2) {
        return [bonusArray count] + 1;
    } else if ((section >= 2 && section < lastIndex) || (type < LOTTO6 && section == lastIndex)) {
        return 3;
    } else {
        return 2;
    }
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    UIImageView *background = (UIImageView *)[cell viewWithTag:1];
    UILabel *numberLabel = (UILabel *)[cell viewWithTag:2];
    
    [background setImage:[self imageForIndexPath:indexPath]];
    numberLabel.text = [self stringForIndexPath:indexPath frame:cell.frame];
    
    if (((type >= MINILOTTO && [indexPath section] > 2) || (type < MINILOTTO && [indexPath section] > 1)) && [indexPath item] > 0) {
        numberLabel.textAlignment = NSTextAlignmentRight;
    }
    
    cell.contentView.frame = cell.bounds;
    
    //if (type <= NUMBER4 && indexPath == 0) {
        
    //}
    
    return cell;
}

- (CGSize) collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rows = rowCount;
    CGFloat height = collectionView.frame.size.height;
    
    CGFloat width = collectionView.frame.size.width;
    
    int section = [indexPath section];
    int index = [indexPath item];
    
    if (section > 1) {
        if (index == 0) {
            if (type >= MINILOTTO && section == 2) {
                width *= 0.7;
            } else if (type >= LOTTO6 && section == rowCount - 1){
                width *= 0.6;
            } else if (type <= NUMBER4 && section <= rowCount){
                width *= 0.45;
            } else {
                width *= 0.25;
            }
        }
        
        else if (index == 1) {
            if (type == LOTTO7 && section == 2) {
                width *= 0.15;
            } else if ((type == LOTTO6 || type == MINILOTTO) && section == 2) {
                width *= 0.3;
            } else if (type >= LOTTO6 && section == rowCount - 1){
                width *= 0.4;
            } else if (type <= NUMBER4 && section <= rowCount){
                width *= 0.229; // iPadAirで崩れるので0.001調整
            } else {
                width *= 0.35;
            }
        }
        
        else if (index == 2) {
            if (type == LOTTO7 && section == 2) {
                width *= 0.15;
            } else if ((type == LOTTO6 || type == MINILOTTO) && section == 2) {
                width *= 0.3;
            } else if (type <= NUMBER4 && section <= rowCount){
                width *= 0.32;
            } else {
                width *= 0.4;
            }
        }

    }
    
    return CGSizeMake(width, (height / rows));
}

- (IBAction)historyButtonTapped:(id)sender
{
    [self performSegueWithIdentifier:@"fmLatestToHistory" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"fmLatestToHistory"]) {
        HistoryViewController *destination = segue.destinationViewController;
        destination.type = self.type;
    }
}

- (int) numberOfRows
{
    switch (type) {
            
        case LOTTO6:
            return 9;
        case MINILOTTO:
            return 7;
        case NUMBER4:
            return 6;
        case NUMBER3:
            return 7;
    }
    
    return 10;
}

- (UIImage *) imageForIndexPath: (NSIndexPath *)indexPath
{
    int section = [indexPath section];
    int index = [indexPath item];
    
    if (section == 0) {
        return pinkImage;
    } else if (section == 1) {
        return lightBlueImage;
    } else if (section > 1) {
        if (index == 0) {
            return pinkImage;
        }
        
        else if (index > 0) {
            
            if (type >= LOTTO6 && section == 2) {
                return lightBlueImage;
            } else {
                return grayImage;
            }
        }
    }
    
    return grayImage;
}

- (NSString *) stringForIndexPath: (NSIndexPath *) indexPath frame: (CGRect)frame {
    
    int section = [indexPath section];
    int index = [indexPath item];
    UILabel *label = [[UILabel alloc] initWithFrame:frame];

    
    if (section == 0) {
        
        return [NSString stringWithFormat:@"第%@回  %@", count, date];
    }
    
    else if (section == 1) {
        
        CGFloat labelWidth = label.frame.size.width;
        CGFloat numberWidth = 0;
        
        for (NSString *number in numbersArray) {
            label.text = number;
            numberWidth += [label sizeThatFits:CGSizeMake(labelWidth, CGFLOAT_MAX)].width;
        }
        
        label.text = @" ";
        CGFloat spaceWidth = [label sizeThatFits:CGSizeMake(labelWidth, CGFLOAT_MAX)].width;
        int inBetweenCount = ceil((labelWidth - numberWidth)/([numbersArray count]*spaceWidth));
        
        NSMutableString *temp = [[NSMutableString alloc] init];
        
        for (NSString *number in numbersArray) {
            
            [temp appendString:number];
            
            for (int i = 0; i<inBetweenCount; i++) {
                [temp appendString:@" "];
            }
        }
        
        return temp;
    }
    
    else if (type >= MINILOTTO) {
        
        if (section == 2) {
            if (index == 0) {
                return @"ボーナス数字";
            }
            
            return [NSString stringWithFormat:@"(%@)", [bonusArray objectAtIndex:(index - 1)]];
        }
        
        else if (section > 1 && ((type >= LOTTO6 && section < rowCount - 1) || (type == MINILOTTO && section < rowCount))) {
        
            if (index == 0) {
                return [NSString stringWithFormat:@"%d等", section - 2];
            }
            
            else if (index == 1) {
                return [hitArray objectAtIndex:(section - 3)];
            }
            
            else if (index == 2) {
                return [prizeArray objectAtIndex:(section - 3)];
            }
        }
        
        else if (type >= LOTTO6){
        
            if (index == 0) {
                return @"キャリーオーバー";
            }
            
            else if (index == 1) {
                return [NSString stringWithFormat:@"%@", prizeCarryOver];
            }
        }
    
    }
    
    else if (type < MINILOTTO) {
    
        if (index == 0) {
            return [labelArray objectAtIndex:(section - 2)];
        }
        
        else if (index == 1) {
            return [hitArray objectAtIndex:(section - 2)];
        }
        
        else if (index == 2) {
            return [prizeArray objectAtIndex:(section - 2)];
        }
    }
    
    return nil;
}

@end
