//
//  HTML.h
//  tousenkigan
//
//  Created by Satoru Ozeki on 2014/06/05.
//  Copyright (c) 2014年 Satoru Ozeki. All rights reserved.
//
#import "HTMLParser.h"

@protocol HTMLDelegate;

@interface HTML : NSObject

@property (nonatomic, assign) id <HTMLDelegate> delegate;

-(int)getLatestNo:(NSString*)db;
- (BOOL)checkLotto:(int)type error:(NSError **)error;
- (BOOL) updateLotto7;
- (BOOL) updateLotto6;
- (BOOL) updateLotto5;
- (BOOL) updateNumbers4AndNumbers3;
- (void) createCoredata;
@end

@protocol HTMLDelegate <NSObject>

- (void) HTMLStartProcess;
@end
