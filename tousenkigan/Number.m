

#import "Number.h"

@implementation Number

@synthesize num1 =_num1;
@synthesize num2 =_num2;
@synthesize num3 =_num3;
@synthesize num4 =_num4;
@synthesize num5 =_num5;
@synthesize num6 =_num6;
@synthesize num7 =_num7;
@synthesize numB1 =_numB1;
@synthesize numB2 =_numB2;

-(id)init: (int) num1 num2: (int) num2 num3: (int) num3 num4: (int) num4 num5: (int) num5 num6: (int) num6 num7: (int) num7 numB1:(int) numB1 numB2:(int) numB2
{
    if(self = [super init]){
        //初期化処理
        self.num1 = num1;
        self.num2 = num2;
        self.num3 = num3;
        self.num4 = num4;
        self.num5 = num5;
        self.num6 = num6;
        self.num7 = num7;
        self.numB1 = numB1;
        self.numB2 = numB2;
    }
    
    return self;
}

@end
