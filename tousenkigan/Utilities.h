//
//  Utilities.h
//  tousenkigan
//
//  Created by JCLuspo on 7/23/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] containsString: @"iPhone" ] )
#define IS_IPHONE_5 IS_WIDESCREEN
//#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )

#define LUCKY_LOTTO_DEFAULT @[@"", @"", @"", @"LuckyNumber3", @"LuckyNumber4", @"LuckyLotto5", @"LuckyLotto6", @"LuckyLotto7"]
#define LUCKY_LOTTO_DATE_DEFAULT @[@"", @"", @"", @"LuckyNumber3Date", @"LuckyNumber4Date", @"LuckyLotto5Date", @"LuckyLotto6Date", @"LuckyLotto7Date"]

#define NUMBER3 3
#define NUMBER4 4
#define MINILOTTO 5
#define LOTTO6 6
#define LOTTO7 7

#define PURCHASED_NUMBER3 13
#define PURCHASED_NUMBER4 14
#define PURCHASED_MINILOTTO 15
#define PURCHASED_LOTTO6 16
#define PURCHASED_LOTTO7 17

#define _APPID_APPSTORE @"987094300"

+ (UIImage*)resizeImage: (UIImage *)image toSize: (CGSize)size;
+ (UIImage*)createResizableImageFromNinePatchImage:(UIImage*)ninePatchImage;
+ (NSArray*)getRGBAsFromImage:(UIImage*)image atX:(int)xx andY:(int)yy count:(int)count;
+ (NSArray *) dailyLuckyNumber: (int)size;
+ (int) randomNumberFromLotto: (int) lottoType;
+ (NSString*)formatNumberString:(NSString*)str;
+ (NSDictionary*)getDateStringArrayFromString: (NSString*)string;

@end
