//
//  SmallNumberPickerViewController.h
//  tousenkigan
//
//  Created by JCLuspo on 7/24/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SmallNumberPickerViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property int type;
@property Boolean fromRandom;
@property Boolean fromCheck;

@property (strong, nonatomic) IBOutlet UICollectionView *choiceCollectionView;
@property (strong, nonatomic) IBOutlet UICollectionView *numberCollectionView;

@property (strong, nonatomic) IBOutlet UIButton *checkButton;
@property (strong, nonatomic) IBOutlet UIButton *clearButton;

@property (strong, nonatomic) IBOutlet UIView *adView;

- (IBAction)checkButtonTapped:(id)sender;
- (IBAction)clearButtonTapped:(id)sender;

@end
