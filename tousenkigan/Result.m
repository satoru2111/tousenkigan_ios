

#import "Result.h"

@implementation Result{
    
}

@synthesize number =_number;
@synthesize count =_count;
@synthesize date =_date;
@synthesize hit1 =_hit1;
@synthesize hit2 =_hit2;
@synthesize hit3 =_hit3;
@synthesize hit4 =_hit4;
@synthesize hit5 =_hit5;
@synthesize hit6 =_hit6;
@synthesize price1 =_price1;
@synthesize price2 =_price2;
@synthesize price3 =_price3;
@synthesize price4 =_price4;
@synthesize price5 =_price5;
@synthesize price6 =_price6;
@synthesize totalPrice =_totalPrice;
@synthesize carryOver =_carryOver;

-(id)init:(Number *)number
count:(NSString *)count
date:(NSString *)date
hit1:(NSString *)hit1
hit2:(NSString *)hit2
hit3:(NSString *)hit3
hit4:(NSString *)hit4
hit5:(NSString *)hit5
hit6:(NSString *)hit6
price1:(NSString *)price1
price2:(NSString *)price2
price3:(NSString *)price3
price4:(NSString *)price4
price5:(NSString *)price5
price6:(NSString *)price6
totalPrice:(NSString *)totalPrice
carryOver:(NSString *)carryOver{
    if(self = [super init]){
        //初期化処理
        _number = number;
        _count = count;
        _date = date;
        _hit1 = hit1;
        _hit2 = hit2;
        _hit3 = hit3;
        _hit4 = hit4;
        _hit5 = hit5;
        _hit6 = hit6;
        _price1 = price1;
        _price2 = price2;
        _price3 = price3;
        _price4 = price4;
        _price5 = price5;
        _price6 = price6;
        _totalPrice = totalPrice;
        _carryOver = carryOver;
    }
    
    return self;
}

- (NSComparisonResult)comparWithCount:(Result *)item
{
    if ([self.count intValue] < [item.count intValue]) {
        return NSOrderedAscending;
    } else {
        return NSOrderedDescending;
    }
}

@end
