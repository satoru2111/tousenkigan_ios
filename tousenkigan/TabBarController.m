//
//  TabBarController.m
//  tousenkigan
//
//  Created by Satoru Ozeki on 2014/05/30.
//  Copyright (c) 2014年 Satoru Ozeki. All rights reserved.
//

#import "TabBarController.h"
#import "SVProgressHUD.h"
#import "Utilities.h"

#import "LatestResultViewController.h"
#import "NumberPickerViewController.h"
#import "SmallNumberPickerViewController.h"
#import "RandomNumberPickerViewController.h"
#import "RankingViewController.h"
#import "CheckNumberViewController.h"

#import "Loto7ViewController.h"
#import "Loto6ViewController.h"
#import "Loto5ViewController.h"
#import "Numbers4ViewController.h"
#import "Numbers3ViewController.h"
#import "DatabaseHandler.h"


#import "JTXReview.h"
#import "Utilities.h"


@interface TabBarController ()

@property int type;
@property BOOL isProcess;
@property DatabaseHandler *databaseHandler;
@property HTML *html;

@end

@implementation TabBarController

@synthesize tabBar;
@synthesize type;
@synthesize isProcess;
@synthesize databaseHandler;
@synthesize html;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.delegate = self;
    
    self.title = @"宝くじ速報　当選祈願";
    
    html = [HTML new];
    html.delegate = self;

    databaseHandler = [DatabaseHandler new];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL isCreated = [ud boolForKey:@"KEY_CREATE_COREDATA"];
    if (!isCreated) {
        isProcess = TRUE;
        
        [self showUpdatingProgressHUD];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
            [html createCoredata];
            [ud setBool:YES forKey:@"KEY_CREATE_COREDATA"];
            [ud setBool:YES forKey:@"Agreed"];
            [SVProgressHUD dismiss];
        }];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    
    //レビュー提示
    JTXReview *review = [[JTXReview alloc]initWithViewController:self];
    review.app_id = _APPID_APPSTORE;
    review.count_trigger_nexttime = 5;
    [review check];
    
    
//    self.tabBar.tintColor = [UIColor orangeColor];
//    [SVProgressHUD show]; // ぐるぐる表示
//    [SVProgressHUD showWithStatus:@"読み込み中"]; // メッセージ付きのぐるぐる
//    
//    self.title = @"宝くじ速報　当選祈願";
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    BOOL isCreated = [ud boolForKey:@"KEY_CREATE_COREDATA"];
//    if (!isCreated) {
//        HTML *html = [[HTML alloc] init];
//        [html createCoredata];
//        [ud setBool:YES forKey:@"KEY_CREATE_COREDATA"];
//    }
//    
//    [self performSelector:@selector(populateData) withObject:nil afterDelay:0.1];
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"//viewDidAppear");
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDate *lastDate = [ud objectForKey:@"KEY_DATE"];
    NSDate *checkDate = [lastDate initWithTimeInterval:(10 * 60) sinceDate:lastDate];
    NSDate *now = [NSDate date];
    NSComparisonResult result = [now compare:checkDate];
    
    switch (result) {
        case NSOrderedSame:
            // 同一時刻
//            [self updateDatabase];
            break;
        case NSOrderedAscending:
            // nowよりcheckDateのほうが未来
            break;
        case NSOrderedDescending:
            // nowよりcheckDateのほうが過去
            [self checkLottos];
            break;
    }
}

-(void)checkLottos{
    [self showUpdatingProgressHUD];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [queue addOperationWithBlock:^{
        NSError *checkError;
        for (int i=NUMBER3; i<=LOTTO7; i++) {
            if ([html checkLotto:i error:&checkError]) {
                isProcess = TRUE;
                break;
            } else if (checkError != nil) {
                break;
            }
        }
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [SVProgressHUD dismiss];
            [self performSelector:@selector(updateByLastCount) withObject:nil afterDelay:1];
        }];
    }];
}
/*- (void)updateDatabase {
    if (isProcess) {
        return;
    }
    
    isProcess = true;
    [SVProgressHUD showWithStatus:@"更新中"]; // メッセージ付きのぐるぐる
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL isCreated = [ud boolForKey:@"KEY_CREATE_COREDATA"];
    if (!isCreated) {
        HTML *html = [[HTML alloc] init];
        [html createCoredata];
        [ud setBool:YES forKey:@"KEY_CREATE_COREDATA"];
        [ud setBool:YES forKey:@"Agreed"];
    }
    
    [self performSelector:@selector(populateData) withObject:nil afterDelay:0.1];
    
    // 今の時間
    //NSDate *now = [NSDate date];
    //[ud setObject:now forKey:@"KEY_DATE"];
    
//    [SVProgressHUD dismiss];
    isProcess = false;
}*/

- (void)applicationDidBecomeActive {
    NSLog(@"//applicationDidBecomeActive");
    /*NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDate *lastDate = [ud objectForKey:@"KEY_DATE"];
    NSDate *checkDate = [lastDate initWithTimeInterval:(10 * 60) sinceDate:lastDate];
    NSDate *now = [NSDate date];
    NSComparisonResult result = [now compare:checkDate];
    
    switch (result) {
        case NSOrderedSame:
            // 同一時刻
            [self updateDatabase];
            break;
        case NSOrderedAscending:
            // nowよりcheckDateのほうが未来
            break;
        case NSOrderedDescending:
            // nowよりcheckDateのほうが過去
            [self updateDatabase];
            break;
    }*/
    
    //[self performSelectorInBackground:@selector(updateByLastCount) withObject:nil];
    
    
    [self performSelector:@selector(checkLottos) withObject:nil afterDelay:0.0f];
}

- (IBAction)refresh:(id)sender {
    isProcess = TRUE;
    [self showUpdatingProgressHUD];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [queue addOperationWithBlock:^{
        [self updateByLastCountNoCheck];
    }];
}

- (void) HTMLStartProcess {
    if (!isProcess) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
            isProcess = TRUE;
            [self showUpdatingProgressHUD];
        }];
    }
}

- (void)updateByLastCount {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL hasNumber = FALSE;
    

    NSDate *lastDate = [ud objectForKey:@"KEY_DATE"];
    NSDate *checkDate = [lastDate initWithTimeInterval:(10 * 60) sinceDate:lastDate];
    NSDate *now = [NSDate date];
    NSComparisonResult result = [now compare:checkDate];

    switch(result) {
        case NSOrderedSame: // 一致したとき
            break;
            
        case NSOrderedAscending: // date1が小さいとき
            if (isProcess) {
                isProcess = FALSE;
                [SVProgressHUD dismiss];
            }
            
            return;
            break;
            
        case NSOrderedDescending: // date1が大きいとき
            break;
    }
    
    BOOL error = false;
    
    
    if(!error && ![html updateLotto7]) {
        error = true;
    }
    if (!error && ![html updateLotto6]) {
        error = true;
    }
    if (!error && ![html updateLotto5]) {
        error = true;
    }
    if (!error && ![html updateNumbers4AndNumbers3]) {
        error = true;
    }\

    if (error) {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
            if (isProcess) {
                isProcess = FALSE;
                [SVProgressHUD dismiss];
            }
            
            if ([UIAlertController class]) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"更新エラー" message:@"更新に失敗しました。\n電波の良い場所で再度お試しください。" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:Nil];
                
            } else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"更新エラー"
                                                                message:@"更新に失敗しました。\n電波の良い場所で再度お試しください。"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }];
        
        return;
    }
    
    now = [NSDate date];
    [ud setObject:now forKey:@"KEY_DATE"];
    
    NSArray *types = [NSArray arrayWithObjects:L7, L6, L5, N4, N3, nil];
    NSArray *keys = [NSArray arrayWithObjects:@"LAST_NO_L7", @"LAST_NO_L6", @"LAST_NO_L5", @"LAST_NO_N4", @"LAST_NO_N3", nil];
    NSMutableArray *messageArray = [NSMutableArray new];
    
    for (int index = 0; index<5; index++) {
        int lastNo = [html getLatestNo:[types objectAtIndex:index]];
        int lastSavedNo = [[ud objectForKey:[keys objectAtIndex:index]] intValue];
        
        if (lastNo != lastSavedNo) {
            
            switch (index) {
                case 0:
                {
                    BOOL hasNumberSaved;
                    NSString *message = [self checkLoto7Hit:lastSavedNo hasNumberSaved:&hasNumberSaved];
                    if (message != Nil) {
                        [messageArray addObject:message];
                    }
                    [ud setObject:[NSNumber numberWithInt:lastNo] forKey:@"LAST_NO_L7"];
                    hasNumber = hasNumber || hasNumberSaved;
                    break;
                }
                case 1:
                {
                    BOOL hasNumberSaved;
                    NSString *message = [self checkLoto6Hit:lastSavedNo hasNumberSaved:&hasNumberSaved];
                    if (message != Nil) {
                        [messageArray addObject:message];
                    }
                    [ud setObject:[NSNumber numberWithInt:lastNo] forKey:@"LAST_NO_L6"];
                    hasNumber = hasNumber || hasNumberSaved;
                    break;
                }
                case 2:
                {
                    BOOL hasNumberSaved;
                    NSString *message = [self checkLoto5Hit:lastSavedNo hasNumberSaved:&hasNumberSaved];
                    if (message != Nil) {
                        [messageArray addObject:message];
                    }
                    [ud setObject:[NSNumber numberWithInt:lastNo] forKey:@"LAST_NO_L5"];
                    hasNumber = hasNumber || hasNumberSaved;
                    break;
                }
                case 3:
                {
                    BOOL hasNumberSaved;
                    NSString *message = [self checkNumbers4Hit:lastSavedNo hasNumberSaved:&hasNumberSaved];
                    if (message != Nil) {
                        [messageArray addObject:message];
                    }
                    [ud setObject:[NSNumber numberWithInt:lastNo] forKey:@"LAST_NO_N4"];
                    hasNumber = hasNumber || hasNumberSaved;
                    break;
                }
                case 4:
                {
                    BOOL hasNumberSaved;
                    NSString *message = [self checkNumbers3Hit:lastSavedNo hasNumberSaved:&hasNumberSaved];
                    if (message != Nil) {
                        [messageArray addObject:message];
                    }
                    [ud setObject:[NSNumber numberWithInt:lastNo] forKey:@"LAST_NO_N3"];
                    hasNumber = hasNumber || hasNumberSaved;
                    break;
                }
                default:
                    break;
            }
        }
    }
    
    //on first load, viewdidappeared not triggered.
    if (self.selectedIndex == 0) {
        Loto7ViewController* loto7 = (Loto7ViewController*)self.selectedViewController;
        [loto7 updateButtonTitle];
    }
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
        
        if (isProcess) {
            isProcess = FALSE;
            [SVProgressHUD dismiss];
        }
        
        if ([messageArray count] > 0) {
            
            NSMutableString *message = [NSMutableString new];
            for (NSString *mess in messageArray) {
                [message appendString:mess];
            }
            
            if ([UIAlertController class]) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"当選確認" message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:Nil];
                
            } else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"自動当選確認"
                                                                message:message
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
        } else if (hasNumber) {
            
            if ([UIAlertController class]) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"自動当選確認" message:@"更新データに当選はありませんでした。" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:Nil];
                
            } else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"自動当選確認"
                                                                message:@"更新データに当選はありませんでした。"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }];
}

- (void)updateByLastCountNoCheck{
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL hasNumber = FALSE;
    
    NSDate *now = [NSDate date];

    BOOL error = false;
    
    
    if(!error && ![html updateLotto7]) {
        error = true;
    }
    if (!error && ![html updateLotto6]) {
        error = true;
    }
    if (!error && ![html updateLotto5]) {
        error = true;
    }
    if (!error && ![html updateNumbers4AndNumbers3]) {
        error = true;
    }
    
    if (error) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
            
            if (isProcess) {
                isProcess = FALSE;
                [SVProgressHUD dismiss];
            }
            
            if ([UIAlertController class]) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"更新エラー" message:@"更新に失敗しました。\n電波の良い場所で再度お試しください。" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:Nil];
                
            } else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"更新エラー"
                                                                message:@"更新に失敗しました。\n電波の良い場所で再度お試しください。"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }];
        
        return;
    }
    
    now = [NSDate date];
    [ud setObject:now forKey:@"KEY_DATE"];
    
    NSArray *types = [NSArray arrayWithObjects:L7, L6, L5, N4, N3, nil];
    NSArray *keys = [NSArray arrayWithObjects:@"LAST_NO_L7", @"LAST_NO_L6", @"LAST_NO_L5", @"LAST_NO_N4", @"LAST_NO_N3", nil];
    NSMutableArray *messageArray = [NSMutableArray new];
    
    for (int index = 0; index<5; index++) {
        int lastNo = [html getLatestNo:[types objectAtIndex:index]];
        int lastSavedNo = [[ud objectForKey:[keys objectAtIndex:index]] intValue];
        
        if (lastNo != lastSavedNo) {
            
            switch (index) {
                case 0:
                {
                    BOOL hasNumberSaved;
                    NSString *message = [self checkLoto7Hit:lastSavedNo hasNumberSaved:&hasNumberSaved];
                    if (message != Nil) {
                        [messageArray addObject:message];
                    }
                    [ud setObject:[NSNumber numberWithInt:lastNo] forKey:@"LAST_NO_L7"];
                    hasNumber = hasNumber || hasNumberSaved;
                    break;
                }
                case 1:
                {
                    BOOL hasNumberSaved;
                    NSString *message = [self checkLoto6Hit:lastSavedNo hasNumberSaved:&hasNumberSaved];
                    if (message != Nil) {
                        [messageArray addObject:message];
                    }
                    [ud setObject:[NSNumber numberWithInt:lastNo] forKey:@"LAST_NO_L6"];
                    hasNumber = hasNumber || hasNumberSaved;
                    break;
                }
                case 2:
                {
                    BOOL hasNumberSaved;
                    NSString *message = [self checkLoto5Hit:lastSavedNo hasNumberSaved:&hasNumberSaved];
                    if (message != Nil) {
                        [messageArray addObject:message];
                    }
                    [ud setObject:[NSNumber numberWithInt:lastNo] forKey:@"LAST_NO_L5"];
                    hasNumber = hasNumber || hasNumberSaved;
                    break;
                }
                case 3:
                {
                    BOOL hasNumberSaved;
                    NSString *message = [self checkNumbers4Hit:lastSavedNo hasNumberSaved:&hasNumberSaved];
                    if (message != Nil) {
                        [messageArray addObject:message];
                    }
                    [ud setObject:[NSNumber numberWithInt:lastNo] forKey:@"LAST_NO_N4"];
                    hasNumber = hasNumber || hasNumberSaved;
                    break;
                }
                case 4:
                {
                    BOOL hasNumberSaved;
                    NSString *message = [self checkNumbers3Hit:lastSavedNo hasNumberSaved:&hasNumberSaved];
                    if (message != Nil) {
                        [messageArray addObject:message];
                    }
                    [ud setObject:[NSNumber numberWithInt:lastNo] forKey:@"LAST_NO_N3"];
                    hasNumber = hasNumber || hasNumberSaved;
                    break;
                }
                default:
                    break;
            }
        }
    }
    
    //on first load, viewdidappeared not triggered.
    
    if ([NSThread isMainThread]) {
        if (self.selectedIndex == 0) {
            Loto7ViewController* loto7 = (Loto7ViewController*)self.selectedViewController;
            [loto7 updateButtonTitle];
        }
    } else {
        dispatch_sync (
            dispatch_get_main_queue(),
            ^{
                if (self.selectedIndex == 0) {
                    Loto7ViewController* loto7 = (Loto7ViewController*)self.selectedViewController;
                    [loto7 updateButtonTitle];
                }
            }
        );
    }
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
        
        if (isProcess) {
            isProcess = FALSE;
            [SVProgressHUD dismiss];
        }
        
        if ([messageArray count] > 0) {
            
            NSMutableString *message = [NSMutableString new];
            for (NSString *mess in messageArray) {
                [message appendString:mess];
            }
            
            if ([UIAlertController class]) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"当選確認" message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:Nil];
                
            } else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"自動当選確認"
                                                                message:message
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
        } else if (hasNumber) {
            
            if ([UIAlertController class]) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"自動当選確認" message:@"更新データに当選はありませんでした。" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:Nil];
                
            } else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"自動当選確認"
                                                                message:@"更新データに当選はありませんでした。"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }];
}

- (void)button1Tap:(int)sender
{
    NSLog(@"Button 1: %d", sender);
    self.type = sender;
    [self performSegueWithIdentifier:@"fmTabToLatestResult" sender:self];
}

- (void)button2Tap:(int)sender
{
    NSLog(@"Button 2: %d", sender);
    
    self.type = sender;
    
    if(self.type >= MINILOTTO) {
        [self performSegueWithIdentifier:@"fmTabToNumberPicker" sender:self];
    } else {
        [self performSegueWithIdentifier:@"fmTabToSmallNumberPicker" sender:self];
    }
}

- (void)button3Tap:(int)sender
{
    NSLog(@"Button 3: %d", sender);
    self.type = sender;
    [self performSegueWithIdentifier:@"fmTabToRandomNumber" sender:self];
}

- (void)button4Tap:(int)sender
{
    NSLog(@"Button 4: %d", sender);
    self.type = sender;
    [self performSegueWithIdentifier:@"fmTabToRanking" sender:self];
}

- (void)button5Tap:(int)sender
{
    NSLog(@"Button 5: %d", sender);
    self.type = sender;
    [self performSegueWithIdentifier:@"fmTabToCheckNumber" sender:self];
}

- (void)button6Tap:(int)sender
{
    NSLog(@"Button 6: %d", sender);
    self.type = sender;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"fmTabToLatestResult"]) {
        LatestResultViewController *destination = segue.destinationViewController;
        destination.type = self.type;
    }
    
    else if ([[segue identifier] isEqualToString:@"fmTabToNumberPicker"]) {
        NumberPickerViewController *destination = segue.destinationViewController;
        destination.type = self.type;
    }
    
    else if ([[segue identifier] isEqualToString:@"fmTabToSmallNumberPicker"]) {
        SmallNumberPickerViewController *destination = segue.destinationViewController;
        destination.type = self.type;
    }
    
    else if ([[segue identifier] isEqualToString:@"fmTabToRandomNumber"]) {
        RandomNumberPickerViewController *destination = segue.destinationViewController;
        destination.type = self.type;
    }
    
    else if ([[segue identifier] isEqualToString:@"fmTabToRanking"]) {
        RankingViewController *destination = segue.destinationViewController;
        destination.type = self.type;
    }
    
    else if ([[segue identifier] isEqualToString:@"fmTabToCheckNumber"]) {
        CheckNumberViewController *destination = segue.destinationViewController;
        destination.type = self.type;
    }
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    //[self updateDatabase];
    //[self updateByLastCount:NO];
}

#pragma mark CheckHit -

-(NSString *)checkLoto7Hit:(int)lastNo hasNumberSaved:(BOOL *)hasNumberSaved
{
    NSArray *result = [databaseHandler getLatestOfType:LOTTO7];
    NSMutableString *message = nil;
    *hasNumberSaved = FALSE;
    
    if ([result count] > 0) {
        
        Lotto7 *latestItem = result[0];
        
        // 最新当選番号と照合
        NSMutableArray *checkList = [[NSMutableArray alloc] init];
        NSMutableArray *checkDatas = [[databaseHandler getAllFromEntityByType:(LOTTO7 + 10)] mutableCopy];
        NSMutableArray *checkNo = [[NSMutableArray alloc] init];
        
        if ([checkDatas count] > 0) {
            *hasNumberSaved = TRUE;
        }
        
        for (PLotto7 *item in checkDatas) {
            NSArray *numbers = [[NSArray alloc] initWithObjects:item.number_1,item.number_2,item.number_3 ,item.number_4,item.number_5,item.number_6,item.number_7, nil];
            NSArray *results = [databaseHandler queryHit:numbers ofType:LOTTO7];
            [checkList addObject:results];
        }
        
        for( int i = lastNo; i < [latestItem.count intValue]+1; i++)
        {
            [checkNo addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        if( [checkList count] > 0 )
        {
            // 6等分のカウンタ
            NSMutableArray *winResults = [[NSMutableArray alloc] init];
            for(int i = 0; i < 6; i++)
            {
                [winResults addObject:[[NSMutableArray alloc] init]];
            }
            
            // 当選結果格納
            NSMutableArray *winCount = [[NSMutableArray alloc] init];
            int checkNum = 0;
            for(NSArray *results in checkList)
            {
                int winNum=0;
                for (NSArray *winResult in results)
                {
                    NSArray *sortResult = [winResult sortedArrayUsingSelector:@selector(comparWithCount:)];
                    for (Result *lotto in sortResult)
                    {
                        for( NSString *count in checkNo)
                        {
                            if( [lotto.count isEqualToString:count] )
                            {
                                [winCount addObject:lotto.count];
                                PLotto7 *lot = [checkDatas objectAtIndex:checkNum];
                                lot.count = lotto.count;
                                [[winResults objectAtIndex:winNum] addObject:lot];
                                break;
                            }
                        }
                    }
                    winNum++;
                }
                checkNum++;
            }
            
            NSMutableSet *singles = [NSMutableSet set];
            
            if( [winCount count] > 0 )
            {
                // メッセージ形成
                message = [[NSMutableString alloc] init];
                int counter = 1;
                for( NSMutableArray *result in winResults)
                {
                    if( [result count] > 0 )
                    {
                        for( PLotto7 *lotto in result)
                        {
                            if (![singles containsObject:lotto.count]) {
                                [singles addObject:lotto.count];
                            } else {
                                continue;
                            }
                            
                            [message appendFormat:@"\nおめでとうございます！\n第%@回 当選です！\n\n",lotto.count];
                            [message appendFormat:@"%d等 \n", counter];
                            [message appendFormat:@"日時:%@ \n",lotto.date];
                            [message appendFormat:@"購入番号:%@  ",lotto.number_1];
                            [message appendFormat:@"%@  ",lotto.number_2];
                            [message appendFormat:@"%@  ",lotto.number_3];
                            [message appendFormat:@"%@  ",lotto.number_4];
                            [message appendFormat:@"%@  ",lotto.number_5];
                            [message appendFormat:@"%@  ",lotto.number_6];
                            [message appendFormat:@"%@  ",lotto.number_7];
                            [message appendFormat:@"\n"];
                        }
                    }
                    counter++;
                }
            }
        }
    }
    
    return message;
}

-(NSString *)checkLoto6Hit:(int)lastNo hasNumberSaved:(BOOL *)hasNumberSaved
{
    NSArray *result = [databaseHandler getLatestOfType:LOTTO6];
    NSMutableString *message = nil;
    *hasNumberSaved = FALSE;
    
    if ([result count] > 0) {
        Lotto6 *latestItem = result[0];
        
        // 最新当選番号と照合
        NSMutableArray *checkList = [[NSMutableArray alloc] init];
        NSMutableArray *checkDatas = [[databaseHandler getAllFromEntityByType:(LOTTO6 + 10)] mutableCopy];
        NSMutableArray *checkNo = [[NSMutableArray alloc] init];
        
        if ([checkDatas count] > 0) {
            *hasNumberSaved = TRUE;
        }
        
        for (PLotto6 *item in checkDatas) {
            NSArray *numbers = [[NSArray alloc] initWithObjects:item.number_1,item.number_2,item.number_3 ,item.number_4,item.number_5,item.number_6, nil];
            NSArray *results = [databaseHandler queryHit:numbers ofType:LOTTO6];
            [checkList addObject:results];
        }
        
        for( int i = lastNo; i <= [latestItem.count intValue]; i++)
        {
            [checkNo addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        if( [checkList count] > 0 )
        {
            // 5等分のカウンタ
            NSMutableArray *winResults = [[NSMutableArray alloc] init];
            for(int i = 0; i < 5; i++)
            {
                [winResults addObject:[[NSMutableArray alloc] init]];
            }
            
            // 当選結果格納
            NSMutableArray *winCount = [[NSMutableArray alloc] init];
            int checkNum = 0;
            for(NSArray *results in checkList)
            {
                int winNum = 0;
                for (NSArray *winResult in results)
                {
                    NSArray *sortResult = [winResult sortedArrayUsingSelector:@selector(comparWithCount:)];
                    for (Result *lotto in sortResult)
                    {
                        for( NSString *count in checkNo)
                        {
                            if( [lotto.count isEqualToString:count] )
                            {
                                [winCount addObject:lotto.count];
                                PLotto6 *lot = [checkDatas objectAtIndex:checkNum];
                                lot.count = lotto.count;
                                [[winResults objectAtIndex:winNum] addObject:lot];
                                break;
                            }
                        }
                    }
                    winNum++;
                }
                checkNum++;
            }
            
            NSMutableSet *singles = [NSMutableSet set];
            
            if( [winCount count] > 0 )
            {
                // メッセージ形成
                message = [[NSMutableString alloc] init];
                int counter = 1;
                for( NSMutableArray *result in winResults)
                {
                    if( [result count] > 0 )
                    {
                        for( PLotto6 *lotto in result)
                        {
                            if (![singles containsObject:lotto.count]) {
                                [singles addObject:lotto.count];
                            } else {
                                continue;
                            }
                            
                            [message appendFormat:@"\nおめでとうございます！\n第%@回 当選です！\n\n",lotto.count];
                            [message appendFormat:@"%d等 \n", counter];
                            [message appendFormat:@"日時:%@ \n",lotto.date];
                            [message appendFormat:@"購入番号:%@  ",lotto.number_1];
                            [message appendFormat:@"%@  ",lotto.number_2];
                            [message appendFormat:@"%@  ",lotto.number_3];
                            [message appendFormat:@"%@  ",lotto.number_4];
                            [message appendFormat:@"%@  ",lotto.number_5];
                            [message appendFormat:@"%@  ",lotto.number_6];
                            [message appendFormat:@"\n"];
                        }
                    }
                    counter++;
                }
            }
        }
    }
    
    return message;
}

-(NSString *)checkLoto5Hit:(int)lastNo hasNumberSaved:(BOOL *)hasNumberSaved
{
    NSArray *result = [databaseHandler getLatestOfType:MINILOTTO];
    NSMutableString *message = nil;
    *hasNumberSaved = FALSE;
    
    if ([result count] > 0) {
        Lotto5 *latestItem = result[0];
        
        // 最新当選番号と照合
        NSMutableArray *checkList = [[NSMutableArray alloc] init];
        NSMutableArray *checkDatas = [[databaseHandler getAllFromEntityByType:(MINILOTTO + 10)] mutableCopy];
        NSMutableArray *checkNo = [[NSMutableArray alloc] init];
        
        if ([checkDatas count] > 0) {
            *hasNumberSaved = TRUE;
        }
        
        for (PLotto5 *item in checkDatas) {
            NSArray *numbers = [[NSArray alloc] initWithObjects:item.number_1,item.number_2,item.number_3 ,item.number_4,item.number_5, nil];
            NSArray *results = [databaseHandler queryHit:numbers ofType:MINILOTTO];
            [checkList addObject:results];
        }
        
        for( int i = lastNo; i <= [latestItem.count intValue]; i++)
        {
            [checkNo addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        if( [checkList count] > 0 )
        {
            // 4等分のカウンタ
            NSMutableArray *winResults = [[NSMutableArray alloc] init];
            for(int i = 0; i < 4; i++)
            {
                [winResults addObject:[[NSMutableArray alloc] init]];
            }
            
            // 当選結果格納
            NSMutableArray *winCount = [[NSMutableArray alloc] init];
            int checkNum = 0;
            for(NSArray *results in checkList)
            {
                int winNum = 0;
                for (NSArray *winResult in results)
                {
                    NSArray *sortResult = [winResult sortedArrayUsingSelector:@selector(comparWithCount:)];
                    for (Result *lotto in sortResult)
                    {
                        for( NSString *count in checkNo)
                        {
                            if( [lotto.count isEqualToString:count] )
                            {
                                [winCount addObject:lotto.count];
                                PLotto5 *lot = [checkDatas objectAtIndex:checkNum];
                                lot.count = lotto.count;
                                [[winResults objectAtIndex:winNum] addObject:lot];
                                break;
                            }
                        }
                    }
                    winNum++;
                }
                checkNum++;
            }
            
            NSMutableSet *singles = [NSMutableSet set];
            
            if( [winCount count] > 0 )
            {
                // メッセージ形成
                message = [[NSMutableString alloc] init];
                int counter = 1;
                for( NSMutableArray *result in winResults)
                {
                    if( [result count] > 0 )
                    {
                        for( PLotto5 *lotto in result)
                        {
                            if (![singles containsObject:lotto.count]) {
                                [singles addObject:lotto.count];
                            } else {
                                continue;
                            }
                            
                            [message appendFormat:@"\nおめでとうございます！\n第%@回 当選です！\n\n",lotto.count];
                            [message appendFormat:@"%d等 \n", counter];
                            [message appendFormat:@"日時:%@ \n",lotto.date];
                            [message appendFormat:@"購入番号:%@  ",lotto.number_1];
                            [message appendFormat:@"%@  ",lotto.number_2];
                            [message appendFormat:@"%@  ",lotto.number_3];
                            [message appendFormat:@"%@  ",lotto.number_4];
                            [message appendFormat:@"%@  ",lotto.number_5];
                            [message appendFormat:@"\n"];
                        }
                    }
                    counter++;
                }
            }
        }
    }
    
    return message;
}

-(NSString *)checkNumbers4Hit:(int)lastNo hasNumberSaved:(BOOL *)hasNumberSaved
{
    NSArray *result = [databaseHandler getLatestOfType:NUMBER4];
    NSMutableString *message = Nil;
    *hasNumberSaved = FALSE;
    
    if ([result count] > 0) {
        Numbers4 *latestItem = result[0];

        
        // 最新当選番号と照合
        NSMutableArray *checkList = [[NSMutableArray alloc] init];
        NSMutableArray *checkDatas = [[databaseHandler getAllFromEntityByType:(NUMBER4 + 10)] mutableCopy];
        
        if ([checkDatas count] > 0) {
            *hasNumberSaved = TRUE;
        }
        
        for (PNumbers4 *item in checkDatas) {
            NSArray *numbers = [[NSArray alloc] initWithObjects:item.number_1,item.number_2,item.number_3 ,item.number_4, nil];
            NSArray *results = [databaseHandler queryHit:numbers ofType:NUMBER4];
            [checkList addObject:results];
        }
        
        NSMutableArray *checkNo = [[NSMutableArray alloc] init];
        for( int i = lastNo; i <= [latestItem.count intValue]; i++)
        {
            [checkNo addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        if( [checkList count] > 0 )
        {
            // 4等分のカウンタ
            NSMutableArray *winResults = [[NSMutableArray alloc] init];
            for(int i = 0; i < 2; i++)
            {
                [winResults addObject:[[NSMutableArray alloc] init]];
            }
            
            // 当選結果格納
            NSMutableArray *winCount = [[NSMutableArray alloc] init];
            int checkNum = 0;
            for(NSArray *results in checkList)
            {
                int winNum = 0;
                for (NSArray *winResult in results)
                {
                    NSArray *sortResult = [winResult sortedArrayUsingSelector:@selector(comparWithCount:)];
                    for (Result *lotto in sortResult)
                    {
                        for( NSString *count in checkNo)
                        {
                            if( [lotto.count isEqualToString:count] )
                            {
                                [winCount addObject:lotto.count];
                                PNumbers4 *lot = [checkDatas objectAtIndex:checkNum];
                                lot.count = lotto.count;
                                [[winResults objectAtIndex:winNum] addObject:lot];
                                break;
                            }
                        }
                    }
                    winNum++;
                }
                checkNum++;
            }
            
            NSMutableSet *singles = [NSMutableSet set];
            
            if( [winCount count] > 0 )
            {
                // メッセージ形成
                NSArray *winName = [[NSArray alloc] initWithObjects:@"ストレート",@"ボックス", nil];
                message = [[NSMutableString alloc] init];
                int counter = 0;
                for( NSMutableArray *result in winResults)
                {
                    if( [result count] > 0 )
                    {
                        for( PNumbers4 *lotto in result)
                        {
                            if (![singles containsObject:lotto.count]) {
                                [singles addObject:lotto.count];
                            } else {
                                continue;
                            }
                            
                            [message appendFormat:@"\nおめでとうございます！\n第%@回 当選です！\n\n",lotto.count];
                            [message appendFormat:@"%@ \n", [winName objectAtIndex:counter]];
                            [message appendFormat:@"日時:%@ \n",lotto.date];
                            [message appendFormat:@"購入番号:%@  ",lotto.number_1];
                            [message appendFormat:@"%@  ",lotto.number_2];
                            [message appendFormat:@"%@  ",lotto.number_3];
                            [message appendFormat:@"%@  ",lotto.number_4];
                            [message appendFormat:@"\n"];
                        }
                    }
                    counter++;
                }
            }
        }
    }
    
    return message;
}

-(NSString *)checkNumbers3Hit:(int)lastNo hasNumberSaved:(BOOL *)hasNumberSaved
{
    NSArray *result = [databaseHandler getLatestOfType:NUMBER3];
    NSMutableString *message = Nil;
    *hasNumberSaved = FALSE;
    
    if ([result count] > 0) {
        Numbers3 *latestItem = result[0];
        
        // 最新当選番号と照合
        NSMutableArray *checkList = [[NSMutableArray alloc] init];
        NSMutableArray *checkDatas = [[databaseHandler getAllFromEntityByType:(NUMBER3 + 10)] mutableCopy];
        
        if ([checkDatas count] > 0) {
            *hasNumberSaved = TRUE;
        }
        
        for (PNumbers3 *item in checkDatas) {
            NSArray *numbers = [[NSArray alloc] initWithObjects:item.number_1,item.number_2,item.number_3, nil];
            NSArray *results = [databaseHandler queryHit:numbers ofType:NUMBER3];
            [checkList addObject:results];
        }
        
        NSMutableArray *checkNo = [[NSMutableArray alloc] init];
        for( int i = lastNo; i <= [latestItem.count intValue]; i++)
        {
            [checkNo addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        if( [checkList count] > 0 )
        {
            // 3等分のカウンタ
            NSMutableArray *winResults = [[NSMutableArray alloc] init];
            for(int i = 0; i < 3; i++)
            {
                [winResults addObject:[[NSMutableArray alloc] init]];
            }
            
            // 当選結果格納
            NSMutableArray *winCount = [[NSMutableArray alloc] init];
            int checkNum = 0;
            for(NSArray *results in checkList)
            {
                int winNum = 0;
                BOOL straightWin = false;
                for (NSArray *winResult in results)
                {
                    // ストレートが当たればそれ以降は見なくていい
                    if(straightWin){ break;}
                    NSArray *sortResult = [winResult sortedArrayUsingSelector:@selector(comparWithCount:)];
                    for (Result *lotto in sortResult)
                    {
                        for( NSString *count in checkNo)
                        {
                            if( [lotto.count isEqualToString:count] )
                            {
                                if(winNum == 0){straightWin = true;}
                                [winCount addObject:lotto.count];
                                PNumbers3 *lot = [checkDatas objectAtIndex:checkNum];
                                lot.count = lotto.count;
                                [[winResults objectAtIndex:winNum] addObject:lot];
                                break;
                            }
                        }
                    }
                    winNum++;
                }
                checkNum++;
            }
            
            NSMutableSet *singles = [NSMutableSet set];
            
            if( [winCount count] > 0 )
            {
                // メッセージ形成
                NSArray *winName = [[NSArray alloc] initWithObjects:@"ストレート",@"ボックス",@"ミニ", nil];
                message = [[NSMutableString alloc] init];
                int counter = 0;
                for( NSMutableArray *result in winResults)
                {
                    if( [result count] > 0 )
                    {
                        for( PNumbers3 *lotto in result)
                        {
                            if (![singles containsObject:lotto.count]) {
                                [singles addObject:lotto.count];
                            } else {
                                continue;
                            }
                            
                            [message appendFormat:@"\nおめでとうございます！\n第%@回 当選です！\n\n",lotto.count];
                            [message appendFormat:@"%@ \n", [winName objectAtIndex:counter]];
                            [message appendFormat:@"日時:%@ \n",lotto.date];
                            [message appendFormat:@"購入番号:%@  ",lotto.number_1];
                            [message appendFormat:@"%@  ",lotto.number_2];
                            [message appendFormat:@"%@  ",lotto.number_3];
                            [message appendFormat:@"\n"];
                        }
                    }
                    counter++;
                }
            }
        }
    }
    
    return message;
}

-(void) showUpdatingProgressHUD{
    //HUD表示中は操作不能にする
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"更新中"];
}
@end
