//
//  Rank.h
//  tousenkigan
//
//  Created by JCLuspo on 8/18/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rank : NSObject

@property NSString *number1;
@property NSString *number2;
@property NSString *number3;
@property NSString *number4;
@property int count;
@property NSString *compareString;

- (id)initWithNumbers: (NSArray *)numbers;
- (id)initWithNumber: (NSString *)num atIndex: (int)index;
- (void) countUp;

@end
