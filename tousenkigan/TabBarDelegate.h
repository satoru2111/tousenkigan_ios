//
//  AppDelegate_TabBarDelegate_h.h
//  tousenkigan
//
//  Created by Satoru Ozeki on 2014/05/10.
//  Copyright (c) 2014年 Satoru Ozeki. All rights reserved.
//

@interface TabBarSampleAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate> {
    UIWindow *window;
    UITabBarController *myTabBarController;
}
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) UITabBarController *myTabBarController;
