//
//  NumberPickerViewController.m
//  tousenkigan
//
//  Created by JCLuspo on 7/23/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "NumberPickerViewController.h"
#import "RandomNumberPickerViewController.h"
#import "CheckNumberViewController.h"
#import "DatabaseHandler.h"
#import "Utilities.h"
#import "AdManager.h"

@interface NumberPickerViewController ()
@property DatabaseHandler *databaseHandler;
@property NSMutableArray *selectedIndexArray;
@property UIImage *selectedImage;
@property UIImage *unselectedImage;
@end

@implementation NumberPickerViewController

@synthesize type;
@synthesize fromRandom;
@synthesize fromCheck;

@synthesize numberCollectionView;
@synthesize checkButton;
@synthesize clearButton;

@synthesize adView;

@synthesize databaseHandler;
@synthesize selectedIndexArray;
@synthesize selectedImage;
@synthesize unselectedImage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [AdManager prepareGADBannerView:adView
                           position:AD_POSITION_TOP
                 rootViewController:self];
    
    databaseHandler = [[DatabaseHandler alloc] init];
    
    numberCollectionView.backgroundColor = [UIColor clearColor];
    numberCollectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    selectedIndexArray = [[NSMutableArray alloc] init];
    
    unselectedImage = [UIImage imageNamed:@"button_gray.9.png"];
    unselectedImage = [Utilities createResizableImageFromNinePatchImage:unselectedImage];
    
    selectedImage = [UIImage imageNamed:@"button_brue.9.png"];
    selectedImage = [Utilities createResizableImageFromNinePatchImage:selectedImage];
    
    UIImage *checkImage = [UIImage imageNamed:@"button_pink.9.png"];
    checkImage = [Utilities createResizableImageFromNinePatchImage:checkImage];
    [checkButton setBackgroundImage:checkImage forState:UIControlStateNormal];
    
    UIImage *ckearImage = [UIImage imageNamed:@"button_gray.9.png"];
    ckearImage = [Utilities createResizableImageFromNinePatchImage:ckearImage];
    [clearButton setBackgroundImage:ckearImage forState:UIControlStateNormal];
    
    if (fromRandom) {
        switch (type) {
            case LOTTO7:
            {
                self.title = @"ロト7 祈願番号指定";
                break;
            }
                
            case LOTTO6:
            {
                self.title = @"ロト6 祈願番号指定";
                break;
            }
                
            case MINILOTTO:
            {
                self.title = @"ミニロト 祈願番号指定";
                break;
            }
                
            case NUMBER4:
            {
                self.title = @"ナンバーズ4 祈願番号指定";
                break;
            }
                
            case NUMBER3:
            {
                self.title = @"ナンバーズ3 祈願番号指定";
                break;
            }
        }
        
        [checkButton setTitle:@" 当選祈願 " forState:UIControlStateNormal];
    } else if (fromCheck){
        switch (type) {
            case LOTTO7:
            {
                self.title = @"ロト7 自動当選確認";
                break;
            }
                
            case LOTTO6:
            {
                self.title = @"ロト6 自動当選確認";
                break;
            }
                
            case MINILOTTO:
            {
                self.title = @"ミニロト 自動当選確認";
                break;
            }
                
            case NUMBER4:
            {
                self.title = @"ナンバーズ4 自動当選確認";
                break;
            }
                
            case NUMBER3:
            {
                self.title = @"ナンバーズ3 自動当選確認";
                break;
            }
        }
        [checkButton setTitle:@"　追加　" forState:UIControlStateNormal];
        [checkButton setEnabled:FALSE];
    } else {
        switch (type) {
            case LOTTO7:
            {
                self.title = @"ロト7 当たってるかな？";
                break;
            }
                
            case LOTTO6:
            {
                self.title = @"ロト6 当たってるかな？";
                break;
            }
                
            case MINILOTTO:
            {
                self.title = @"ミニロト 当たってるかな？";
                break;
            }
                
            case NUMBER4:
            {
                self.title = @"ナンバーズ4 当たってるかな？";
                break;
            }
                
            case NUMBER3:
            {
                self.title = @"ナンバーズ3 当たってるかな？";
                break;
            }
        }
        [checkButton setTitle:@" 当選確認 " forState:UIControlStateNormal];
        [checkButton setEnabled:FALSE];
    }

    NSArray *test = [databaseHandler getAllFromEntity:L7 withCountSort:YES];
    for (Lotto7 *item in test)
    {
        NSLog(@"%@ %@ %@ %@ %@ %@ %@", item.number_1, item.number_2, item.number_3, item.number_4, item.number_5, item.number_6, item.number_7);
    }
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (type) {
        
        case LOTTO6:
            return 43;
            
        case MINILOTTO:
            return 31;
    }
    
    return 37;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    // １セルあたりのサイズを計算
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    NSUInteger space = 10;
    NSUInteger width = (screenSize.size.width - space * 2) / 5;
    CGSize listCellSize = CGSizeMake(width,
                                     width/2);
    return listCellSize;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    UIImageView *background = (UIImageView *)[cell viewWithTag:1];
    UILabel *numberLabel = (UILabel *)[cell viewWithTag:2];
    
    [background setImage:unselectedImage];
    
    int index = [indexPath item];
    
    numberLabel.text = [NSString stringWithFormat:@"%d", index + 1];
    
    CGRect collectionFrame = collectionView.frame;
    CGRect cellFrame = cell.frame;
    CGFloat cellHeight = collectionFrame.size.height / 9;
    
    cellFrame.origin.y = (floor(index / 5) * cellHeight);
    cellFrame.size.height = cellHeight;
    [cell setFrame:cellFrame];
    
    cell.contentView.frame = cell.bounds;
    
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"clicked: %li", (long)[indexPath item]);
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    UIImageView *background = (UIImageView *)[cell viewWithTag:1];
    
    if (![selectedIndexArray containsObject:indexPath] && [selectedIndexArray count] < type) {
        
        [background setImage:selectedImage];
        [selectedIndexArray addObject:indexPath];
        
    } else {
        
        [background setImage:unselectedImage];
        [selectedIndexArray removeObject:indexPath];
    }
    
    if (fromRandom || [selectedIndexArray count] == type) {
        [checkButton setEnabled:TRUE];
    } else {
        [checkButton setEnabled:FALSE];
    }
}

- (IBAction)checkButtonTapped:(id)sender
{
    NSMutableArray *selectedNumbers = [[NSMutableArray alloc] init];
    
    for (NSIndexPath *indexPath in selectedIndexArray) {
        [selectedNumbers addObject:[NSNumber numberWithInt:([indexPath item] + 1)]];
    }
    
    [selectedNumbers sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES]]];
    
    //call or transition to results.
    if (fromRandom) {
        
        int previousViewIndex = [self.navigationController.viewControllers count] - 2;
        
        RandomNumberPickerViewController *previous = [self.navigationController.viewControllers objectAtIndex:previousViewIndex];
        previous.selectedNumbers = selectedNumbers;
        
        [self.navigationController popViewControllerAnimated:YES];
        
    } else if (fromCheck) {
        
        Number *number = [[Number alloc] init];
        int length = [selectedNumbers count];
        
        number.num1 = [[selectedNumbers objectAtIndex:0] intValue];
        number.num2 = [[selectedNumbers objectAtIndex:1] intValue];
        number.num3 = [[selectedNumbers objectAtIndex:2] intValue];
        number.num4 = [[selectedNumbers objectAtIndex:3] intValue];
        number.num5 = [[selectedNumbers objectAtIndex:4] intValue];
        
        if (6 <= length) {
            number.num6 = [[selectedNumbers objectAtIndex:5] intValue];
        }
        
        if (7 <= length) {
            number.num7 = [[selectedNumbers objectAtIndex:6] intValue];
        }
        
        
        [databaseHandler savePurchase:number ofType:(type + 10)];
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        
        NSArray *results = [databaseHandler queryHit:selectedNumbers ofType:type];
        NSMutableString *message = [[NSMutableString alloc] init];
        int counter = 1;
        [message appendString:@"\n"];
        for (NSArray *winResult in results)
        {

            [message appendFormat:@"%d等 合計 %d回\n", counter, [winResult count]];
            
            NSArray *sortResult = [winResult sortedArrayUsingSelector:@selector(comparWithCount:)];
            int resultCount = 1;
            for (Result *lotto in sortResult)
            {
                if (resultCount % 4 == 0) {
                   [message appendString:@"\n"];
                }
                [message appendFormat:@"第%@回 ", lotto.count];
                resultCount++;
            }
            
            [message appendString:@"\n\n"];
            
            counter++;
        }
        
        NSMutableString *selectNum = [[NSMutableString alloc] init];
        [selectNum appendFormat:@"結果 "];
        for (NSString *num in selectedNumbers) {
            [selectNum appendFormat:@"%@  ", num];
        }
        
        if ([UIAlertController class]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:selectNum message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:Nil];
            
        } else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:selectNum
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}


- (IBAction)clearButtonTapped:(id)sender
{

    for (NSIndexPath *indexPath in selectedIndexArray) {
        
        UICollectionViewCell *cell = [numberCollectionView cellForItemAtIndexPath:indexPath];
        UIImageView *background = (UIImageView *)[cell viewWithTag:1];
        [background setImage:unselectedImage];
    }
    
    selectedIndexArray = [[NSMutableArray alloc] init];
    [checkButton setEnabled:false];
}

@end


