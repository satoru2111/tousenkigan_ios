//
//  Rank.m
//  tousenkigan
//
//  Created by JCLuspo on 8/18/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import "Rank.h"

@implementation Rank

@synthesize number1;
@synthesize number2;
@synthesize number3;
@synthesize number4;
@synthesize count;
@synthesize compareString;

- (id)init
{
    return [self initWithNumbers:@[]];
}

- (id)initWithNumbers: (NSArray *)numbers
{
    self = [super init];
    
    if (self) {
        
        int numCount = (int)[numbers count];
        NSMutableString *temp = [[NSMutableString alloc] init];
        
        for (int i=0; i<4; i++) {
            if (i >= numCount || [[numbers objectAtIndex:i] isEqualToString:@"-1"]) {
                [self setNumber:i value:@"-1"];
                [temp appendString:@"AA"];
                continue;
            }
            
            [self setNumber:i value:[numbers objectAtIndex:i]];
            [temp appendString:[NSString stringWithFormat:@"%02d", [[numbers objectAtIndex:i] intValue]]];
        }
        
        compareString = temp;
        count = 1;
    }
    
    return self;
}

- (id)initWithNumber: (NSString *)num atIndex: (int)index
{
    NSMutableArray *nums = [[NSMutableArray alloc] init];
    for (int i=0; i<4; i++) {
        if (index == i) {
            [nums addObject:num];
        } else {
            [nums addObject:@"-1"];
        }
    }
    
    return [self initWithNumbers:nums];
}

- (void) setNumber: (int)num value:(NSString *)value
{
    switch (num) {
        case 0:
            number1 = value;
            break;
        case 1:
            number2 = value;
            break;
        case 2:
            number3 = value;
            break;
        case 3:
            number4 = value;
            break;
    }
}

- (void) countUp
{
    count++;
}

@end
