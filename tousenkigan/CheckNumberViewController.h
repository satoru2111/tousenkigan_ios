//
//  CheckNumberViewController.h
//  tousenkigan
//
//  Created by JCLuspo on 7/24/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CheckNumberViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property int type;

@property (strong, nonatomic) IBOutlet UIView *bottomAdView;

@property (strong, nonatomic) IBOutlet UITableView *purchaseListTable;
@property (strong, nonatomic) IBOutlet UILabel *bottomLabel;
@property (strong, nonatomic) IBOutlet UIButton *addButton;

- (IBAction)addClick:(id)sender;

@end
