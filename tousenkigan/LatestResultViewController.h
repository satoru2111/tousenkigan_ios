//
//  LatestResultViewController.h
//  tousenkigan
//
//  Created by JCLuspo on 7/24/14.
//  Copyright (c) 2014 JCLuspo. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface LatestResultViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property int type;

@property (strong, nonatomic) IBOutlet UIButton *historyButton;

@property (strong, nonatomic) IBOutlet UIView *topAdView;

@property (strong, nonatomic) IBOutlet UIView *bottomAdView;

@property (strong, nonatomic) IBOutlet UICollectionView *displayCollectionView;

- (IBAction)historyButtonTapped:(id)sender;

@end
