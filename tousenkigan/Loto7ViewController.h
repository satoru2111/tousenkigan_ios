//
//  ViewController_Loto7ViewController.h
//  tousenkigan
//
//  Created by Satoru Ozeki on 2014/05/30.
//  Copyright (c) 2014年 Satoru Ozeki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utilities.h"
#import "ImobileSdkAds/ImobileSdkAds.h"
@interface Loto7ViewController : UIViewController< IMobileSdkAdsDelegate >
@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIButton *button3;
@property (strong, nonatomic) IBOutlet UIButton *button4;
@property (strong, nonatomic) IBOutlet UIButton *button5;
@property (strong, nonatomic) IBOutlet UIButton *button6;

@property (strong, nonatomic) IBOutlet UILabel *lucky1;
@property (strong, nonatomic) IBOutlet UILabel *lucky2;
@property (strong, nonatomic) IBOutlet UILabel *lucky3;
@property (strong, nonatomic) IBOutlet UILabel *lucky4;
@property (strong, nonatomic) IBOutlet UILabel *lucky5;
@property (strong, nonatomic) IBOutlet UILabel *lucky6;
@property (strong, nonatomic) IBOutlet UILabel *lucky7;

@property (strong, nonatomic) IBOutlet UIView *adView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonViewWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mikoViewWidthContstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mikoViewHeightConstraint;

- (IBAction)button1Tap:(id)sender;
- (IBAction)button2Tap:(id)sender;
- (IBAction)button3Tap:(id)sender;
- (IBAction)button4Tap:(id)sender;
- (IBAction)button5Tap:(id)sender;
- (IBAction)button6Tap:(id)sender;

//-(void)checkHit:(int)lastNo;
- (void) updateButtonTitle;
@end
