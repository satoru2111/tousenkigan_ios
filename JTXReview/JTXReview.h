//
//  JTXReview.h
//  JTXReview
//
//  Created by 章雅平 on 2016/02/17.
//  Copyright © 2016年 JapanTaxi.co.jp. All rights reserved.
//

/*
 【機能】
 App Storeへのレビュー誘導
 
 【使い方】
 
 Step 1. 下記のファイルを既存のプロジェクトにインポート
 
　JTXReview/
　　　include/
　　　　　JTXReview/
　　　　　　　JTXReview.h
　　　lib/
　　　　　libJTXReview.a
　　　JTXReview.bundle
 
 
 Step 2. UIViewControllerを継承するクラスのメソッドに下記のソースを追加する。
 

    #import "JTXReview.h"
 
 
    JTXReview *review = [[JTXReview alloc]initWithViewController:self];
    review.app_id = @"1076866913";
    //review.count_trigger_firsttime = 2;     // 初回アラートを出すタイミング（何回目の起動で出すか。デフォルトは３回目に出す）
    review.count_trigger_nexttime = 5;     //「後で」を選択されたら再度アラートを出すタイミング（何回目の起動で出すか）
    //review.isDebug = YES;   //デバッグ情報を出力するか（テスト用）
    [review check];
 
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


/* オプション */
#define _TRIGGER_DEFAULT 3  // 初回アラートを出すタイミング（何回目の起動で出すか）
#define _TRIGGER_LATER 3    //「後で」を選択されたら再度アラートを出すタイミング（何回目の起動で出すか）


@interface JTXReview : UIView
@property(nonatomic,weak)UIViewController *controller;
@property(nonatomic)BOOL isDebug;
@property(nonatomic,weak)NSString *app_id;
@property(nonatomic)NSInteger count_trigger_firsttime;
@property(nonatomic)NSInteger count_trigger_nexttime;

- (id)initWithViewController:(UIViewController *)_controller;
- (void)check;

@end
